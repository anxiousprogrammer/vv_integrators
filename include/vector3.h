#include <cmath> // std::sqrt
#include <type_traits> // std::is_same
#include <stdexcept> // std::runtime_error


#ifndef VVI_VECTOR3_H
#define VVI_VECTOR3_H

namespace vvi
{

template<class Treal_t>
class vector3
{
    //+/////////////////
    // members
    //+/////////////////
    
    static_assert(std::is_same<Treal_t, float>::value || std::is_same<Treal_t, double>::value);
    
    Treal_t data_[3] = {};
    
    
public:

    //+/////////////////
    // lifcycle
    //+/////////////////
    
    vector3() = default;
    
    inline vector3(const Treal_t& x, const Treal_t& y, const Treal_t& z)
        : data_{x, y, z} {}
    
    //+/////////////////
    // access
    //+/////////////////
    
    inline Treal_t& operator()(const std::size_t& index)
    {
        #ifndef NDEBUG
        if (3 <= index)
        {
            throw std::invalid_argument("vector3_operator()_invalid_index_error");
        }
        #endif
        return data_[index];
    }
    
    inline const Treal_t& operator()(const std::size_t& index) const
    {
        return const_cast<vector3*>(this)->operator()(index);
    }
    
    //+/////////////////
    // arithmetic
    //+/////////////////
    
    inline vector3 operator-() const
    {
        return vector3(-data_[0], -data_[1], -data_[2]);
    }
    
    inline void operator+=(const vector3& rhs)
    {
        data_[0] += rhs.data_[0];
        data_[1] += rhs.data_[1];
        data_[2] += rhs.data_[2];
    }
    
    inline void operator-=(const vector3& rhs)
    {
        (*this) += -rhs;
    }
    
    inline vector3 operator+(const vector3& rhs) const
    {
        auto result = (*this);
        result += rhs;
        return result;
    }
    
    inline vector3 operator-(const vector3& rhs) const
    {
        auto result = (*this);
        result -= rhs;
        return result;
    }
    
    inline vector3 operator*(const double& rhs) const
    {
        return vector3(this->data_[0] * rhs, this->data_[1] * rhs, this->data_[2] * rhs);
    }
};

} // namespace vvi

#endif
