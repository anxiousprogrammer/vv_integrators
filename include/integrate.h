#include "dim.h"
#include "system_view.h"
#ifdef VVI_USE_MPI
#include "replica_view.h"
#include "scratch_cache.h"
#include "comm_cache.h"
#endif

#include "force/i_force_element.h"
#include "force/i_thermostat.h"

#include <utility> // std::size_t

#ifdef VVI_USE_MPI
#include <mpi.h>
#endif


#ifndef VVI_INTEGRATE_H
#define VVI_INTEGRATE_H

namespace vvi
{

template<class Treal_t>
void integrate(const e_dim dim, system_view<Treal_t>& sys, const Treal_t& dt, const std::size_t& time_step_count,
    i_force_element<Treal_t>* force_element, i_thermostat<Treal_t>* thermostat, const std::string& write_file_name);

#ifdef VVI_USE_MPI
template<class Treal_t>
void integrate(const e_dim dim, replica_view<Treal_t>& bead, const Treal_t& dt_bd, const Treal_t& gamma_bd, const Treal_t& dt,
    const std::size_t& time_step_count, i_force_element<Treal_t>* force_element, i_thermostat<Treal_t>* thermostat,
    scratch_cache<Treal_t>& scache, comm_cache<Treal_t>& ccache, MPI_Comm comm, const std::string& write_file_name);
#endif

} // namespace vvi

#endif
