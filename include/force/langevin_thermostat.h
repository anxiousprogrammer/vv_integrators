#include "dim.h"
#include "vector3.h"

#include "force/i_thermostat.h"

#include <random> // std::mt19937


#ifndef VVI_LANGEVIN_THERMOSTAT_H
#define VVI_LANGEVIN_THERMOSTAT_H

namespace vvi
{

template<class Treal_t>
class langevin_thermostat : public i_thermostat<Treal_t>
{
public:
    
    //+/////////////////
    // lifecycle
    //+/////////////////
    
    langevin_thermostat(const e_dim dim, const Treal_t& dt, const Treal_t& mass, const Treal_t& gamma,
        const Treal_t& temperature);
    
    //+/////////////////
    // main functionality
    //+/////////////////
    
    virtual void operator()(const vector3<Treal_t>& velocity, vector3<Treal_t>& force) final;

    
private:
    
    const e_dim dim_;
    const Treal_t mass_;
    const Treal_t gamma_;
    const Treal_t temperature_;
    
    std::mt19937_64 random_engine_;
    
    // cached variable
    const Treal_t sigma_ = {};
};

} // namespace vvi

#endif
