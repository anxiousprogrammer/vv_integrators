#include "vector3.h"
#include "i_force_element.h"


#ifndef VVI_I_FORCE_ELEMENT_PAIRWISE_H
#define VVI_I_FORCE_ELEMENT_PAIRWISE_H

namespace vvi
{

template<class Treal_t>
class i_force_element_pairwise : public i_force_element<Treal_t>
{
public:
    
    //+/////////////////
    // lifecycle
    //+/////////////////
    
    i_force_element_pairwise()
        : i_force_element<Treal_t>(e_force_element_interaction::pairwise) {}
    
    
    //+/////////////////
    // main functionality
    //+/////////////////
    
    virtual Treal_t compute_potential_energy(const vector3<Treal_t>&, const vector3<Treal_t>&) = 0;
    
    virtual vector3<Treal_t> compute_force(const vector3<Treal_t>&, const vector3<Treal_t>&) = 0;
};

} // namespace vvi

#endif
