#ifndef VVI_I_FORCE_ELEMENT_H
#define VVI_I_FORCE_ELEMENT_H

namespace vvi
{

enum class e_force_element_interaction {external, pairwise};

template<class Treal_t>
class i_force_element
{
public:
    
    //+/////////////////
    // lifecycle
    //+/////////////////
    
    inline i_force_element(const e_force_element_interaction interaction)
        : interaction_(interaction) {}
    
    virtual ~i_force_element() = default;
    
    //+/////////////////
    // main functionality
    //+/////////////////
    
    inline e_force_element_interaction get_interaction_type() const
    {
        return interaction_;
    }
    
    
protected:
    
    //+/////////////////
    // members
    //+/////////////////
    
    e_force_element_interaction interaction_;
};

} // namespace vvi

#endif
