#include "vector3.h"


#ifndef VVI_I_THERMOSTAT_H
#define VVI_I_THERMOSTAT_H

namespace vvi
{

template<class Treal_t>
class i_thermostat
{
public:
    
    //+/////////////////
    // main functionality
    //+/////////////////
    
    virtual void operator()(const vector3<Treal_t>& velocity, vector3<Treal_t>& force) = 0;
};

} // namespace vvi

#endif
