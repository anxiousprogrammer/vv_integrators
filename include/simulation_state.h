#include "vector3.h"
#include "vector3_array.h"

#include <memory> // std::unique_ptr
#include <algorithm> // std::fill


#ifndef VVI_SIMULATION_STATE_H
#define VVI_SIMULATION_STATE_H

namespace vvi
{

//+////////////////////////////////////////////////////////////////////////////////////////////////
// class which represents the state of the simulation
//+////////////////////////////////////////////////////////////////////////////////////////////////

// forward declarations
template<class Treal_t> class system_view;
template<class Treal_t> class replica_view;

template<class Treal_t> class simulation_state;

template<class Treal_t>
std::unique_ptr<simulation_state<Treal_t>> create_simulation_state(const std::size_t&);
template<class Treal_t>
void init_simulation_state(simulation_state<Treal_t>*);


// class template definition 
template<class Treal_t>
class simulation_state
{
    friend class system_view<Treal_t>; // standard MD
    friend class replica_view<Treal_t>;
    
    friend std::unique_ptr<simulation_state> create_simulation_state<Treal_t>(const std::size_t&);
    friend void init_simulation_state<Treal_t>(simulation_state*);
    
    //+/////////////////
    // lifecycle
    //+/////////////////
    
    simulation_state() = default;
    
    
    //+/////////////////
    // members
    //+/////////////////
    
    vector3_array<Treal_t> positions_ = {};
    vector3_array<Treal_t> velocities_ = {};
    vector3_array<Treal_t> forces_ = {};
    std::unique_ptr<Treal_t> masses_ = {};
    
    // cache
    std::unique_ptr<Treal_t> inv_masses_ = {};
    vector3_array<Treal_t> etas_ = {};
};


//+////////////////////////////////////////////////////////////////////////////////////////////////
// function which creates a state variable
//+////////////////////////////////////////////////////////////////////////////////////////////////

template<class Treal_t>
std::unique_ptr<simulation_state<Treal_t>> create_simulation_state(const std::size_t& size)
{
    // output variable
    auto* sstate = new simulation_state<Treal_t>();
    
    // resizing
    sstate->positions_.resize(size);
    sstate->velocities_.resize(size);
    sstate->forces_.resize(size);
    sstate->masses_.reset(new Treal_t[size]);
    
    sstate->inv_masses_.reset(new Treal_t[size]);
    sstate->etas_.resize(size);
    
    // done
    return std::unique_ptr<simulation_state<Treal_t>>(sstate);
}


// NOTE: this function is for convenience only!
template<class Treal_t>
void init_simulation_state(simulation_state<Treal_t>* sstate)
{
    // preconditions
    if (sstate == nullptr)
    {
        throw std::invalid_argument("init_simulation_state_nullptr_error");
    }
    
    // fill
    std::fill(sstate->positions_.data(), sstate->positions_.data() + sstate->positions_.size(), vector3<Treal_t>{});
    std::fill(sstate->velocities_.data(), sstate->velocities_.data() + sstate->velocities_.size(), vector3<Treal_t>{});
    std::fill(sstate->forces_.data(), sstate->forces_.data() + sstate->forces_.size(), vector3<Treal_t>{});
    std::fill(sstate->masses_.get(), sstate->masses_.get() + sstate->positions_.size(), static_cast<Treal_t>(1.0)); // we'll probably not change this at all within this project
    
    std::fill(sstate->inv_masses_.get(), sstate->inv_masses_.get() + sstate->positions_.size(), static_cast<Treal_t>(1.0)); // same
    std::fill(sstate->etas_.data(), sstate->etas_.data() + sstate->etas_.size(), vector3<Treal_t>{});
}

} // namespace vvi

#endif
