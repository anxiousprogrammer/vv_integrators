#include "vector3.h"

#include <memory> // std::malloc
#include <stdexcept> // std::runtime_error


#ifndef VVI_VECTOR3_ARRAY_H
#define VVI_VECTOR3_ARRAY_H

namespace vvi
{

template<class Treal_t>
class vector3_array
{
    //+/////////////////
    // members
    //+/////////////////
    
    vector3<Treal_t>* data_ = nullptr;
    std::size_t size_ = {};
    
    
    //+/////////////////
    // helper
    //+/////////////////
    
    inline void allocate(const std::size_t& size)
    {
        data_ = reinterpret_cast<vector3<Treal_t>*>(std::malloc(size * sizeof(vector3<Treal_t>)));
    }
    
    inline void free()
    {
        if (data_ != nullptr)
        {
            std::free(data_);
        }
        data_ = nullptr;
    }
    
    
public:

    //+/////////////////
    // lifecycle
    //+/////////////////
    
    vector3_array() = default;
    
    vector3_array(const vector3_array& ref);
    
    vector3_array(vector3_array&& ref);
    
    vector3_array& operator=(const vector3_array& ref);
    
    vector3_array& operator=(vector3_array&& ref);
    
    ~vector3_array();
    
    
    //+/////////////////
    // access
    //+/////////////////
    
    inline std::size_t size() const
    {
        return size_;
    }
    
    inline vector3<Treal_t>& operator[](const std::size_t& index)
    {
        #ifndef NDEBUG
        if (size() <= index)
        {
            throw std::invalid_argument("vector3_array_operator[]_invalid_index_error");
        }
        #endif
        return data_[index];
    }
    
    inline const vector3<Treal_t>& operator[](const std::size_t& index) const
    {
        return const_cast<vector3_array*>(this)->operator[](index);
    }
    
    
    //+/////////////////
    // raw access
    //+/////////////////
    
    inline vector3<Treal_t>* data()
    {
        return data_;
    }
    
    inline const vector3<Treal_t>* data() const
    {
        return const_cast<vector3_array<Treal_t>*>(this)->data();
    }
    
    
    //+/////////////////
    // manipulation
    //+/////////////////
    
    void resize(const std::size_t& new_size);
};

} // namespace vvi

#endif
