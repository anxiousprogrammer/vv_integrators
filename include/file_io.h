#include "vector3.h"

#include <string> // std::string
#include <vector> // std::vector


#ifndef VVI_FILE_IO_H
#define VVI_FILE_IO_H

namespace vvi
{

void clear_target(const std::string& file_name);

template<template<class> class Tview_t, class Treal_t>
void write_coordinates(const std::string& file_name, const std::size_t& time_step, const Tview_t<Treal_t>& view);

template<class Treal_t>
struct t_md_frame
{
    std::string comment = {};
    std::vector<std::string> atom_names = {};
    std::vector<vector3<Treal_t>> positions = {};
};

template<class Treal_t>
std::vector<t_md_frame<Treal_t>> read_xyz_file(const std::string& file_name);

template<class Treal_t>
void write_xyz_file(const std::string& file_name, const std::vector<t_md_frame<Treal_t>>& frames);

} // namespace vvi

#endif
