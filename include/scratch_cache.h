#include "vector3.h"

#include <vector> // std::vector
#include <algorithm> // std::fill


#ifndef VVI_SCRATCH_CACHE_H
#define VVI_SCRATCH_CACHE_H

namespace vvi
{

template<class Treal_t>
struct scratch_cache
{
    std::vector<vector3<Treal_t>> vector_vector3_0;
    std::vector<vector3<Treal_t>> vector_vector3_1;
    std::vector<vector3<Treal_t>> vector_vector3_2;
    std::vector<vector3<Treal_t>> vector_vector3_3;
    std::vector<vector3<Treal_t>> vector_vector3_4;
};

template<class Treal_t>
inline scratch_cache<Treal_t> create_scratch_cache(const std::size_t& particle_count)
{
    auto ret_var = scratch_cache<Treal_t>{};
    
    // resize
    ret_var.vector_vector3_0.resize(particle_count);
    ret_var.vector_vector3_1.resize(particle_count);
    ret_var.vector_vector3_2.resize(particle_count);
    ret_var.vector_vector3_3.resize(particle_count);
    ret_var.vector_vector3_4.resize(particle_count);
    
    // init
    // TODO: if ccNUMA first-touch is desired, it needs to be done here!
    std::fill(ret_var.vector_vector3_0.begin(), ret_var.vector_vector3_0.end(), vector3<Treal_t>{});
    std::fill(ret_var.vector_vector3_1.begin(), ret_var.vector_vector3_1.end(), vector3<Treal_t>{});
    std::fill(ret_var.vector_vector3_2.begin(), ret_var.vector_vector3_2.end(), vector3<Treal_t>{});
    std::fill(ret_var.vector_vector3_3.begin(), ret_var.vector_vector3_3.end(), vector3<Treal_t>{});
    std::fill(ret_var.vector_vector3_4.begin(), ret_var.vector_vector3_4.end(), vector3<Treal_t>{});
    
    return ret_var;
}

} // namespace vvi

#endif
