#include <utility> // std::size_t
#include <stdexcept> // std::runtime_error
#include <type_traits> // std::make_signed


#ifndef VVI_ARRAY_VIEW_H
#define VVI_ARRAY_VIEW_H

namespace vvi
{

template<class Tvalue_t>
class array_view // a contiguous, forward-iterating view
{
public:
    
    //+/////////////////
    // types
    //+/////////////////
    
    static_assert(!std::is_lvalue_reference<Tvalue_t>::value && !std::is_rvalue_reference<Tvalue_t>::value);
    
    using value_type = Tvalue_t;
    using size_type = std::size_t;
    using difference_type = std::ptrdiff_t;
    using reference = Tvalue_t&;
    using const_reference = const Tvalue_t&;
    using pointer = Tvalue_t*;
    using const_pointer = const Tvalue_t*;
    using iterator = Tvalue_t*;
    using const_iterator = const Tvalue_t*;
    
    
    //+/////////////////
    // lifecycle
    //+/////////////////
    
    array_view() = default;
    
    array_view(iterator begin, const_iterator end, const std::size_t& size)
        : begin_(begin), end_(end), size_(size)
    {
        // preconditions
        #ifndef NDEBUG
        if (begin_ == nullptr || end_ == nullptr)
        {
            throw std::invalid_argument("array_view_dangling_state_error");
        }
        #endif
        
        // preconditions
        if (size_ == 0)
        {
            throw std::invalid_argument("array_view_invalid_size_error");
        }
        
        // post-conditions
        const auto signed_size = end_ - begin_;
        if (signed_size != static_cast<std::make_signed<std::size_t>::type>(size_))
        {
            throw std::invalid_argument("array_view_invalid_iterators_error");
        }
    }
    
    
    //+/////////////
    // access
    //+/////////////
    
    inline std::size_t size() const
    {
        return size_;
    }
    
    bool empty() const
    {
        return begin_ == end_;
    }
    
    iterator begin()
    {
        return begin_;
    }
    
    const_iterator begin() const
    {
        return begin_;
    }
    
    const_iterator end() const
    {
        return end_;
    }
    
    
private:
    
    //+/////////////
    // access
    //+/////////////
    
    iterator begin_ = {};
    const_iterator end_ = {};
    
    // cache
    std::size_t size_ = {};
};

} // namespace vvi

#endif
