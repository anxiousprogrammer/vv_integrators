#include "vector3.h"
#include "simulation_state.h"
#include "array_view.h"

#include <utility> // std::size_t
#include <stdexcept> // std::runtime_error


#ifndef VVI_REPLICA_VIEW_H
#define VVI_REPLICA_VIEW_H

namespace vvi
{

template<class Treal_t>
class replica_view
{
    //+/////////////////
    // members
    //+/////////////////
    
    simulation_state<Treal_t>* state_;
    
    // cache
    std::size_t size_ = {};
    
    inline simulation_state<Treal_t>* state()
    {
        #ifndef NDEBUG
        if (state_ == nullptr)
        {
            throw std::runtime_error("replica_view_state_dangling_pointer_error");
        }
        #endif
        return state_;
    }
    
    inline const simulation_state<Treal_t>* state() const
    {
        return const_cast<replica_view*>(this)->state();
    }
    
    
public:

    //+/////////////////
    // lifecycle
    //+/////////////////
    
    inline replica_view(simulation_state<Treal_t>* state)
        : state_(state)
    {
        // preconditions
        if (state_ == nullptr)
        {
            throw std::invalid_argument("replica_view_dangling_state_error");
        }
        size_ = state_->positions_.size();
        if (size_ == 0)
        {
            throw std::runtime_error("replica_view_empty_containers_error");
        }
        if (size_ != state_->velocities_.size()
            || size_ != state_->forces_.size())
        {
            throw std::runtime_error("replica_view_container_size_mismatch_error");
        }
    }
    
    
    //+/////////////
    // access
    //+/////////////
    
    inline std::size_t size() const
    {
        return size_;
    }
    
    inline array_view<vector3<Treal_t>> get_positions_view()
    {
        auto& positions = state()->positions_;
        return array_view(positions.data(), positions.data() + positions.size(), size_);
    }
    
    inline array_view<const vector3<Treal_t>> get_positions_view() const
    {
        const auto& positions = state()->positions_;
        return array_view<const vector3<Treal_t>>(positions.data(), positions.data() + positions.size(), size_);
    }
    
    inline array_view<vector3<Treal_t>> get_forces_view()
    {
        auto& forces = state()->forces_;
        return array_view(forces.data(), forces.data() + forces.size(), size_);
    }
    
    inline array_view<const vector3<Treal_t>> get_forces_view() const
    {
        const auto& forces = state()->forces_;
        return array_view<const vector3<Treal_t>>(forces.data(), forces.data() + forces.size(), size_);
    }
    
    inline array_view<Treal_t> get_masses_view()
    {
        auto& masses = state()->masses_;
        return array_view(masses.get(), masses.get() + size_, size_);
    }
    
    inline array_view<const Treal_t> get_masses_view() const
    {
        const auto& masses = state()->masses_;
        return array_view<const Treal_t>(masses.get(), masses.get() + size_, size_);
    }
    
    inline array_view<Treal_t> get_cached_inv_masses_view()
    {
        auto& inv_masses = state()->inv_masses_;
        return array_view(inv_masses.get(), inv_masses.get() + size_, size_);
    }
    
    inline array_view<const Treal_t> get_cached_inv_masses_view() const
    {
        const auto& inv_masses = state()->inv_masses_;
        return array_view<const Treal_t>(inv_masses.get(), inv_masses.get() + size_, size_);
    }
    
    inline array_view<vector3<Treal_t>> get_cached_etas_view()
    {
        auto& etas = state()->etas_;
        return array_view(etas.data(), etas.data() + etas.size(), size_);
    }
    
    inline array_view<const vector3<Treal_t>> get_cached_etas_view() const
    {
        const auto& etas = state()->etas_;
        return array_view<const vector3<Treal_t>>(etas.data(), etas.data() + etas.size(), size_);
    }
    
    
    //+/////////////
    // access (only for readability in 'integrate')
    // Warning: these functions will NOT be tested!
    //+/////////////
    
    inline vector3<Treal_t>& get_position(const std::size_t& index)
    {
        #ifndef NDEBUG
        if (size() <= index)
        {
            throw std::invalid_argument("replica_view_get_position_invalid_index_error");
        }
        #endif
        return state()->positions_[index];
    }
    
    inline const vector3<Treal_t>& get_position(const std::size_t& index) const
    {
        return const_cast<replica_view*>(this)->get_position(index);
    }
    
    inline vector3<Treal_t>& get_velocity(const std::size_t& index)
    {
        #ifndef NDEBUG
        if (size() <= index)
        {
            throw std::invalid_argument("replica_view_get_velocity_invalid_index_error");
        }
        #endif
        return state()->velocities_[index];
    }
    
    inline const vector3<Treal_t>& get_velocity(const std::size_t& index) const
    {
        return const_cast<replica_view*>(this)->get_velocity(index);
    }
    
    inline vector3<Treal_t>& get_force(const std::size_t& index)
    {
        #ifndef NDEBUG
        if (size() <= index)
        {
            throw std::invalid_argument("replica_view_get_force_invalid_index_error");
        }
        #endif
        return state()->forces_[index];
    }
    
    inline const vector3<Treal_t>& get_force(const std::size_t& index) const
    {
        return const_cast<replica_view*>(this)->get_force(index);
    }
    
    inline Treal_t& get_mass(const std::size_t& index)
    {
        #ifndef NDEBUG
        if (size() <= index)
        {
            throw std::invalid_argument("replica_view_get_mass_invalid_index_error");
        }
        #endif
        return state()->masses_.get()[index];
    }
    
    inline const Treal_t& get_mass(const std::size_t& index) const
    {
        return const_cast<replica_view*>(this)->get_mass(index);
    }
};

} // namespace vvi

#endif
