#ifndef VVI_DIM_H
#define VVI_DIM_H

namespace vvi
{

enum class e_dim { d1 = 1, d2 = 2, d3 = 3 };

} // namespace vvi

#endif
