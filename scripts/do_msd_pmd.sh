#!/bin/bash

# clear
rm -f \# *.xyz

# run
VVI_THERMOSTAT=1 VVI_TRAJECTORY_INTERVAL=100 VVI_EQ_CUT_OFF=10000 mpirun --oversubscribe -n 120 ../app/many_body_msd_pmd 0.001 120 100.0 8 0.001 20001 0.6 70.0 > many_body_msd_pmd.log

# transpose
../bin/transpose . many_body_msd_pmd_p 120 out

# compute msd
../bin/msd ./out/ transposed 100

# plot histogram
gnuplot plot_histogram.plt
cp histogram.png ../histogram_pmd.png
