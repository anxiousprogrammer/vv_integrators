binwidth = 0.002 # need to fine-tune this to get a good histogram
bin(x, width) = width*floor(x/width)

set tics out nomirror
set style fill transparent solid 0.5 border lt -1
set xtics binwidth
set boxwidth binwidth

set xrange [0:0.1]

set term pngcairo size 1500,800 enhanced font ",6"
set output 'histogram.png'

plot "squared_deviations.dat" u (bin($1,binwidth)):(1.0) smooth freq with boxes notitle
