#!/bin/bash

# clear
rm -f \# *.xyz *.log *.dat *.png

# run
RUN_COUNT=100
for (( i=0; i<$RUN_COUNT; i+=1 ))
do
    VVI_THERMOSTAT=1 VVI_TRAJECTORY_INTERVAL=1 VVI_EQ_CUT_OFF=1000 ../app/many_body_msd 0.001 1121 8 100.0 70.0 > many_body_msd$i.log
    mv many_body_msd.xyz many_body_msd$i.xyz
done

# compute msd
../bin/msd . many_body_msd $RUN_COUNT

# plot histogram
gnuplot plot_histogram.plt
cp histogram.png ../histogram_bd.png
