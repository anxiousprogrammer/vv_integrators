/// \file This file contains the benchmark program of the kernel 'compute_effective_forces'.

#include "vector3.h"

#include "tixl.h"

#include <memory> // std::malloc
#include <utility> // std::abort
#include <iostream> // std::cerr
#include <random> // std::mt19937_64
#include <cmath> // std::fabs

#ifdef USE_EXPLICIT_VEC
#include "intrinsics_helper.h"
#endif

#include <omp.h>


using namespace vvi;

//+////////////////////////////////////////////////////////////////////////////////////////////////
// implementation and test
//+////////////////////////////////////////////////////////////////////////////////////////////////

namespace benchmark
{

template<class Treal_t>
void compute_effective_forces(const vector3<Treal_t>* etas, const vector3<Treal_t>* etas_left, const Treal_t* masses,
    const vector3<Treal_t>* hessian_U_t_eta_eps, vector3<Treal_t>* effective_forces, const std::size_t& size,
    const Treal_t& gamma_bd_b_dt_bd)
{
    #ifndef NDEBUG
    if (size == 0)
    {
        throw std::invalid_argument("compute_effective_forces_zero_size_error");
    }
    if (etas == nullptr || etas_left == nullptr || masses == nullptr || hessian_U_t_eta_eps == nullptr
        || effective_forces == nullptr)
    {
        throw std::invalid_argument("compute_effective_forces_null_pointer_error");
    }
    constexpr auto zero = std::is_same<Treal_t, float>::value ? static_cast<Treal_t>(1E-6) : static_cast<Treal_t>(1E-13);
    if (gamma_bd_b_dt_bd < zero)
    {
        throw std::invalid_argument("compute_effective_forces_zero_gamma_bd_b_dt_bd_error");
    }
    #endif
    
    const auto k2 = static_cast<Treal_t>(0.5) * gamma_bd_b_dt_bd;
    #pragma omp parallel for simd safelen(16)
    for (auto particle_index = std::size_t{}; particle_index < size; ++particle_index)
    {
        effective_forces[particle_index] = (etas[particle_index] - etas_left[particle_index]) * k2 * masses[particle_index]
            + hessian_U_t_eta_eps[particle_index] * static_cast<Treal_t>(0.5);
        // FP: 15 FLOP; Traffic: 13+3 Treal_t
    }
}


//+////////////////////////////////////////////////////////////////////////////////////////////////
// functor
//+////////////////////////////////////////////////////////////////////////////////////////////////

template<class Treal_t>
class exp_fn : public tixl::experiment_functor
{
    //+/////////////////
    // members
    //+/////////////////
    
    const std::size_t size_;
    vector3<Treal_t>* etas_ = nullptr;
    vector3<Treal_t>* etas_left_ = nullptr;
    Treal_t* masses_ = nullptr;
    vector3<Treal_t>* hessian_U_t_eta_eps_ = nullptr;
    vector3<Treal_t>* effective_forces_ = nullptr;
    Treal_t gamma_bd_b_dt_bd_;
    
public:
    
    //+/////////////////
    // lifecycle
    //+/////////////////
    
    exp_fn(const std::size_t& size, const Treal_t& gamma_bd_b_dt_bd)
        : size_(size), gamma_bd_b_dt_bd_(gamma_bd_b_dt_bd) {}
    
    //+/////////////////
    // main functionality
    //+/////////////////
    
    void init() final
    {
        // allocate anew and lay a view on it
        etas_ = reinterpret_cast<vector3<Treal_t>*>(std::malloc(size_ * sizeof(vector3<Treal_t>)));
        etas_left_ = reinterpret_cast<vector3<Treal_t>*>(std::malloc(size_ * sizeof(vector3<Treal_t>)));
        masses_ = reinterpret_cast<Treal_t*>(std::malloc(size_ * sizeof(Treal_t)));
        hessian_U_t_eta_eps_ = reinterpret_cast<vector3<Treal_t>*>(std::malloc(size_ * sizeof(vector3<Treal_t>)));
        effective_forces_ = reinterpret_cast<vector3<Treal_t>*>(std::malloc(size_ * sizeof(vector3<Treal_t>)));
        if (etas_ == nullptr || etas_left_ == nullptr || masses_ == nullptr || hessian_U_t_eta_eps_ == nullptr
            || effective_forces_ == nullptr)
        {
            std::cerr << "Fatal error: no memory could be allocated for test data!" << std::endl;
            std::abort();
        }
        
        // first touch
        #pragma omp parallel
        {
            const auto vector3_size = size_ * sizeof(vector3<Treal_t>) / sizeof(Treal_t);
            const auto vector3_chunk_size = vector3_size / omp_get_max_threads();
            tixl::set_first_touch(reinterpret_cast<Treal_t*>(etas_), vector3_size, vector3_chunk_size, omp_get_thread_num(),
                omp_get_max_threads());
            tixl::set_first_touch(reinterpret_cast<Treal_t*>(etas_left_), vector3_size, vector3_chunk_size, omp_get_thread_num(),
                omp_get_max_threads());
            tixl::set_first_touch(reinterpret_cast<Treal_t*>(masses_), size_, size_ / omp_get_max_threads(),
                omp_get_thread_num(), omp_get_max_threads());
            tixl::set_first_touch(reinterpret_cast<Treal_t*>(hessian_U_t_eta_eps_), vector3_size, vector3_chunk_size,
                omp_get_thread_num(), omp_get_max_threads());
            tixl::set_first_touch(reinterpret_cast<Treal_t*>(effective_forces_), vector3_size, vector3_chunk_size,
                omp_get_thread_num(), omp_get_max_threads());
        }
    }
    
    void perform_experiment() final
    {
        compute_effective_forces(etas_, etas_left_, masses_, hessian_U_t_eta_eps_, effective_forces_, size_, gamma_bd_b_dt_bd_);
    }
    
    void finish() final
    {
        std::free(etas_);
        std::free(etas_left_);
        std::free(masses_);
        std::free(hessian_U_t_eta_eps_);
        std::free(effective_forces_);
        etas_ = nullptr;
        etas_left_ = nullptr;
        masses_ = nullptr;
        hessian_U_t_eta_eps_ = nullptr;
        effective_forces_ = nullptr;
    }
};

} // namespace benchmark


//+////////////////////////////////////////////////////////////////////////////////////////////////
// main
//+////////////////////////////////////////////////////////////////////////////////////////////////

using real_t = float;

int main(int argc, char** argv)
{
    // extract the problem size
    if (argc < 2)
    {
        std::cerr << "Please provide the problem size as command line argument." << std::endl;
        return 1;
    }
    const auto n = static_cast<std::size_t>(std::stoul(argv[1]));
    if (n < 1)
    {
        std::cerr << "Zero valued problem size provided. The program will now be aborted." << std::endl;
        return 1;
    }
    const auto size = n * n * n;
    if (size * sizeof(real_t) / (1024 * 1024 * 1024) > 40)
    {
        std::cerr << "Problem size is too large. The program will now be aborted." << std::endl;
        return 1;
    }
    
    // report
    std::cout << "Starting runs with problem size n=" << n << ".\nThread count: " << omp_get_max_threads() << "."
        << std::endl;
    
    // fix the experiment count
    const auto experiment_count = std::size_t{100};
    
    // perform the experiments
    auto ef = benchmark::exp_fn<real_t>(size, static_cast<real_t>(0.1));
    const auto measurements = tixl::perform_experiments(ef, experiment_count);
    const auto stats = tixl::compute_statistics(measurements);
    tixl::output_results("Benchmarking of compute_effective_forces", stats, 15 * size, 16 * size * sizeof(real_t));
    
    return 0;
}

// END OF TESTFILE
