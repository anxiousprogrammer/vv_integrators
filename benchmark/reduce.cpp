/// \file This file contains the benchmark program of calling MPI_Reduce with size 2.

#include "tixl_w_mpi.h"

#include <cstdlib> // std::malloc
#include <type_traits> // std::is_same
#include <iostream> // std::cerr
#include <array> // std::array

#include <mpi.h>
#include <omp.h>


//+////////////////////////////////////////////////////////////////////////////////////////////////
// Functor
//+////////////////////////////////////////////////////////////////////////////////////////////////

namespace benchmark
{

class exp_fn : public tixl::experiment_functor
{
    //+/////////////////
    // members
    //+/////////////////
    
    static constexpr std::size_t N_ = 2;
    const int comm_rank_;
    const int comm_size_;
    double* send_data_ = nullptr;
    double* recv_data_ = nullptr;
    
    
public:
    
    //+/////////////////
    // lifecycle
    //+/////////////////
    
    exp_fn(const int comm_rank, const int comm_size)
        : comm_rank_(comm_rank), comm_size_(comm_size) {}
    
    //+/////////////////
    // main functionality
    //+/////////////////
    
    void init() final
    {
        // allocate anew and lay a view on it
        send_data_ = reinterpret_cast<double*>(std::malloc(N_ * sizeof(double)));
        recv_data_ = reinterpret_cast<double*>(std::malloc(N_ * sizeof(double)));
        if (send_data_ == nullptr || recv_data_ == nullptr)
        {
            if (comm_rank_ == 0)
            {
                std::cerr << "Fatal error: Test data could not be allocated!" << std::endl;
            }
            MPI_Abort(MPI_COMM_WORLD, MPI_ERR_UNKNOWN);
        }
        
        // first touch (pretty ridiculous here...)
        send_data_[0] = 0.0;
        recv_data_[0] = 0.0;
    }
    
    void perform_experiment() final
    {
        MPI_Reduce(send_data_, recv_data_, 2, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);
    }
    
    void finish() final
    {
        std::free(send_data_);
        std::free(recv_data_);
    }
};

} // namespace benchmark


//+////////////////////////////////////////////////////////////////////////////////////////////////
// main
//+////////////////////////////////////////////////////////////////////////////////////////////////

int main(int argc, char** argv)
{
    // MPI init
    auto provided = int{};
    MPI_Init_thread(&argc, &argv, MPI_THREAD_FUNNELED, &provided);
    if (provided < MPI_THREAD_FUNNELED)
    {
        std::cerr << "Fatal error: Multi-threading support not available with the current MPI implementation." << std::endl;
        MPI_Abort(MPI_COMM_WORLD, MPI_ERR_UNKNOWN);
    }
    
    // comm. details
    auto comm_rank = -1;
    MPI_Comm_rank(MPI_COMM_WORLD, &comm_rank);
    auto comm_size = -1;
    MPI_Comm_size(MPI_COMM_WORLD, &comm_size);
    
    // report
    if (comm_rank == 0)
    {
        std::cout << "Starting runs with thread count: " << omp_get_max_threads() << "." << std::endl;
    }
    
    // fix the experiment count
    const auto experiment_count = std::size_t{100};
    
    // perform the experiments
    auto ef = benchmark::exp_fn(comm_rank, comm_size);
    const auto measurements = tixl::mpi_perform_experiments(MPI_COMM_WORLD, ef, experiment_count);
    const auto stats = tixl::mpi_compute_statistics(MPI_COMM_WORLD, measurements);
    if (comm_rank == 0)
    {
        tixl::write_measurements(measurements, std::string("reduce_measurements_P") + std::to_string(comm_size)
            + std::string(".txt"));
        tixl::output_results("Benchmarking of MPI Reduce", stats, 0, 0);
    }
    
    // finalize
    MPI_Finalize();
    
    return 0;
}

// END OF TESTFILE
