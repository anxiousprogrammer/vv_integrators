/// \file This file can be misused to test very specific things

#include "vector3.h"
#include "vector3_array.h"

#include <utility> // std::abort
#include <iostream> // std::cerr
#include <cmath> // std::fabs
#include <vector> // std::vector
#include <iomanip> // std::setprecision

#include <immintrin.h>


using namespace vvi;

//+//////////////////////////////////////////////
// functionality
//+//////////////////////////////////////////////

constexpr auto g_size = std::size_t{74};

template<class Treal_t>
struct structure_of_arrays
{
    std::array<Treal_t, g_size> x = {};
    std::array<Treal_t, g_size> y = {};
    std::array<Treal_t, g_size> z = {};
    
    inline void zero()
    {
        std::fill(x.data(), x.data() + x.size(), Treal_t{});
        std::fill(y.data(), y.data() + y.size(), Treal_t{});
        std::fill(z.data(), z.data() + z.size(), Treal_t{});
    }
};


template<class Treal_t>
void aos_to_soa(const vector3_array<Treal_t>& aos, structure_of_arrays<Treal_t>& soa)
{
    // loop constants
    constexpr auto is_sp = std::is_same<Treal_t, float>::value;
    constexpr auto aos_inc = is_sp ? 24 : 12;
    const auto stream_size_div = aos.size() * 3 / aos_inc * aos_inc; // 216
    constexpr auto soa_inc = is_sp ? 8 : 4;
    
    const auto* vector3_real_stream = reinterpret_cast<const Treal_t*>(aos.data());
    auto soa_index = std::size_t{};
    for (auto aos_index = std::size_t{}; aos_index < stream_size_div; aos_index += aos_inc, soa_index += soa_inc) // 9 runs
    {
        const auto* start = vector3_real_stream + aos_index;
        if constexpr (is_sp)
        {
            // load 128-bit chunks
            const auto l1_0 = _mm_loadu_ps(start);
            const auto l1_1 = _mm_loadu_ps(start + 4);
            const auto l1_2 = _mm_loadu_ps(start + 8);
            const auto l1_3 = _mm_loadu_ps(start + 12);
            const auto l1_4 = _mm_loadu_ps(start + 16);
            const auto l1_5 = _mm_loadu_ps(start + 20);
            
            // rearrange as 256-bit chunks (expensive ops)
            const auto l2_0_low = _mm256_castps128_ps256(l1_0);
            const auto l2_1_low = _mm256_castps128_ps256(l1_1);
            const auto l2_2_low = _mm256_castps128_ps256(l1_2);
            const auto l2_0 = _mm256_insertf128_ps(l2_0_low, l1_3, 1);
            const auto l2_1 = _mm256_insertf128_ps(l2_1_low, l1_4, 1);
            const auto l2_2 = _mm256_insertf128_ps(l2_2_low, l1_5, 1);
            
            // 5 shuffles set it up
            const auto l3_0 = _mm256_shuffle_ps(l2_0, l2_1, 0b01001001); // 1,0,2,1
            const auto l3_1 = _mm256_shuffle_ps(l2_1, l2_2, 0b10011110); // 2,1,3,2
            const auto x = _mm256_shuffle_ps(l2_0, l3_1, 0b10001100); // 2,0,3,0
            const auto y = _mm256_shuffle_ps(l3_0, l3_1, 0b11011000); // 3,1,2,0
            const auto z = _mm256_shuffle_ps(l3_0, l2_2, 0b11001101); // 3,0,3,1
            
            // stores
            _mm256_storeu_ps(soa.x.data() + soa_index, x);
            _mm256_storeu_ps(soa.y.data() + soa_index, y);
            _mm256_storeu_ps(soa.z.data() + soa_index, z);
        }
        else if constexpr (!is_sp)
        {
            // load 256-bit chunks
            const auto l0_0 = _mm256_loadu_pd(start);
            const auto l0_1 = _mm256_loadu_pd(start + 4);
            const auto l0_2 = _mm256_loadu_pd(start + 8);
            
            // shuffles x5
            const auto l1_0 = _mm256_shuffle_pd(l0_0, l0_1, 0b00001011);
            const auto l1_1 = _mm256_shuffle_pd(l0_1, l0_2, 0b00000010);
            const auto l2_0 = _mm256_shuffle_pd(l0_0, l1_1, 0b00000110);
            const auto y = _mm256_shuffle_pd(l1_0, l1_1, 0b00001100);
            const auto l2_2 = _mm256_shuffle_pd(l1_0, l0_2, 0b00001001);
            
            // rearrange (expensive ops)
            const auto x = _mm256_permute4x64_pd(l2_0, 0b01111000); // 1,3,2,0
            const auto z = _mm256_permute4x64_pd(l2_2, 0b11010010); // 3,1,0,2
            
            // stores
            _mm256_storeu_pd(soa.x.data() + soa_index, x);
            _mm256_storeu_pd(soa.y.data() + soa_index, y);
            _mm256_storeu_pd(soa.z.data() + soa_index, z);
        }
    }
    for (auto index = soa_index; index < aos.size(); ++index)
    {
        soa.x[index] = aos[index](0);
        soa.y[index] = aos[index](1);
        soa.z[index] = aos[index](2);
    }
}


template<class Treal_t>
void soa_to_aos(const structure_of_arrays<Treal_t>& soa, vector3_array<Treal_t>& aos)
{
    // loop constants
    constexpr auto is_sp = std::is_same<Treal_t, float>::value;
    constexpr auto aos_inc = is_sp ? 24 : 12;
    const auto stream_size_div = aos.size() * 3 / aos_inc * aos_inc; // 216
    constexpr auto soa_inc = is_sp ? 8 : 4;
    
    const auto vector3_real_stream = reinterpret_cast<Treal_t*>(aos.data());
    auto soa_index = std::size_t{};
    for (auto aos_index = std::size_t{}; aos_index < stream_size_div; aos_index += aos_inc, soa_index += soa_inc) // 9 runs
    {
        auto* start = vector3_real_stream + aos_index;
        if constexpr (is_sp)
        {
            // load 256-bit chunks
            const auto l0_0 = _mm256_loadu_ps(soa.x.data() + soa_index);
            const auto l0_1 = _mm256_loadu_ps(soa.y.data() + soa_index);
            const auto l0_2 = _mm256_loadu_ps(soa.z.data() + soa_index);
            
            // shuffles x6
            const auto l1_0 = _mm256_shuffle_ps(l0_0, l0_1, 0b10001000); // 2,0,2,0
            const auto l1_1 = _mm256_shuffle_ps(l0_2, l0_0, 0b11011000); // 3,1,2,0
            const auto l1_2 = _mm256_shuffle_ps(l0_1, l0_2, 0b11011101); // 3,1,3,1
            
            const auto l2_0 = _mm256_shuffle_ps(l1_0, l1_1, 0b10001000); // 2,0,2,0
            const auto l2_1 = _mm256_shuffle_ps(l1_2, l1_0, 0b11011000); // 3,1,2,0
            const auto l2_2 = _mm256_shuffle_ps(l1_1, l1_2, 0b11011101); // 3,1,3,1
            
            // rearrange as 128-bit chunks (expensive ops)
            const auto l3_0 = _mm256_castps256_ps128(l2_0);
            const auto l3_1 = _mm256_castps256_ps128(l2_1);
            const auto l3_2 = _mm256_castps256_ps128(l2_2);
            const auto l3_3 = _mm256_extractf128_ps(l2_0, 1);
            const auto l3_4 = _mm256_extractf128_ps(l2_1, 1);
            const auto l3_5 = _mm256_extractf128_ps(l2_2, 1);
            
            // stores
            _mm_storeu_ps(start, l3_0);
            _mm_storeu_ps(start + 4, l3_1);
            _mm_storeu_ps(start + 8, l3_2);
            _mm_storeu_ps(start + 12, l3_3);
            _mm_storeu_ps(start + 16, l3_4);
            _mm_storeu_ps(start + 20, l3_5);
        }
        else if constexpr (!is_sp)
        {
            // load 256-bit chunks
            const auto l0_0 = _mm256_loadu_pd(soa.x.data() + soa_index);
            const auto l0_1 = _mm256_loadu_pd(soa.y.data() + soa_index);
            const auto l0_2 = _mm256_loadu_pd(soa.z.data() + soa_index);
            
            // shuffles x3
            const auto l1_0 = _mm256_shuffle_pd(l0_0, l0_1, 0b00000000);
            const auto l1_1 = _mm256_shuffle_pd(l0_2, l0_0, 0b00001010);
            const auto l1_2 = _mm256_shuffle_pd(l0_1, l0_2, 0b00001111);
            
            // rearrange as 128-bit chunks (expensive ops)
            const auto l3_0 = _mm256_castpd256_pd128(l1_0);
            const auto l3_1 = _mm256_castpd256_pd128(l1_1);
            const auto l3_2 = _mm256_castpd256_pd128(l1_2);
            const auto l3_3 = _mm256_extractf128_pd(l1_0, 1);
            const auto l3_4 = _mm256_extractf128_pd(l1_1, 1);
            const auto l3_5 = _mm256_extractf128_pd(l1_2, 1);
            
            // stores
            _mm_storeu_pd(start, l3_0);
            _mm_storeu_pd(start + 2, l3_1);
            _mm_storeu_pd(start + 4, l3_2);
            _mm_storeu_pd(start + 6, l3_3);
            _mm_storeu_pd(start + 8, l3_4);
            _mm_storeu_pd(start + 10, l3_5);
        }
    }
    for (auto index = soa_index; index < aos.size(); ++index)
    {
        aos[index](0) = soa.x[index];
        aos[index](1) = soa.y[index];
        aos[index](2) = soa.z[index];
    }
}


//+//////////////////////////////////////////////
// main (test)
//+//////////////////////////////////////////////

using real_t = double;

int main()
{
    //+//////////////////////////////////////////
    // AOS to SOA
    //+//////////////////////////////////////////
    
    // initialize AOS
    auto aos = vector3_array<real_t>{};
    aos.resize(g_size);
    for (auto index = std::size_t{}; index < aos.size(); ++index)
    {
        const auto value = static_cast<real_t>(index * 3);
        aos[index] = vector3{value + static_cast<real_t>(1.0), value + static_cast<real_t>(2.0),
            value + static_cast<real_t>(3.0)};
    }
    
    // initialize SOA
    auto soa = structure_of_arrays<real_t>{};
    soa.zero();
    
    // do AOS to SOA
    aos_to_soa(aos, soa);
    
    // test
    const auto do_test = [&aos, &soa]()
    {
        for (auto index = std::size_t{}; index < aos.size(); ++index)
        {
            if (std::fabs(soa.x[index] - aos[index](0)) > 1E-6
                || std::fabs(soa.y[index] - aos[index](1)) > 1E-6
                || std::fabs(soa.z[index] - aos[index](2)) > 1E-6)
            {
                std::cerr << std::fixed << std::setprecision(7)
                    << "Deviation observed for index " << index << "; expected and computed values are: ("
                    << aos[index](0) << ", " << aos[index](1) << ", " << aos[index](2) << ") and ("
                    << soa.x[index] << ", " << soa.y[index] << ", " << soa.z[index]
                    << "). The program will be aborted." << std::endl;
                std::abort();
            }
        }
    };
    do_test();
    
    // report success
    std::cout << "Passed the AOS to SOA test!" << std::endl;
    
    //+//////////////////////////////////////////
    // SOA to AOS
    //+//////////////////////////////////////////
    
    // zero output container
    std::fill(aos.data(), aos.data() + aos.size(), vector3<real_t>{});
    
    // do SOA to AOS
    soa_to_aos(soa, aos);
    
    // test
    do_test();
    
    // report success
    std::cout << "Passed the SOA to AOS test!" << std::endl;
    
    // load 
    return 0;
}

// END OF TESTFILE
