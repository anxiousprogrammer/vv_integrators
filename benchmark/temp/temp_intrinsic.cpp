/// \file This file can be misused to test very specific things

#include "vector3.h"
#include "vector3_array.h"

#include <utility> // std::abort
#include <iostream> // std::cerr
#include <cmath> // std::fabs
#include <vector> // std::vector
#include <iomanip> // std::setprecision

#include <immintrin.h>


using namespace vvi;

int main()
{
    // initialize data
    const float a[8] = {1.f, 2.f, 3.f, 4.f,    5.f, 6.f, 7.f, 8.f};
    const float b[8] = {1.f, 2.f, 3.f, 4.f,    5.f, 6.f, 7.f, 8.f};
    const float c[8] = {1.f, 2.f, 3.f, 4.f,    5.f, 6.f, 7.f, 8.f};
    const auto a_p = _mm256_loadu_ps(a);
    
    // horizontal sum test
    const auto high = _mm256_extractf128_ps(a_p, 1);
    const auto low = _mm256_castps256_ps128(a_p);
    const auto sum_128 = _mm_add_ps(low, high);
    const auto sum_128_highx2 = _mm_movehl_ps(sum_128, sum_128);
    const auto sum_64 = _mm_add_ps(sum_128, sum_128_highx2);
    const auto sum_64_1 = _mm_shuffle_ps(sum_64, sum_64, 0x01010101); // 1,1,1,1
    const auto sum_32 = _mm_add_ps(sum_64, sum_64_1);
    const auto accumulator = _mm_cvtss_f32(sum_32);
    
    // output result
    const auto error = std::fabs(accumulator - 36.0f);
    if (error > 1E-6)
    {
        std::cout << "Test failed!";
    }
    else
    {
        std::cout << "Test passed!";
    }
    std::cout << " error: " << error << "." << std::endl;
    
    return 0;
}

// END OF TESTFILE
