/// \file This file contains the benchmark program of the kernel 'xaxpy_xaxmy'.

#include "vector3.h"

#include "tixl.h"

#include <memory> // std::malloc
#include <utility> // std::abort
#include <iostream> // std::cerr
#include <random> // std::mt19937_64
#include <cmath> // std::fabs

#ifdef USE_EXPLICIT_VEC
#include "intrinsics_helper.h"
#endif

#include <omp.h>


using namespace vvi;

//+////////////////////////////////////////////////////////////////////////////////////////////////
// implementation and test
//+////////////////////////////////////////////////////////////////////////////////////////////////

namespace benchmark
{

template<class Treal_t>
void xaxpy_xaxmy(const Treal_t& a, const vector3<Treal_t>* x, const vector3<Treal_t>* y, vector3<Treal_t>* result1,
    vector3<Treal_t>* result2, const std::size_t& size)
{
    #ifndef NDEBUG
    if (size == 0)
    {
        throw std::invalid_argument("xaxpy_xaxmy_size_zero_error");
    }
    if (x == nullptr || y == nullptr || result1 == nullptr || result2 == nullptr)
    {
        throw std::invalid_argument("xaxpy_xaxmy_null_pointer_error");
    }
    #endif
    
    #pragma omp parallel for simd safelen(16)
    for (auto index = std::size_t{}; index < size; ++index)
    {
        result1[index] = y[index] + x[index] * a; // FP: 6 FLOP; Traffic: 3+1 vector3<Treal_t>
        result2[index] = y[index] - x[index] * a; // FP: 6 FLOP; Traffic: 1+1 vector3<Treal_t>
    }
}


//+////////////////////////////////////////////////////////////////////////////////////////////////
// functor
//+////////////////////////////////////////////////////////////////////////////////////////////////

template<class Treal_t>
class exp_fn : public tixl::experiment_functor
{
    //+/////////////////
    // members
    //+/////////////////
    
    const std::size_t size_;
    Treal_t a_;
    vector3<Treal_t>* x_ = nullptr;
    vector3<Treal_t>* y_ = nullptr;
    vector3<Treal_t>* results1_ = nullptr;
    vector3<Treal_t>* results2_ = nullptr;
    
    
public:
    
    //+/////////////////
    // lifecycle
    //+/////////////////
    
    exp_fn(const std::size_t& size, const Treal_t& a)
        : size_(size), a_(a) {}
    
    
    //+/////////////////
    // main functionality
    //+/////////////////
    
    void init() final
    {
        // allocate anew and lay a view on it
        x_ = reinterpret_cast<vector3<Treal_t>*>(std::malloc(size_ * sizeof(vector3<Treal_t>)));
        y_ = reinterpret_cast<vector3<Treal_t>*>(std::malloc(size_ * sizeof(vector3<Treal_t>)));
        results1_ = reinterpret_cast<vector3<Treal_t>*>(std::malloc(size_ * sizeof(vector3<Treal_t>)));
        results2_ = reinterpret_cast<vector3<Treal_t>*>(std::malloc(size_ * sizeof(vector3<Treal_t>)));
        if (x_ == nullptr || y_ == nullptr || results1_ == nullptr || results2_ == nullptr)
        {
            std::cerr << "Fatal error: no memory could be allocated for test data!" << std::endl;
            std::abort();
        }
        
        // first touch
        #pragma omp parallel
        {
            const auto size = size_ * sizeof(vector3<Treal_t>) / sizeof(Treal_t);
            tixl::set_first_touch(reinterpret_cast<Treal_t*>(x_), size, size / omp_get_max_threads(),
                omp_get_thread_num(), omp_get_max_threads());
            tixl::set_first_touch(reinterpret_cast<Treal_t*>(y_), size, size / omp_get_max_threads(),
                omp_get_thread_num(), omp_get_max_threads());
            tixl::set_first_touch(reinterpret_cast<Treal_t*>(results1_), size, size / omp_get_max_threads(),
                omp_get_thread_num(), omp_get_max_threads());
            tixl::set_first_touch(reinterpret_cast<Treal_t*>(results2_), size, size / omp_get_max_threads(),
                omp_get_thread_num(), omp_get_max_threads());
        }
    }
    
    void perform_experiment() final
    {
        xaxpy_xaxmy(a_, x_, y_, results1_, results2_, size_);
    }
    
    void finish() final
    {
        std::free(x_);
        std::free(y_);
        std::free(results1_);
        std::free(results2_);
        x_ = nullptr;
        y_ = nullptr;
        results1_ = nullptr;
        results2_ = nullptr;
    }
};

} // namespace benchmark


//+////////////////////////////////////////////////////////////////////////////////////////////////
// main
//+////////////////////////////////////////////////////////////////////////////////////////////////

using real_t = float;

int main(int argc, char** argv)
{
    // extract the problem size
    if (argc < 2)
    {
        std::cerr << "Please provide the problem size as command line argument." << std::endl;
        return 1;
    }
    const auto n = static_cast<std::size_t>(std::stoul(argv[1]));
    if (n < 1)
    {
        std::cerr << "Zero valued problem size provided. The program will now be aborted." << std::endl;
        return 1;
    }
    const auto size = n * n * n;
    if (size * sizeof(real_t) / (1024 * 1024 * 1024) > 40)
    {
        std::cerr << "Problem size is too large. The program will now be aborted." << std::endl;
        return 1;
    }
    
    // report
    std::cout << "Starting runs with problem size n=" << n << ".\nThread count: " << omp_get_max_threads() << "."
        << std::endl;
    
    // fix the experiment count
    const auto experiment_count = std::size_t{100};
    
    // perform the experiments
    auto ef = benchmark::exp_fn<real_t>(size, static_cast<real_t>(0.4));
    const auto measurements = tixl::perform_experiments(ef, experiment_count);
    const auto stats = tixl::compute_statistics(measurements);
    tixl::output_results("Benchmarking of xaxpy_xaxmy", stats, 12 * size, 6 * size * sizeof(vector3<real_t>));
    
    return 0;
}

// END OF TESTFILE
