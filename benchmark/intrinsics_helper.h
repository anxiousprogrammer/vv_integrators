#include <immintrin.h>


#ifndef VVI_INTRINSICS_HELPER_H
#define VVI_INTRINSICS_HELPER_H

namespace vvi
{

//+////////////////////////////////////////////////////////////////////////////////////////////////
// macros for vector flags
//+////////////////////////////////////////////////////////////////////////////////////////////////

#if defined(__SSE__) && defined(__SSE2__)
#define SSE_SSE2_AVAILABLE
#endif
#if defined(__AVX__) && defined(__AVX2__)
#define AVX_AVX2_AVAILABLE
#endif
#if defined(__AVX512F__) && defined(__AVX512DQ__)
#define AVX512x_AVAILABLE
#endif


//+////////////////////////////////////////////////////////////////////////////////////////////////
// macros for vector flags
//+////////////////////////////////////////////////////////////////////////////////////////////////

template<class Treal_t>
inline void intrinsics_type_test()
{
    static_assert(std::is_same<Treal_t, float>::value || std::is_same<Treal_t, double>::value);
};


//+////////////////////////////////////////////////////////////////////////////////////////////////
// 128-bit intrinsics
//+////////////////////////////////////////////////////////////////////////////////////////////////

#ifdef SSE_SSE2_AVAILABLE

//+//////////////////////////////////////////////
// LD/ST
//+//////////////////////////////////////////////

inline __m128 loadu128_helper(const float* value)
{
    return _mm_loadu_ps(value);
}


inline __m128d loadu128_helper(const double* value)
{
    return _mm_loadu_pd(value);
}


template<class Treal_t>
struct packed128_type_helper
{
    using type = decltype(loadu128_helper(std::declval<const Treal_t*>()));
};


//+//////////////////////////////////////////////
// ADD, SUB, FMA etc
//+//////////////////////////////////////////////

template<class Treal_t>
Treal_t hsum128_helper(const typename packed128_type_helper<Treal_t>::type accumulator)
{
    intrinsics_type_test<Treal_t>();
    
    if constexpr (std::is_same<Treal_t, float>::value)
    {
        const auto accumulator_highx2 = _mm_movehl_ps(accumulator, accumulator);
        const auto sum_64 = _mm_add_ps(accumulator, accumulator_highx2);
        const auto sum_64_1 = _mm_shuffle_ps(sum_64, sum_64, 0b01010101); // 1,1,1,1
        const auto sum_32 = _mm_add_ps(sum_64, sum_64_1);
        return _mm_cvtss_f32(sum_32);
    }
    else if constexpr (std::is_same<Treal_t, double>::value)
    {
        const auto accumulator_lane_swapped = _mm_shuffle_pd(accumulator, accumulator, 0b00000001); // 0, 1
        const auto sum_64 = _mm_add_pd(accumulator, accumulator_lane_swapped);
        return _mm_cvtsd_f64(sum_64);
    }
    return {}; // control never reaches here
}

#endif // ifdef SSE_SSE2_AVAILABLE


//+////////////////////////////////////////////////////////////////////////////////////////////////
// 256-bit intrinsics
//+////////////////////////////////////////////////////////////////////////////////////////////////

#ifdef AVX_AVX2_AVAILABLE

//+//////////////////////////////////////////////
// LD/ST
//+//////////////////////////////////////////////

inline __m256 loadu256_helper(const float* value)
{
    return _mm256_loadu_ps(value);
}


inline __m256d loadu256_helper(const double* value)
{
    return _mm256_loadu_pd(value);
}


template<class Treal_t>
struct packed256_type_helper
{
    using type = decltype(loadu256_helper(std::declval<const Treal_t*>()));
};


template<class Treal_t>
inline typename packed256_type_helper<Treal_t>::type broadcast256_helper(const Treal_t* value)
{
    intrinsics_type_test<Treal_t>();
    
    if constexpr (std::is_same<Treal_t, float>::value)
    {
        return _mm256_broadcast_ss(value);
    }
    else if constexpr (std::is_same<Treal_t, double>::value)
    {
        return _mm256_broadcast_sd(value);
    }
    return typename packed256_type_helper<Treal_t>::type{}; // control never reaches here
}


template<class Treal_t>
inline void storeu256_helper(Treal_t* ptr, const typename packed256_type_helper<Treal_t>::type val)
{
    intrinsics_type_test<Treal_t>();
    
    if constexpr (std::is_same<Treal_t, float>::value)
    {
        _mm256_storeu_ps(ptr, val);
    }
    else if constexpr (std::is_same<Treal_t, double>::value)
    {
        _mm256_storeu_pd(ptr, val);
    }
}


//+//////////////////////////////////////////////
// ADD, SUB, FMA etc
//+//////////////////////////////////////////////

template<class Treal_t>
inline typename packed256_type_helper<Treal_t>::type add256_helper(const typename packed256_type_helper<Treal_t>::type v1,
    const typename packed256_type_helper<Treal_t>::type v2)
{
    intrinsics_type_test<Treal_t>();
    
    if constexpr (std::is_same<Treal_t, float>::value)
    {
        return _mm256_add_ps(v1, v2);
    }
    else if constexpr (std::is_same<Treal_t, double>::value)
    {
        return _mm256_add_pd(v1, v2);
    }
    return typename packed256_type_helper<Treal_t>::type{}; // control never reaches here
}


template<class Treal_t>
inline typename packed256_type_helper<Treal_t>::type sub256_helper(const typename packed256_type_helper<Treal_t>::type v1,
    const typename packed256_type_helper<Treal_t>::type v2)
{
    intrinsics_type_test<Treal_t>();
    
    if constexpr (std::is_same<Treal_t, float>::value)
    {
        return _mm256_sub_ps(v1, v2);
    }
    else if constexpr (std::is_same<Treal_t, double>::value)
    {
        return _mm256_sub_pd(v1, v2);
    }
    return typename packed256_type_helper<Treal_t>::type{}; // control never reaches here
}


template<class Treal_t>
inline typename packed256_type_helper<Treal_t>::type mul256_helper(const typename packed256_type_helper<Treal_t>::type v1,
    const typename packed256_type_helper<Treal_t>::type v2)
{
    intrinsics_type_test<Treal_t>();
    
    if constexpr (std::is_same<Treal_t, float>::value)
    {
        return _mm256_mul_ps(v1, v2);
    }
    else if constexpr (std::is_same<Treal_t, double>::value)
    {
        return _mm256_mul_pd(v1, v2);
    }
    return typename packed256_type_helper<Treal_t>::type{}; // control never reaches here
}


template<class Treal_t>
inline typename packed256_type_helper<Treal_t>::type fmadd256_helper(const typename packed256_type_helper<Treal_t>::type v1,
    const typename packed256_type_helper<Treal_t>::type v2, const typename packed256_type_helper<Treal_t>::type v3)
{
    intrinsics_type_test<Treal_t>();
    
    if constexpr (std::is_same<Treal_t, float>::value)
    {
        return _mm256_fmadd_ps(v1, v2, v3);
    }
    else if constexpr (std::is_same<Treal_t, double>::value)
    {
        return _mm256_fmadd_pd(v1, v2, v3);
    }
    return typename packed256_type_helper<Treal_t>::type{}; // control never reaches here
}


template<class Treal_t>
inline typename packed256_type_helper<Treal_t>::type fnmadd256_helper(const typename packed256_type_helper<Treal_t>::type v1,
    const typename packed256_type_helper<Treal_t>::type v2, const typename packed256_type_helper<Treal_t>::type v3)
{
    intrinsics_type_test<Treal_t>();
    
    if constexpr (std::is_same<Treal_t, float>::value)
    {
        return _mm256_fnmadd_ps(v1, v2, v3);
    }
    else if constexpr (std::is_same<Treal_t, double>::value)
    {
        return _mm256_fnmadd_pd(v1, v2, v3);
    }
    return typename packed256_type_helper<Treal_t>::type{}; // control never reaches here
}


template<class Treal_t>
Treal_t hsum256_helper(const typename packed256_type_helper<Treal_t>::type accumulator)
{
    intrinsics_type_test<Treal_t>();
    
    if constexpr (std::is_same<Treal_t, float>::value)
    {
        const auto high = _mm256_extractf128_ps(accumulator, 1);
        const auto low = _mm256_castps256_ps128(accumulator);
        const auto sum_128 = _mm_add_ps(low, high);
        return hsum128_helper<Treal_t>(sum_128);
    }
    else if constexpr (std::is_same<Treal_t, double>::value)
    {
        const auto high = _mm256_extractf128_pd(accumulator, 1);
        const auto low = _mm256_castpd256_pd128(accumulator);
        const auto sum_128 = _mm_add_pd(low, high);
        return hsum128_helper<Treal_t>(sum_128);
    }
    return {}; // control never reaches here
}

#endif // ifdef AVX_AVX2_AVAILABLE


//+////////////////////////////////////////////////////////////////////////////////////////////////
// 512-bit intrinsics
//+////////////////////////////////////////////////////////////////////////////////////////////////

#ifdef AVX512x_AVAILABLE

//+//////////////////////////////////////////////
// LD/ST
//+//////////////////////////////////////////////

inline __m512 loadu512_helper(const float* stream)
{
    return _mm512_loadu_ps(stream);
}


inline __m512d loadu512_helper(const double* stream)
{
    return _mm512_loadu_pd(stream);
}


template<class Treal_t>
struct packed512_type_helper
{
    using type = decltype(loadu512_helper(std::declval<const Treal_t*>()));
};


template<class Treal_t>
inline typename packed512_type_helper<Treal_t>::type broadcast512_helper(const Treal_t* value)
{
    intrinsics_type_test<Treal_t>();
    
    if constexpr (std::is_same<Treal_t, float>::value)
    {
        return _mm512_broadcastss_ps(_mm_broadcast_ss(value));
    }
    else if constexpr (std::is_same<Treal_t, double>::value)
    {
        return _mm512_broadcastsd_pd(_mm_set1_pd(*value)); // TODO: there's no equivalent of _mm_broadcast_pd!
    }
    return typename packed512_type_helper<Treal_t>::type{}; // control never reaches here
}


template<class Treal_t>
inline void storeu512_helper(Treal_t* ptr, const typename packed512_type_helper<Treal_t>::type val)
{
    intrinsics_type_test<Treal_t>();
    
    if constexpr (std::is_same<Treal_t, float>::value)
    {
        _mm512_storeu_ps(ptr, val);
    }
    else if constexpr (std::is_same<Treal_t, double>::value)
    {
        _mm512_storeu_pd(ptr, val);
    }
}


//+//////////////////////////////////////////////
// ADD, SUB, FMA etc
//+//////////////////////////////////////////////

template<class Treal_t>
inline typename packed512_type_helper<Treal_t>::type add512_helper(const typename packed512_type_helper<Treal_t>::type v1,
    const typename packed512_type_helper<Treal_t>::type v2)
{
    intrinsics_type_test<Treal_t>();
    
    if constexpr (std::is_same<Treal_t, float>::value)
    {
        return _mm512_add_ps(v1, v2);
    }
    else if constexpr (std::is_same<Treal_t, double>::value)
    {
        return _mm512_add_pd(v1, v2);
    }
    return typename packed512_type_helper<Treal_t>::type{}; // control never reaches here
}


template<class Treal_t>
inline typename packed512_type_helper<Treal_t>::type sub512_helper(const typename packed512_type_helper<Treal_t>::type v1,
    const typename packed512_type_helper<Treal_t>::type v2)
{
    intrinsics_type_test<Treal_t>();
    
    if constexpr (std::is_same<Treal_t, float>::value)
    {
        return _mm512_sub_ps(v1, v2);
    }
    else if constexpr (std::is_same<Treal_t, double>::value)
    {
        return _mm512_sub_pd(v1, v2);
    }
    return typename packed512_type_helper<Treal_t>::type{}; // control never reaches here
}


template<class Treal_t>
inline typename packed512_type_helper<Treal_t>::type mul512_helper(const typename packed512_type_helper<Treal_t>::type v1,
    const typename packed512_type_helper<Treal_t>::type v2)
{
    intrinsics_type_test<Treal_t>();
    
    if constexpr (std::is_same<Treal_t, float>::value)
    {
        return _mm512_mul_ps(v1, v2);
    }
    else if constexpr (std::is_same<Treal_t, double>::value)
    {
        return _mm512_mul_pd(v1, v2);
    }
    return typename packed512_type_helper<Treal_t>::type{}; // control never reaches here
}


template<class Treal_t>
inline typename packed512_type_helper<Treal_t>::type fmadd512_helper(const typename packed512_type_helper<Treal_t>::type v1,
    const typename packed512_type_helper<Treal_t>::type v2, const typename packed512_type_helper<Treal_t>::type v3)
{
    intrinsics_type_test<Treal_t>();
    
    if constexpr (std::is_same<Treal_t, float>::value)
    {
        return _mm512_fmadd_ps(v1, v2, v3);
    }
    else if constexpr (std::is_same<Treal_t, double>::value)
    {
        return _mm512_fmadd_pd(v1, v2, v3);
    }
    return typename packed512_type_helper<Treal_t>::type{}; // control never reaches here
}


template<class Treal_t>
inline typename packed512_type_helper<Treal_t>::type fnmadd512_helper(const typename packed512_type_helper<Treal_t>::type v1,
    const typename packed512_type_helper<Treal_t>::type v2, const typename packed512_type_helper<Treal_t>::type v3)
{
    intrinsics_type_test<Treal_t>();
    
    if constexpr (std::is_same<Treal_t, float>::value)
    {
        return _mm512_fnmadd_ps(v1, v2, v3);
    }
    else if constexpr (std::is_same<Treal_t, double>::value)
    {
        return _mm512_fnmadd_pd(v1, v2, v3);
    }
    return typename packed512_type_helper<Treal_t>::type{}; // control never reaches here
}


template<class Treal_t>
inline Treal_t hsum512_helper(const typename packed512_type_helper<Treal_t>::type accumulator)
{
    intrinsics_type_test<Treal_t>();
    
    if constexpr (std::is_same<Treal_t, float>::value)
    {
        const auto high = _mm512_extractf32x8_ps(accumulator, 1);
        const auto low = _mm512_castps512_ps256(accumulator);
        const auto sum_256 = _mm256_add_ps(high, low);
        return hsum256_helper<Treal_t>(sum_256);
    }
    else if constexpr (std::is_same<Treal_t, double>::value)
    {
        const auto high = _mm512_extractf64x4_pd(accumulator, 1);
        const auto low = _mm512_castpd512_pd256(accumulator);
        const auto sum_256 = _mm256_add_pd(high, low);
        return hsum256_helper<Treal_t>(sum_256);
    }
    return {}; // control never reaches here
}

#endif // ifdef AVX512x_AVAILABLE


//+////////////////////////////////////////////////////////////////////////////////////////////////
// automatic AVX instructions
//+////////////////////////////////////////////////////////////////////////////////////////////////

#if defined(VVI_USE_AVX2)

#ifndef AVX_AVX2_AVAILABLE
#error "The required AVX2 flag(s) are unavailable on this CPU!"
#endif
#define MAX_SIMD_WIDTH 256

#elif defined(VVI_USE_AVX512)

#ifndef AVX512x_AVAILABLE
#error "The required AVX512 flag(s) are unavailable on this CPU!"
#endif
#define MAX_SIMD_WIDTH 512

#else // choose the widest

#if defined(AVX512x_AVAILABLE)
#define MAX_SIMD_WIDTH 512
#elif defined(AVX_AVX2_AVAILABLE)
#define MAX_SIMD_WIDTH 256
#else
#error "Required AVX2 and/or AVX512 flag(s) are unavailable on this CPU!"
#endif

#endif

//+//////////////////////////////////////////////
// LD/ST
//+//////////////////////////////////////////////

template<class Treal_t>
struct avx2_or_avx512_packed_type_helper
{
    #if MAX_SIMD_WIDTH == 512
    using type = decltype(loadu512_helper(std::declval<const Treal_t*>()));
    #elif MAX_SIMD_WIDTH == 256
    using type = decltype(loadu256_helper(std::declval<const Treal_t*>()));
    #endif
};


template<class Treal_t>
inline typename avx2_or_avx512_packed_type_helper<Treal_t>::type avx2_or_avx512_unaligned_load_helper(const Treal_t* stream)
{
    #if MAX_SIMD_WIDTH == 512
    return loadu512_helper(stream);
    #elif MAX_SIMD_WIDTH == 256
    return loadu256_helper(stream);
    #endif
}


template<class Treal_t>
inline typename avx2_or_avx512_packed_type_helper<Treal_t>::type avx2_or_avx512_broadcast_helper(const Treal_t* stream)
{
    #if MAX_SIMD_WIDTH == 512
    return broadcast512_helper(stream);
    #elif MAX_SIMD_WIDTH == 256
    return broadcast256_helper(stream);
    #endif
}


template<class Treal_t>
inline void avx2_or_avx512_unaligned_store_helper(Treal_t* ptr,
    const typename avx2_or_avx512_packed_type_helper<Treal_t>::type value)
{
    #if MAX_SIMD_WIDTH == 512
    storeu512_helper(ptr, value);
    #elif MAX_SIMD_WIDTH == 256
    storeu256_helper(ptr, value);
    #endif
}


//+//////////////////////////////////////////////
// ADD, SUB, FMA etc
//+//////////////////////////////////////////////

template<class Treal_t>
inline typename avx2_or_avx512_packed_type_helper<Treal_t>::type avx2_or_avx512_add_helper(
    const typename avx2_or_avx512_packed_type_helper<Treal_t>::type v1,
    const typename avx2_or_avx512_packed_type_helper<Treal_t>::type v2)
{
    #if MAX_SIMD_WIDTH == 512
    return add512_helper<Treal_t>(v1, v2);
    #elif MAX_SIMD_WIDTH == 256
    return add256_helper<Treal_t>(v1, v2);
    #endif
}


template<class Treal_t>
inline typename avx2_or_avx512_packed_type_helper<Treal_t>::type avx2_or_avx512_sub_helper(
    const typename avx2_or_avx512_packed_type_helper<Treal_t>::type v1,
    const typename avx2_or_avx512_packed_type_helper<Treal_t>::type v2)
{
    #if MAX_SIMD_WIDTH == 512
    return sub512_helper<Treal_t>(v1, v2);
    #elif MAX_SIMD_WIDTH == 256
    return sub256_helper<Treal_t>(v1, v2);
    #endif
}


template<class Treal_t>
inline typename avx2_or_avx512_packed_type_helper<Treal_t>::type avx2_or_avx512_mul_helper(
    const typename avx2_or_avx512_packed_type_helper<Treal_t>::type v1,
    const typename avx2_or_avx512_packed_type_helper<Treal_t>::type v2)
{
    #if MAX_SIMD_WIDTH == 512
    return mul512_helper<Treal_t>(v1, v2);
    #elif MAX_SIMD_WIDTH == 256
    return mul256_helper<Treal_t>(v1, v2);
    #endif
}


template<class Treal_t>
inline typename avx2_or_avx512_packed_type_helper<Treal_t>::type avx2_or_avx512_fmadd_helper(
    const typename avx2_or_avx512_packed_type_helper<Treal_t>::type v1,
    const typename avx2_or_avx512_packed_type_helper<Treal_t>::type v2,
    const typename avx2_or_avx512_packed_type_helper<Treal_t>::type v3)
{
    #if MAX_SIMD_WIDTH == 512
    return fmadd512_helper<Treal_t>(v1, v2, v3);
    #elif MAX_SIMD_WIDTH == 256
    return fmadd256_helper<Treal_t>(v1, v2, v3);
    #endif
}


template<class Treal_t>
inline typename avx2_or_avx512_packed_type_helper<Treal_t>::type avx2_or_avx512_fnmadd_helper(
    const typename avx2_or_avx512_packed_type_helper<Treal_t>::type v1,
    const typename avx2_or_avx512_packed_type_helper<Treal_t>::type v2,
    const typename avx2_or_avx512_packed_type_helper<Treal_t>::type v3)
{
    #if MAX_SIMD_WIDTH == 512
    return fnmadd512_helper<Treal_t>(v1, v2, v3);
    #elif MAX_SIMD_WIDTH == 256
    return fnmadd256_helper<Treal_t>(v1, v2, v3);
    #endif
}


template<class Treal_t>
inline Treal_t avx2_or_avx512_hsum_helper(const typename avx2_or_avx512_packed_type_helper<Treal_t>::type accumulator)
{
    #if MAX_SIMD_WIDTH == 512
    return hsum512_helper<Treal_t>(accumulator);
    #elif MAX_SIMD_WIDTH == 256
    return hsum256_helper<Treal_t>(accumulator);
    #endif
}

} // namespace vvi

#endif
