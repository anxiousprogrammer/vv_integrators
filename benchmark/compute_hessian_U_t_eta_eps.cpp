/// \file This file contains the benchmark program of the kernel 'compute_hessian_U_t_eta_eps'.

#include "vector3.h"

#include "tixl.h"

#include <memory> // std::malloc
#include <utility> // std::abort
#include <iostream> // std::cerr
#include <random> // std::mt19937_64
#include <cmath> // std::fabs

#ifdef USE_EXPLICIT_VEC
#include "intrinsics_helper.h"
#endif

#include <omp.h>


using namespace vvi;

//+////////////////////////////////////////////////////////////////////////////////////////////////
// implementation and test
//+////////////////////////////////////////////////////////////////////////////////////////////////

namespace benchmark
{

template<class Treal_t>
void compute_hessian_U_t_eta_eps(const vector3<Treal_t>* forces_plus, const vector3<Treal_t>* forces_minus,
    vector3<Treal_t>* hessian_U_t_eta_eps, const std::size_t& size, const Treal_t& eps)
{
    #ifndef NDEBUG
    if (size == 0)
    {
        throw std::invalid_argument("compute_hessian_U_t_eta_eps_zero_size_error");
    }
    if (forces_plus == nullptr || forces_minus == nullptr || hessian_U_t_eta_eps == nullptr)
    {
        throw std::invalid_argument("compute_hessian_U_t_eta_eps_null_pointer_error");
    }
    constexpr auto zero_tolerance = std::is_same<Treal_t, float>::value ? static_cast<float>(1E-6) : 1E-13;
    if (eps < zero_tolerance)
    {
        throw std::invalid_argument("compute_hessian_U_t_eta_eps_invalid_eps_error");
    }
    #endif
    
    const auto eps_t_2_inv = static_cast<Treal_t>(0.5) / eps;
    #pragma omp parallel for simd safelen(16)
    for (auto particle_index = std::size_t{}; particle_index < size; ++particle_index)
    {
        hessian_U_t_eta_eps[particle_index] = (forces_plus[particle_index] - forces_minus[particle_index]) * eps_t_2_inv;
        // FP: 6 FLOP; Traffic: 3+1 vector3<Treal_t>
    }
}


//+////////////////////////////////////////////////////////////////////////////////////////////////
// functor
//+////////////////////////////////////////////////////////////////////////////////////////////////

template<class Treal_t>
class exp_fn : public tixl::experiment_functor
{
    //+/////////////////
    // members
    //+/////////////////
    
    const std::size_t size_;
    vector3<Treal_t>* forces_plus_ = nullptr;
    vector3<Treal_t>* forces_minus_ = nullptr;
    vector3<Treal_t>* hessian_U_t_eta_eps_ = nullptr;
    const Treal_t eps_ = std::is_same<Treal_t, float>::value ? static_cast<float>(1E-3) : 1E-6;
    
    
public:
    
    //+/////////////////
    // lifecycle
    //+/////////////////
    
    exp_fn(const std::size_t& size)
        : size_(size) {}
    
    
    //+/////////////////
    // main functionality
    //+/////////////////
    
    void init() final
    {
        // allocate anew and lay a view on it
        forces_plus_ = reinterpret_cast<vector3<Treal_t>*>(std::malloc(size_ * sizeof(vector3<Treal_t>)));
        forces_minus_ = reinterpret_cast<vector3<Treal_t>*>(std::malloc(size_ * sizeof(vector3<Treal_t>)));
        hessian_U_t_eta_eps_ = reinterpret_cast<vector3<Treal_t>*>(std::malloc(size_ * sizeof(vector3<Treal_t>)));
        if (forces_plus_ == nullptr || forces_minus_ == nullptr || hessian_U_t_eta_eps_ == nullptr)
        {
            std::cerr << "Fatal error: no memory could be allocated for test data!" << std::endl;
            std::abort();
        }
        
        // first touch
        #pragma omp parallel
        {
            const auto size = size_ * sizeof(vector3<Treal_t>) / sizeof(Treal_t);
            tixl::set_first_touch(reinterpret_cast<Treal_t*>(forces_plus_), size, size / omp_get_max_threads(),
                omp_get_thread_num(), omp_get_max_threads());
            tixl::set_first_touch(reinterpret_cast<Treal_t*>(forces_minus_), size, size / omp_get_max_threads(),
                omp_get_thread_num(), omp_get_max_threads());
            tixl::set_first_touch(reinterpret_cast<Treal_t*>(hessian_U_t_eta_eps_), size, size / omp_get_max_threads(),
                omp_get_thread_num(), omp_get_max_threads());
        }
    }
    
    void perform_experiment() final
    {
        compute_hessian_U_t_eta_eps(forces_plus_, forces_minus_, hessian_U_t_eta_eps_, size_, eps_);
    }
    
    void finish() final
    {
        std::free(forces_plus_);
        std::free(forces_minus_);
        std::free(hessian_U_t_eta_eps_);
        forces_plus_ = nullptr;
        forces_minus_ = nullptr;
        hessian_U_t_eta_eps_ = nullptr;
    }
};

} // namespace benchmark


//+////////////////////////////////////////////////////////////////////////////////////////////////
// main
//+////////////////////////////////////////////////////////////////////////////////////////////////

using real_t = float;

int main(int argc, char** argv)
{
    // extract the problem size
    if (argc < 2)
    {
        std::cerr << "Please provide the problem size as command line argument." << std::endl;
        return 1;
    }
    const auto n = static_cast<std::size_t>(std::stoul(argv[1]));
    if (n < 1)
    {
        std::cerr << "Zero valued problem size provided. The program will now be aborted." << std::endl;
        return 1;
    }
    const auto size = n * n * n;
    if (size * sizeof(real_t) / (1024 * 1024 * 1024) > 40)
    {
        std::cerr << "Problem size is too large. The program will now be aborted." << std::endl;
        return 1;
    }
    
    // report
    std::cout << "Starting runs with problem size n=" << n << ".\nThread count: " << omp_get_max_threads() << "."
        << std::endl;
    
    // fix the experiment count
    const auto experiment_count = std::size_t{100};
    
    // perform the experiments
    auto ef = benchmark::exp_fn<real_t>(size);
    const auto measurements = tixl::perform_experiments(ef, experiment_count);
    const auto stats = tixl::compute_statistics(measurements);
    tixl::output_results("Benchmarking of compute_hessian_U_t_eta_eps", stats, 6 * size, 4 * size * sizeof(vector3<real_t>));
    
    return 0;
}

// END OF TESTFILE
