/// \file This file contains the benchmark program of the kernel 'compute_etas'.

#include "vector3.h"

#include "tixl.h"

#include <memory> // std::malloc
#include <utility> // std::abort
#include <iostream> // std::cerr
#include <random> // std::mt19937_64
#include <cmath> // std::fabs

#ifdef USE_EXPLICIT_VEC
#include "intrinsics_helper.h"
#endif

#include <omp.h>


using namespace vvi;

//+////////////////////////////////////////////////////////////////////////////////////////////////
// implementation and test
//+////////////////////////////////////////////////////////////////////////////////////////////////

namespace benchmark
{

template<class Treal_t>
void compute_etas(const vector3<Treal_t>* positions_right, const vector3<Treal_t>* positions, const vector3<Treal_t>* forces,
    const Treal_t* inv_masses, vector3<Treal_t>* etas, const std::size_t& size, const Treal_t& dt_bd_b_gamma_bd)
{
    #ifndef NDEBUG
    if (size == 0)
    {
        throw std::invalid_argument("compute_etas_size_zero_error");
    }
    if (positions == nullptr || positions_right == nullptr || forces == nullptr || inv_masses == nullptr || etas == nullptr)
    {
        throw std::invalid_argument("compute_etas_null_pointer_error");
    }
    constexpr auto zero = std::is_same<Treal_t, float>::value ? static_cast<Treal_t>(1E-6) : static_cast<Treal_t>(1E-13);
    if (dt_bd_b_gamma_bd < zero)
    {
        throw std::invalid_argument("compute_etas_zero_dt_bd_b_gamma_bd_error");
    }
    #endif
    
    #pragma omp parallel for simd safelen(16)
    for (auto particle_index = std::size_t{}; particle_index < size; ++particle_index)
    {
        etas[particle_index] = positions_right[particle_index] - positions[particle_index]
            - forces[particle_index] * inv_masses[particle_index] * dt_bd_b_gamma_bd;
        // FP: 12 FLOP; Traffic: 13+3 Treal_t
    }
}


//+////////////////////////////////////////////////////////////////////////////////////////////////
// functor
//+////////////////////////////////////////////////////////////////////////////////////////////////

template<class Treal_t>
class exp_fn : public tixl::experiment_functor
{
    //+/////////////////
    // members
    //+/////////////////
    
    const std::size_t size_;
    vector3<Treal_t>* positions_right_ = nullptr;
    vector3<Treal_t>* positions_ = nullptr;
    vector3<Treal_t>* forces_ = nullptr;
    Treal_t* inv_masses_ = nullptr;
    vector3<Treal_t>* etas_ = nullptr;
    Treal_t dt_bd_b_gamma_bd_;
    
    
public:
    
    //+/////////////////
    // lifecycle
    //+/////////////////
    
    exp_fn(const std::size_t& size, const Treal_t& dt_bd_b_gamma_bd)
        : size_(size), dt_bd_b_gamma_bd_(dt_bd_b_gamma_bd) {}
    
    //+/////////////////
    // main functionality
    //+/////////////////
    
    void init() final
    {
        // allocate anew and lay a view on it
        positions_right_ = reinterpret_cast<vector3<Treal_t>*>(std::malloc(size_ * sizeof(vector3<Treal_t>)));
        positions_ = reinterpret_cast<vector3<Treal_t>*>(std::malloc(size_ * sizeof(vector3<Treal_t>)));
        forces_ = reinterpret_cast<vector3<Treal_t>*>(std::malloc(size_ * sizeof(vector3<Treal_t>)));
        inv_masses_ = reinterpret_cast<Treal_t*>(std::malloc(size_ * sizeof(Treal_t)));
        etas_ = reinterpret_cast<vector3<Treal_t>*>(std::malloc(size_ * sizeof(vector3<Treal_t>)));
        if (positions_right_ == nullptr || positions_ == nullptr || forces_ == nullptr || inv_masses_ == nullptr
            || etas_ == nullptr)
        {
            std::cerr << "Fatal error: no memory could be allocated for test data!" << std::endl;
            std::abort();
        }
        
        // first touch
        #pragma omp parallel
        {
            const auto vector3_size = size_ * sizeof(vector3<Treal_t>) / sizeof(Treal_t);
            const auto vector3_chunk_size = vector3_size / omp_get_max_threads();
            tixl::set_first_touch(reinterpret_cast<Treal_t*>(positions_right_), vector3_size, vector3_chunk_size,
                omp_get_thread_num(), omp_get_max_threads());
            tixl::set_first_touch(reinterpret_cast<Treal_t*>(positions_), vector3_size, vector3_chunk_size,
                omp_get_thread_num(), omp_get_max_threads());
            tixl::set_first_touch(reinterpret_cast<Treal_t*>(forces_), vector3_size, vector3_chunk_size, omp_get_thread_num(),
                omp_get_max_threads());
            tixl::set_first_touch(reinterpret_cast<Treal_t*>(inv_masses_), size_, size_ / omp_get_max_threads(), omp_get_thread_num(),
                omp_get_max_threads());
            tixl::set_first_touch(reinterpret_cast<Treal_t*>(etas_), vector3_size, vector3_chunk_size, omp_get_thread_num(),
                omp_get_max_threads());
        }
    }
    
    void perform_experiment() final
    {
        compute_etas(positions_right_, positions_, forces_, inv_masses_, etas_, size_, dt_bd_b_gamma_bd_);
    }
    
    void finish() final
    {
        std::free(positions_right_);
        std::free(positions_);
        std::free(forces_);
        std::free(inv_masses_);
        std::free(etas_);
        positions_right_ = nullptr;
        positions_ = nullptr;
        forces_ = nullptr;
        inv_masses_ = nullptr;
        etas_ = nullptr;
    }
};

} // namespace benchmark


//+////////////////////////////////////////////////////////////////////////////////////////////////
// main
//+////////////////////////////////////////////////////////////////////////////////////////////////

using real_t = float;

int main(int argc, char** argv)
{
    // extract the problem size
    if (argc < 2)
    {
        std::cerr << "Please provide the problem size as command line argument." << std::endl;
        return 1;
    }
    const auto n = static_cast<std::size_t>(std::stoul(argv[1]));
    if (n < 1)
    {
        std::cerr << "Zero valued problem size provided. The program will now be aborted." << std::endl;
        return 1;
    }
    const auto size = n * n * n;
    if (size * sizeof(real_t) / (1024 * 1024 * 1024) > 40)
    {
        std::cerr << "Problem size is too large. The program will now be aborted." << std::endl;
        return 1;
    }
    
    // report
    std::cout << "Starting runs with problem size n=" << n << ".\nThread count: " << omp_get_max_threads() << "."
        << std::endl;
    
    // fix the experiment count
    const auto experiment_count = std::size_t{100};
    
    // perform the experiments
    auto ef = benchmark::exp_fn<real_t>(size, static_cast<real_t>(0.1));
    const auto measurements = tixl::perform_experiments(ef, experiment_count);
    const auto stats = tixl::compute_statistics(measurements);
    tixl::output_results("Benchmarking of compute_etas", stats, 12 * size, 16 * size * sizeof(real_t));
    
    return 0;
}

// END OF TESTFILE
