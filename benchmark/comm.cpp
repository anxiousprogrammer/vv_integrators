/// \file This file contains the benchmark program of the compute etas.

#include "vector3.h"

#include "tixl_w_mpi.h"

#include <cstdlib> // std::malloc
#include <type_traits> // std::is_same
#include <iostream> // std::cerr
#include <array> // std::array

#include <mpi.h>
#include <omp.h>


//+////////////////////////////////////////////////////////////////////////////////////////////////
// Functor
//+////////////////////////////////////////////////////////////////////////////////////////////////

namespace benchmark
{

template<class Treal_t>
class exp_fn : public tixl::experiment_functor
{
    //+/////////////////
    // members
    //+/////////////////
    
    const std::size_t N_;
    const int comm_rank_;
    const int comm_size_;
    vvi::vector3<Treal_t>* send_data_ = nullptr;
    vvi::vector3<Treal_t>* recv_data_ = nullptr;
    
    
public:
    
    //+/////////////////
    // lifecycle
    //+/////////////////
    
    exp_fn(const std::size_t& N, const int comm_rank, const int comm_size)
        : N_(N), comm_rank_(comm_rank), comm_size_(comm_size) {}
    
    //+/////////////////
    // main functionality
    //+/////////////////
    
    void init() final
    {
        // allocate anew and lay a view on it
        send_data_ = reinterpret_cast<vvi::vector3<Treal_t>*>(std::malloc(N_ * sizeof(vvi::vector3<Treal_t>)));
        recv_data_ = reinterpret_cast<vvi::vector3<Treal_t>*>(std::malloc(N_ * sizeof(vvi::vector3<Treal_t>)));
        if (send_data_ == nullptr || recv_data_ == nullptr)
        {
            if (comm_rank_ == 0)
            {
                std::cerr << "Fatal error: Test data could not be allocated!" << std::endl;
            }
            MPI_Abort(MPI_COMM_WORLD, MPI_ERR_UNKNOWN);
        }
        
        // first touch
        #pragma omp parallel
        {
            const auto size = sizeof(vvi::vector3<Treal_t>) / sizeof(Treal_t) * N_;
            tixl::set_first_touch(reinterpret_cast<Treal_t*>(send_data_), size, size / omp_get_max_threads(),
                omp_get_thread_num(), omp_get_max_threads());
            tixl::set_first_touch(reinterpret_cast<Treal_t*>(recv_data_), size, size / omp_get_max_threads(),
                omp_get_thread_num(), omp_get_max_threads());
        }
    }
    
    void perform_experiment() final
    {
        // send/receive ring
        static constexpr auto mpi_datatype = std::is_same<Treal_t, float>::value ? MPI_FLOAT : MPI_DOUBLE;
        auto request_counter = int{};
        auto requests = std::array<MPI_Request, 2>{};
        const auto size = sizeof(vvi::vector3<Treal_t>) / sizeof(Treal_t) * N_;
        if (comm_rank_ != 0)
        {
            MPI_Isend(reinterpret_cast<const Treal_t*>(send_data_), size, mpi_datatype, comm_rank_ - 1, 0, MPI_COMM_WORLD,
                &requests[request_counter]);
            ++request_counter;
        }
        if (comm_rank_ != comm_size_ - 1)
        {
            MPI_Irecv(reinterpret_cast<Treal_t*>(recv_data_), size, mpi_datatype, comm_rank_ + 1, 0, MPI_COMM_WORLD,
                &requests[request_counter]);
            ++request_counter;
        }
        
        // wait
        MPI_Waitall(request_counter, requests.data(), MPI_STATUSES_IGNORE);
    }
    
    void finish() final
    {
        std::free(send_data_);
        std::free(recv_data_);
    }
};

} // namespace benchmark


//+////////////////////////////////////////////////////////////////////////////////////////////////
// main
//+////////////////////////////////////////////////////////////////////////////////////////////////

using real_t = double;

int main(int argc, char** argv)
{
    // MPI init
    auto provided = int{};
    MPI_Init_thread(&argc, &argv, MPI_THREAD_FUNNELED, &provided);
    if (provided < MPI_THREAD_FUNNELED)
    {
        std::cerr << "Fatal error: Multi-threading support not available with the current MPI implementation." << std::endl;
        MPI_Abort(MPI_COMM_WORLD, MPI_ERR_UNKNOWN);
    }
    
    // comm. details
    auto comm_rank = -1;
    MPI_Comm_rank(MPI_COMM_WORLD, &comm_rank);
    auto comm_size = -1;
    MPI_Comm_size(MPI_COMM_WORLD, &comm_size);
    
    // extract the problem size
    if (argc < 2)
    {
        if (comm_rank == 0)
        {
            std::cerr << "Please provide the system size as command line argument." << std::endl;
        }
        MPI_Abort(MPI_COMM_WORLD, MPI_ERR_UNKNOWN);
    }
    const auto N = static_cast<std::size_t>(std::stoul(argv[1]));
    if (N < 1)
    {
        if (comm_rank == 0)
        {
            std::cerr << "Zero valued count(s) provided. The program will now be aborted." << std::endl;
        }
        MPI_Abort(MPI_COMM_WORLD, MPI_ERR_UNKNOWN);
    }
    static constexpr auto sizeofvector3 = sizeof(vvi::vector3<real_t>);
    static const auto gigabyte_in_real_t = 1024 * 1024 * 1024 / sizeofvector3;
    if (N > 25 * gigabyte_in_real_t)
    {
        if (comm_rank == 0)
        {
            std::cerr << "System size is too large. The program will now be aborted." << std::endl;
        }
        MPI_Abort(MPI_COMM_WORLD, MPI_ERR_UNKNOWN);
    }
    
    // report
    if (comm_rank == 0)
    {
        std::cout << "Starting runs with system size N=" << N << " and comm. size P=" << comm_size << ".\nThread count: "
            << omp_get_max_threads() << "." << std::endl;
    }
    
    // fix the experiment count
    const auto experiment_count = std::size_t{100};
    
    // perform the experiments
    auto ef = benchmark::exp_fn<real_t>(N, comm_rank, comm_size);
    const auto measurements = tixl::mpi_perform_experiments(MPI_COMM_WORLD, ef, experiment_count);
    const auto stats = tixl::mpi_compute_statistics(MPI_COMM_WORLD, measurements);
    if (comm_rank == 0)
    {
        tixl::write_measurements(measurements, std::string("comm_measurements_N") + std::to_string(N) + std::string("_P")
            + std::to_string(comm_size) + std::string(".txt"));
        tixl::output_results("Benchmarking of communication", stats, 0, N * sizeofvector3);
    }
    
    // finalize
    MPI_Finalize();
    
    return 0;
}

// END OF TESTFILE
