/// \file This file contains the benchmark program of the kernel 'accumulate_eta_sq_t_mass'.

#include "vector3.h"

#include "tixl.h"

#include <memory> // std::malloc
#include <utility> // std::abort
#include <iostream> // std::cerr
#include <cmath> // std::fabs

#ifdef USE_EXPLICIT_VEC
#include "intrinsics_helper.h"
#endif

#include <omp.h>


using namespace vvi;

//+////////////////////////////////////////////////////////////////////////////////////////////////
// implementation and test
//+////////////////////////////////////////////////////////////////////////////////////////////////

namespace benchmark
{

template<class Treal_t>
double accumulate_eta_sq_t_mass(const vector3<Treal_t>* etas, const Treal_t* masses, const std::size_t& size)
{
    #ifndef NDEBUG
    if (size == 0)
    {
        throw std::invalid_argument("accumulate_eta_sq_t_mass_size_zero_error");
    }
    if (masses == nullptr || etas == nullptr)
    {
        throw std::invalid_argument("accumulate_eta_sq_t_mass_null_pointer_error");
    }
    #endif
    
    auto accumulator = 0.0;
    #pragma omp parallel for simd safelen(16) reduction(+:accumulator)
    for (auto particle_index = std::size_t{}; particle_index < size; ++particle_index)
    {
        const auto& x = etas[particle_index](0);
        const auto& y = etas[particle_index](1);
        const auto& z = etas[particle_index](2);
        accumulator += (x * x + y * y + z * z) * masses[particle_index]; // FP: 7 FLOP; Traffic: 4 Treal_t
    }
    
    return accumulator;
}


//+////////////////////////////////////////////////////////////////////////////////////////////////
// functor
//+////////////////////////////////////////////////////////////////////////////////////////////////

template<class Treal_t>
class exp_fn : public tixl::experiment_functor
{
    //+/////////////////
    // members
    //+/////////////////
    
    const std::size_t size_;
    vector3<Treal_t>* etas_ = nullptr;
    Treal_t* masses_ = nullptr;

    
public:
    
    //+/////////////////
    // lifecycle
    //+/////////////////
    
    exp_fn(const std::size_t& size)
        : size_(size) {}
    
    //+/////////////////
    // main functionality
    //+/////////////////
    
    void init() final
    {
        // allocate anew and lay a view on it
        etas_ = reinterpret_cast<vector3<Treal_t>*>(std::malloc(size_ * sizeof(vector3<Treal_t>)));
        masses_ = reinterpret_cast<Treal_t*>(std::malloc(size_ * sizeof(Treal_t)));
        if (etas_ == nullptr || masses_ == nullptr)
        {
            std::cerr << "Fatal error: no memory could be allocated for test data!" << std::endl;
            std::abort();
        }
        
        // first touch
        #pragma omp parallel
        {
            const auto size = size_ * sizeof(vector3<Treal_t>) / sizeof(Treal_t);
            tixl::set_first_touch(reinterpret_cast<Treal_t*>(etas_), size, size / omp_get_max_threads(), omp_get_thread_num(),
                omp_get_max_threads());
            tixl::set_first_touch(reinterpret_cast<Treal_t*>(masses_), size_, size_ / omp_get_max_threads(), omp_get_thread_num(),
                omp_get_max_threads());
        }
    }
    
    void perform_experiment() final
    {
        accumulate_eta_sq_t_mass(etas_, masses_, size_);
    }
    
    void finish() final
    {
        std::free(etas_);
        std::free(masses_);
        etas_ = nullptr;
        masses_ = nullptr;
    }
};

} // namespace benchmark


//+////////////////////////////////////////////////////////////////////////////////////////////////
// main
//+////////////////////////////////////////////////////////////////////////////////////////////////

using real_t = float;

int main(int argc, char** argv)
{
    // extract the problem size
    if (argc < 2)
    {
        std::cerr << "Please provide the problem size as command line argument." << std::endl;
        return 1;
    }
    const auto n = static_cast<std::size_t>(std::stoul(argv[1]));
    if (n < 1)
    {
        std::cerr << "Zero valued problem size provided. The program will now be aborted." << std::endl;
        return 1;
    }
    const auto size = n * n * n;
    if (size * sizeof(real_t) / (1024 * 1024 * 1024) > 40)
    {
        std::cerr << "Problem size is too large. The program will now be aborted." << std::endl;
        return 1;
    }
    
    // report
    std::cout << "Starting runs with problem size n=" << n << ".\nThread count: " << omp_get_max_threads() << "."
        << std::endl;
    
    // fix the experiment count
    const auto experiment_count = std::size_t{100};
    
    // perform the experiments
    auto ef = benchmark::exp_fn<real_t>(size);
    const auto measurements = tixl::perform_experiments(ef, experiment_count);
    const auto stats = tixl::compute_statistics(measurements);
    tixl::output_results("Benchmarking of accumulate_eta_sq_t_mass", stats, 7 * size, 4 * size * sizeof(real_t));
    
    return 0;
}

// END OF TESTFILE
