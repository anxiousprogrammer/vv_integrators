#include "constants.h"
#include "integrate.h"
#include "replica_view.h"
#include "scratch_cache.h"
#include "comm_cache.h"
#include "file_io.h"

#include "force/i_force_element.h"
#include "force/i_thermostat.h"

#include "implementation/compute_forces.h"
#include "implementation/compute_potential_energy.h"

#include <iostream> // std::cout
#include <cstdio> // std::printf
#include <cstdlib> // std::getenv
#include <stdexcept> // std::invalid_argument
#include <iomanip> // std::setprecision
#include <type_traits> // std::is_same
#include <filesystem> // std::filesystem::path

#include <mpi.h>


// implementation of path MD MPI variant in integrate.h

namespace vvi
{

//+//////////////////////////////////////////////
// helper functions
//+//////////////////////////////////////////////

template<class Treal_t>
void compute_forces_apply_thermostat(replica_view<Treal_t>& bead, const Treal_t& dt_bd, const Treal_t& gamma_bd,
    i_force_element<Treal_t>* force_element, i_thermostat<Treal_t>* thermostat, scratch_cache<Treal_t>& scache,
    comm_cache<Treal_t>& ccache, const MPI_Comm comm, const int comm_rank, const int comm_size)
{
    // effective force
    if (comm_size == 1)
    {
        implementation::compute_forces(bead, force_element);
    }
    else
    {
        implementation::compute_forces_pmd_bd(bead, dt_bd, gamma_bd, force_element, scache, ccache, comm, comm_rank, comm_size);
    }
    
    // apply thermostat
    if (thermostat == nullptr)
    {
        return;
    }
    for (auto particle_index = std::size_t{}; particle_index < bead.size(); ++particle_index)
    {
        (*thermostat)(bead.get_velocity(particle_index), bead.get_force(particle_index));
    }
}


template<class Treal_t>
std::tuple<Treal_t, Treal_t, Treal_t, Treal_t> compute_pe_ke_te_t(const e_dim dim, const replica_view<Treal_t>& bead,
    const Treal_t& dt_bd, const Treal_t& gamma_bd, i_force_element<Treal_t>* force_element, const MPI_Comm comm,
    const int comm_rank, const int comm_size)
{
    // potential energy
    const auto potential_energy = implementation::compute_potential_energy_pmd_bd(bead, dt_bd, gamma_bd, force_element, comm,
        comm_rank, comm_size);
    
    // kinetic energy
    auto accumulator = 0.0;
    for (auto particle_index = std::size_t{}; particle_index < bead.size(); ++particle_index)
    {
        const auto& velocity = bead.get_velocity(particle_index);
        const auto& mass = bead.get_mass(particle_index);
        accumulator += static_cast<double>((velocity(0) * velocity(0) + velocity(1) * velocity(1)
            + velocity(2) * velocity(2)) * mass);
    }
    const auto kinetic_energy = static_cast<Treal_t>(accumulator) * static_cast<Treal_t>(0.5);
    
    // reduce kinetic energy to root
    auto reduced_kinetic_energy = Treal_t{};
    static constexpr auto mpi_datatype = std::is_same<Treal_t, float>::value ? MPI_FLOAT : MPI_DOUBLE;
    MPI_Reduce(&kinetic_energy, &reduced_kinetic_energy, 1, mpi_datatype, MPI_SUM, 0, comm);
    
    if (comm_rank == 0)
    {
        // total energy
        const auto total_energy = reduced_kinetic_energy + potential_energy;
        
        // temperature
        const auto temperature = static_cast<Treal_t>(2.0) * reduced_kinetic_energy
            / (static_cast<Treal_t>(dim) * static_cast<Treal_t>(kb) * bead.size() * comm_size);
        
        return std::make_tuple(potential_energy, reduced_kinetic_energy, total_energy, temperature);
    }
    
    return {};
}



//+//////////////////////////////////////////////
// API function
//+//////////////////////////////////////////////

template<class Treal_t>
void integrate(const e_dim dim, replica_view<Treal_t>& bead, const Treal_t& dt_bd, const Treal_t& gamma_bd, const Treal_t& dt,
    const std::size_t& time_step_count, i_force_element<Treal_t>* force_element, i_thermostat<Treal_t>* thermostat,
    scratch_cache<Treal_t>& scache, comm_cache<Treal_t>& ccache, const MPI_Comm comm, const std::string& write_file_name)
{
    // pre-conditions
    {
        auto is_init = -1;
        MPI_Initialized(&is_init);
        if (is_init != 1)
        {
            throw std::invalid_argument("integrate_not_initialized_error");
        }
    }
    if (bead.size() == 0)
    {
        throw std::invalid_argument("integrate_invalid_replica_view_error");
    }
    constexpr auto zero = std::is_same<Treal_t, float>::value ? static_cast<Treal_t>(1E-6) : static_cast<Treal_t>(1E-13);
    if (dt_bd < zero)
    {
        throw std::invalid_argument("integrate_invalid_dt_bd_error");
    }
    if (gamma_bd < zero)
    {
        throw std::invalid_argument("integrate_invalid_damping_coefficient_bd_error");
    }
    if (dt < zero)
    {
        throw std::invalid_argument("integrate_invalid_dt_error");
    }
    if (time_step_count < 1)
    {
        throw std::invalid_argument("integrate_invalid_time_step_count_error");
    }
    if (force_element == nullptr)
    {
        throw std::invalid_argument("integrate_force_element_nullptr_error");
    }
    if (bead.size() != scache.vector_vector3_0.size()
        || scache.vector_vector3_1.size() != scache.vector_vector3_0.size()
        || scache.vector_vector3_2.size() != scache.vector_vector3_0.size()
        || scache.vector_vector3_3.size() != scache.vector_vector3_0.size()
        || scache.vector_vector3_4.size() != scache.vector_vector3_0.size())
    {
        throw std::invalid_argument("integrate_improperly_initialized_scratch_cache_error");
    }
    if (bead.size() != ccache.vector_vector3_0.size()
        || ccache.vector_vector3_1.size() != ccache.vector_vector3_0.size()
        || ccache.vector_vector3_2.size() != ccache.vector_vector3_0.size())
    {
        throw std::invalid_argument("integrate_improperly_initialized_comm_cache_error");
    }
    
    // comm. details
    auto comm_rank = -1;
    auto comm_size = -1;
    MPI_Comm_rank(comm, &comm_rank);
    MPI_Comm_size(comm, &comm_size);
    if (comm_rank < 0 || comm_size < 1)
    {
        throw std::runtime_error("integrate_comm_rank_size_error");
    }
    
    // logging interval
    auto log_interval = std::size_t{1};
    const auto log_interval_str = std::getenv("VVI_LOG_INTERVAL");
    if (log_interval_str != nullptr)
    {
        if (std::stol(log_interval_str) < 0)
        {
            throw std::runtime_error("integrate_invalid_log_interval_error");
        }
        log_interval = std::stoul(log_interval_str);
    }
    
    // trajectory interval
    auto trajectory_interval = std::size_t{1};
    const auto trajectory_interval_str = std::getenv("VVI_TRAJECTORY_INTERVAL");
    if (trajectory_interval_str != nullptr)
    {
        if (std::stol(trajectory_interval_str) < 0)
        {
            throw std::runtime_error("integrate_invalid_trajectory_interval_error");
        }
        trajectory_interval = std::stoul(trajectory_interval_str);
    }
    
    // equilibriation starts at...
    auto eq_cut_off = std::size_t{};
    const auto eq_cut_off_str = std::getenv("VVI_EQ_CUT_OFF");
    if (eq_cut_off_str != nullptr)
    {
        if (std::stol(eq_cut_off_str) < 0)
        {
            throw std::runtime_error("integrate_invalid_equilibriation_cut_off_error");
        }
        eq_cut_off = std::stoul(eq_cut_off_str);
    }
    
    // set trajectory file name based on comm. rank & clear write-target
    if (std::filesystem::path(write_file_name).extension() != "")
    {
        throw std::runtime_error("integrate_filename_extension_error");
    }
    const auto proc_write_file_name = write_file_name + "_p" + std::to_string(comm_rank) + ".xyz";
    clear_target(proc_write_file_name);
    
    // inform user about the output
    if (comm_rank == 0 && log_interval != 0)
    {
        std::cout << "# timestep         Temperature                  PE                  KE                  TE" << std::endl;
    }
    
    // computation of forces for the first report
    if (trajectory_interval != 0 && eq_cut_off == 0)
    {
        compute_forces_apply_thermostat(bead, dt_bd, gamma_bd, force_element, thermostat, scache, ccache, comm, comm_rank,
            comm_size);
        write_coordinates(proc_write_file_name, 0, bead);
    }
    
    // recording of observables
    auto recorded_observables = std::vector<Treal_t>();
    
    // loop
    const auto dt_b_2 = dt * static_cast<Treal_t>(0.5);
    for (auto time_step = std::size_t{}; time_step < time_step_count; ++time_step)
    {
        // report
        if (log_interval != 0 && time_step % log_interval == 0)
        {
            const auto [potential_energy, kinetic_energy, total_energy, temperature] = compute_pe_ke_te_t(dim, bead, dt_bd,
                gamma_bd, force_element, comm, comm_rank, comm_size);
            if (comm_rank == 0)
            {
                std::printf("%10lu%20.10f%20.10f%20.10f%20.10f\n", time_step, temperature, potential_energy, kinetic_energy,
                    total_energy);
                
                // record the observables
                if (thermostat == nullptr)
                {
                    recorded_observables.push_back(total_energy);
                }
                else
                {
                    recorded_observables.push_back(temperature);
                }
            }
        }
        
        // update phase 1 (half-step velocity and full-step position)
        for (auto particle_index = std::size_t{}; particle_index < bead.size(); ++particle_index)
        {
            bead.get_velocity(particle_index) += bead.get_force(particle_index) * dt_b_2;
            bead.get_position(particle_index) += bead.get_velocity(particle_index) * dt;
        }
        
        // compute the force
        compute_forces_apply_thermostat(bead, dt_bd, gamma_bd, force_element, thermostat, scache, ccache, comm, comm_rank,
            comm_size);
        
        // update phase 2 (half-step velocity)
        for (auto particle_index = std::size_t{}; particle_index < bead.size(); ++particle_index)
        {
            bead.get_velocity(particle_index) += bead.get_force(particle_index) * dt_b_2;
        }
        
        // write the polymer's coordinates (as per the given frequency)
        if (trajectory_interval != 0 && time_step % trajectory_interval == 0 && time_step > eq_cut_off)
        {
            write_coordinates(proc_write_file_name, time_step + 1, bead);
        }
    }
    
    // energy report
    if (comm_rank == 0)
    {
        if (thermostat == nullptr)
        {
            // expect total energies to have been recorded
            if (recorded_observables.empty())
            {
                throw std::runtime_error("integrate_total_energies_not_recorded_error");
            }
            
            // compute the mean
            auto mean = 0.0;
            for (const auto& recorded_observable : recorded_observables)
            {
                mean += static_cast<double>(recorded_observable);
            }
            mean /= recorded_observables.size();
            
            // compute the std deviation
            auto std_dev = 0.0;
            for (const auto& recorded_observable : recorded_observables)
            {
                const auto dev = static_cast<double>(recorded_observable - mean);
                std_dev += dev * dev;
            }
            std_dev = std::sqrt(std_dev / recorded_observables.size());
            
            // report
            std::cout << "The std. deviation of " << recorded_observables.size() << " recorded total energies is: " << std_dev
                << " kJ/mol. This corresponds to " << std_dev / mean * 100.0 << "% drift.\n";
        }
        else
        {
            // make sure that there are records beyond the equilibriation cut off point
            if (recorded_observables.size() <= eq_cut_off)
            {
                throw std::runtime_error("integrate_temperatures_not_recorded_error");
            }
            
            // compute the mean beyond the transient (if applicable)
            auto sum = 0.0;
            auto squared_sum = 0.0;
            auto record_count = std::size_t{};
            for (auto record_index = eq_cut_off; record_index < recorded_observables.size(); ++record_index)
            {
                const auto summed_value = static_cast<double>(recorded_observables[record_index]);
                sum += summed_value;
                squared_sum += summed_value * summed_value;
                ++record_count;
            }
            const auto mean = sum / record_count;
            const auto std_dev = squared_sum / record_count - mean * mean;
            
            // report
            if (eq_cut_off > 0)
            {
                std::cout << "The transient was cut off at " << eq_cut_off << ". ";
            }
            std::cout << "The mean of " << record_count << " recorded temperatures is: " << mean << " K with std. deviation: "
                << std_dev << " K.\n";
        }
    }
}

// full instantiations
template void integrate<float>(const e_dim, replica_view<float>&, const float&, const float&, const float&, const std::size_t&,
    i_force_element<float>*, i_thermostat<float>*, scratch_cache<float>&, comm_cache<float>&, const MPI_Comm, const std::string&);
template void integrate<double>(const e_dim, replica_view<double>&, const double&, const double&, const double&,
    const std::size_t&, i_force_element<double>*, i_thermostat<double>*, scratch_cache<double>&, comm_cache<double>&,
    const MPI_Comm, const std::string&);

} // namespace vvi

// END OF IMPLEMENTATION
