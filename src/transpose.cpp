#include "file_io.h"
#include "vector3.h"

#include <iostream> // std::cerr
#include <filesystem> // ::path


using real_t = double;

int main(int argc, char** argv)
{
    // extract arguments
    if (argc < 5)
    {
        std::cerr << "[transpose]: Please pass\n"
            << "                 1. path to .xyz files,\n"
            << "                 2. base file name of .xyz files (without extension),\n"
            << "                 3. replica count (indexes upto which will be suffixed to the base file name) and\n"
            << "                 4. name of output directory (which will be created)\n"
            << "             as command-line arguments." << std::endl;
        return 1;
    }
    const auto path = std::string(argv[1]);
    if (path[path.size() - 1] != '/')
    {
        const_cast<std::string&>(path) += "/";
    }
    if (!std::filesystem::exists(path))
    {
        std::cerr << "[transpose]: The provided path is not valid. The program will be aborted." << std::endl;
        return 1;
    }
    const auto base_file_name = std::string(argv[2]);
    if (std::filesystem::path(base_file_name).extension() != "")
    {
        std::cerr << "[transpose]: The provided base file name is not valid. The program will be aborted." << std::endl;
        return 1;
    }
    const auto replica_count = std::stoul(argv[3]);
    if (replica_count < 1)
    {
        std::cerr << "[transpose]: The provided replica count is not valid. The program will be aborted." << std::endl;
        return 1;
    }
    const auto output_dir = std::string(argv[4]);
    if (output_dir[output_dir.size() - 1] != '/')
    {
        const_cast<std::string&>(output_dir) += "/";
    }
    
    // read all trajectories
    auto trajectory_matrix = std::vector<std::vector<vvi::t_md_frame<real_t>>>{};
    trajectory_matrix.reserve(replica_count);
    for (auto file_index = std::size_t{}; file_index < replica_count; ++file_index)
    {
        // make sure the file exists
        const auto full_file_name = path + base_file_name + std::to_string(file_index) + ".xyz";
        if (!std::filesystem::exists(full_file_name))
        {
            std::cerr << "[transpose]: At least one of the input files does not exist. The program will be aborted."
                << std::endl;
            return 1;
        }
        
        // read the file
        try
        {
            trajectory_matrix.emplace_back(vvi::read_xyz_file<real_t>(full_file_name));
        }
        catch (const std::exception& ex)
        {
            std::cerr << "[transpose]: An exception was caught: " << ex.what() << ". The program will be aborted." << std::endl;
            return 1;
        }
    }
    
    // ensure that all trajectories have the same frame count
    const auto& frame_count = trajectory_matrix[0].size();
    for (auto trajectory_index = std::size_t{}; trajectory_index < replica_count; ++trajectory_index)
    {
        if (trajectory_matrix[trajectory_index].size() != frame_count)
        {
            std::cerr << "[transpose]: Not all trajectories in the trajectory matrix have the same frame count. The program "
                << "will be aborted." << std::endl;
            return 1;
        }
    }
    
    // report
    std::cout << "[transpose]: Read " << replica_count << " .xyz files consisting of " << frame_count << " frames each.\n";
    
    // create the output directory
    const auto output_path = std::string(path + output_dir);
    if (!std::filesystem::exists(output_path))
    {
        if (!std::filesystem::create_directory(output_path))
        {
            std::cerr << "[transpose]: Output directory does not exist and could not be created. The program will be aborted."
                << std::endl;
            return 1;
        }
    }
    
    // transpose the trajectory matrix and write to file
    for (auto frame_index = std::size_t{}; frame_index < frame_count; ++frame_index)
    {
        // assemble the output trajectory
        auto output_trajectory = std::vector(replica_count, vvi::t_md_frame<real_t>{});
        for (auto replica_index = std::size_t{}; replica_index < replica_count; ++replica_index)
        {
            output_trajectory[replica_index] = trajectory_matrix[replica_index][frame_index];
        }
        
        // write the assembled trajectory
        try
        {
            vvi::write_xyz_file(output_path + "transposed" + std::to_string(frame_index) + ".xyz", output_trajectory);
        }
        catch (const std::exception& ex)
        {
            std::cerr << "[transpose]: An exception was caught: " << ex.what() << ". The program will be aborted." << std::endl;
            return 1;
        }
    }
    
    // report
    std::cout << "[transpose]: Wrote " << frame_count << " .xyz files consisting of " << replica_count << " frames each.\n";
    
    return 0;
}
