#include "vector3.h"
#include "vector3_array.h"

#include <stdexcept> // std::runtime_error
#include <algorithm> // std::copy


// implementation of vector3_array.h

namespace vvi
{

//+////////////////////////////////////////////////////////////////////////////////////////////////
// lifecycle
//+////////////////////////////////////////////////////////////////////////////////////////////////

template<class Treal_t>
vector3_array<Treal_t>::vector3_array(const vector3_array& ref)
{
    if (ref.size_ == 0)
    {
        return;
    }
    
    size_ = ref.size_;
    allocate(size_);
    std::copy(ref.data_, ref.data_ + ref.size_, data_);
}


template<class Treal_t>
vector3_array<Treal_t>::vector3_array(vector3_array&& ref)
{
    data_ = ref.data_;
    size_ = ref.size_;
    ref.data_ = nullptr;
    ref.size_ = {};
}


template<class Treal_t>
vector3_array<Treal_t>& vector3_array<Treal_t>::operator=(const vector3_array& ref)
{
    // aliasing check
    if (&ref == this)
    {
        return *this;
    }
    
    // resize check
    if (size_ != ref.size_)
    {
        free();
        size_ = ref.size_;
        if (size_ != 0)
        {
            allocate(size_);
        }
    }
    
    // now copy
    if (data_ == nullptr)
    {
        throw std::runtime_error("vector3_array_operator=_dangling_data_pointer_error");
    }
    std::copy(ref.data_, ref.data_ + ref.size_, data_);
    
    return *this;
}


template<class Treal_t>
vector3_array<Treal_t>& vector3_array<Treal_t>::operator=(vector3_array&& ref)
{
    data_ = ref.data_;
    size_ = ref.size_;
    ref.data_ = nullptr;
    ref.size_ = {};
    
    return *this;
}


template<class Treal_t>
vector3_array<Treal_t>::~vector3_array()
{
    free();
    size_ = {};
}



//+////////////////////////////////////////////////////////////////////////////////////////////////
// manipulation
//+////////////////////////////////////////////////////////////////////////////////////////////////

template<class Treal_t>
void vector3_array<Treal_t>::resize(const std::size_t& new_size)
{
    // resize check
    if (size_ == new_size)
    {
        return;
    }
    
    // reallocate
    free();
    size_ = new_size;
    if (size_ == 0)
    {
        return;
    }
    allocate(size_);
}



//+////////////////////////////////////////////////////////////////////////////////////////////////
// full instantiations
//+////////////////////////////////////////////////////////////////////////////////////////////////

template class vector3_array<float>;
template class vector3_array<double>;

} // namespace vvi

// END OF IMPLEMENTATION
