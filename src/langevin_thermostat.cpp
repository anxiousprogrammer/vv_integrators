#include "dim.h"
#include "constants.h"
#include "vector3.h"

#include "force/langevin_thermostat.h"

#include <random> // std::mt19937
#include <stdexcept> // std::invalid_argument
#include <type_traits> // std::is_same


// implementation of langevin_thermostat.h

namespace vvi
{

//+////////////////////////////////////////////////////////////////////////////////////////////////
// lifecycle
//+////////////////////////////////////////////////////////////////////////////////////////////////

template<class Treal_t>
langevin_thermostat<Treal_t>::langevin_thermostat(const e_dim dim, const Treal_t& dt, const Treal_t& mass, const Treal_t& gamma,
    const Treal_t& temperature)
    : dim_(dim), mass_(mass), gamma_(gamma), temperature_(temperature), random_engine_(std::random_device()())
{
    constexpr auto zero_tolerance = std::is_same<Treal_t, float>::value ? static_cast<Treal_t>(1E-6)
        : static_cast<Treal_t>(1E-13);
    if (dt < zero_tolerance)
    {
        throw std::invalid_argument("langevin_thermostat_ctor_invalid_dt_error");
    }
    if (gamma_ < zero_tolerance)
    {
        throw std::invalid_argument("langevin_thermostat_ctor_invalid_damping_coefficient_error");
    }
    if (temperature_ < zero_tolerance)
    {
        throw std::invalid_argument("langevin_thermostat_ctor_invalid_temperature_error");
    }
    
    // set the cached variable
    // TODO: M assumed to be the same for all particles
    const_cast<Treal_t&>(sigma_) = std::sqrt(static_cast<Treal_t>(2.0 * kb) * mass_ * gamma_ * temperature_ / dt);
}



//+////////////////////////////////////////////////////////////////////////////////////////////////
// main functionality
//+////////////////////////////////////////////////////////////////////////////////////////////////

template<class Treal_t>
void langevin_thermostat<Treal_t>::operator()(const vector3<Treal_t>& velocity, vector3<Treal_t>& force)
{
    // calculate the noise vector
    auto gaussian_noise = std::normal_distribution(static_cast<Treal_t>(0.0), sigma_);
    auto noise_vector = vector3<Treal_t>{gaussian_noise(random_engine_), static_cast<Treal_t>(0.0),
        static_cast<Treal_t>(0.0)};
    if (static_cast<unsigned int>(dim_) >= static_cast<unsigned int>(e_dim::d2))
    {
        noise_vector(1) = gaussian_noise(random_engine_);
    }
    if (static_cast<unsigned int>(dim_) == static_cast<unsigned int>(e_dim::d3))
    {
        noise_vector(2) = gaussian_noise(random_engine_);
    }
    
    // adapt the force
    force = force - velocity * gamma_ * mass_ + noise_vector;
}



//+////////////////////////////////////////////////////////////////////////////////////////////////
// full instantiations
//+////////////////////////////////////////////////////////////////////////////////////////////////

template class langevin_thermostat<float>;
template class langevin_thermostat<double>;

} // namespace vvi

// END OF IMPLEMENTATION
