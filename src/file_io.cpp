#include "file_io.h"

#include "system_view.h"
#include "replica_view.h"

#include <stdexcept> // std::runtime_error
#include <fstream> // std::ofstream
#include <sstream> // std::stringstream
#include <string> // std::getline
#include <cctype> // std::isdigit

// TODO DEBUG
#include <iostream>


// implementation of file_io.h

namespace vvi
{

void clear_target(const std::string& file_name)
{
    // open in trunc mode
    auto file_clearer = std::ofstream(file_name, std::ios_base::trunc);
    if (!file_clearer.is_open())
    {
        throw std::runtime_error("clear_target_unable_to_clear_file_error");
    }
    
    // close now
    file_clearer.close();
    if (file_clearer.is_open())
    {
        throw std::runtime_error("clear_target_could_not_close_output_file_error");
    }
}


template<template<class> class Tview_t, class Treal_t>
void write_coordinates(const std::string& file_name, const std::size_t& time_step, const Tview_t<Treal_t>& view)
{
    // pre-conditions
    if (view.size() == 0)
    {
        throw std::invalid_argument("write_coordinates_invalid_input_error");
    }
    
    // open the file for writing
    auto file = std::ofstream(file_name, std::ios_base::app);
    if (!file.is_open())
    {
        throw std::runtime_error("write_coordinates_could_not_open_output_file_error");
    }
    
    // write to file
    file << view.size() << "\nTime-step: " << time_step << "\n";
    for (auto particle_index = std::size_t{}; particle_index < view.size(); ++particle_index)
    {
        const auto& position = view.get_position(particle_index);
        file << "C " << position(0) << " " << position(1) << " " << position(2) << "\n"; // TODO: atom name is always "C" as of now!
    }
    
    // close the file
    file.close();
    if (file.is_open())
    {
        throw std::runtime_error("write_coordinates_could_not_close_output_file_error");
    }
}


template<class Treal_t>
std::vector<t_md_frame<Treal_t>> read_xyz_file(const std::string& file_name)
{
    // pre-conditions
    if (file_name.empty())
    {
        throw std::invalid_argument("read_xyz_file_invalid_file_name_error");
    }
    
    // open the file for writing
    auto file = std::ifstream(file_name);
    if (!file.is_open())
    {
        throw std::runtime_error("read_xyz_file_could_not_open_input_file_error");
    }
    
    // read file line by line
    auto frames = std::vector<t_md_frame<Treal_t>>{};
    for (auto line_counter = std::size_t{}; line_counter < std::numeric_limits<std::size_t>::max(); ++line_counter)
    {
        // read the system size line
        auto system_size_line = std::string{};
        std::getline(file, system_size_line);
        
        // check if eof (time to break?)
        if (file.eof())
        {
            break;
        }
        if (file.fail())
        {
            throw std::runtime_error("read_xyz_file_fail_bit_set_while_reading_system_size_error");
        }
        
        // validate and extract the system size
        for (const auto ch : system_size_line)
        {
            if (!std::isdigit(ch))
            {
                throw std::runtime_error("read_xyz_file_system_size_format_error");
            }
        }
        const auto system_size = std::stoul(system_size_line);
        
        // create a new frame
        auto frame = t_md_frame<Treal_t>{};
        frame.atom_names.resize(system_size);
        frame.positions.resize(system_size);
        
        // read the comment
        std::getline(file, frame.comment);
        
        // read the atoms
        for (auto atom_counter = std::size_t{}; atom_counter < system_size; ++atom_counter)
        {
            // extract
            auto atom_line = std::string{};
            std::getline(file, atom_line);
            if (file.fail())
            {
                throw std::runtime_error("read_xyz_file_fail_bit_set_while_reading_atom_line_error");
            }
            auto atom_extractor = std::stringstream(atom_line);
            
            // atom name
            atom_extractor >> frame.atom_names[atom_counter];
            if (atom_extractor.fail())
            {
                throw std::runtime_error("read_xyz_file_fail_bit_set_while_extracting_atom_name_error");
            }
            
            // X
            atom_extractor >> frame.positions[atom_counter](0);
            if (atom_extractor.fail())
            {
                throw std::runtime_error("read_xyz_file_fail_bit_set_while_extracting_pos_x_error");
            }
            
            // Y
            atom_extractor >> frame.positions[atom_counter](1);
            if (atom_extractor.fail())
            {
                throw std::runtime_error("read_xyz_file_fail_bit_set_while_extracting_pos_y_error");
            }
            
            // Z
            atom_extractor >> frame.positions[atom_counter](2);
            if (atom_extractor.fail())
            {
                throw std::runtime_error("read_xyz_file_fail_bit_set_while_extracting_pos_z_error");
            }
        }
        
        // append to the returning list
        frames.emplace_back(frame);   
    }
    
    // close the file
    file.close();
    if (file.is_open())
    {
        throw std::runtime_error("read_xyz_file_could_not_close_input_file_error");
    }
    
    return frames;
}


template<class Treal_t>
void write_xyz_file(const std::string& file_name, const std::vector<t_md_frame<Treal_t>>& frames)
{
    // pre-conditions
    if (file_name.empty())
    {
        throw std::invalid_argument("write_xyz_file_invalid_file_name_error");
    }
    if (frames.empty())
    {
        throw std::invalid_argument("write_xyz_file_invalid_input_error");
    }
    
    // open the file for writing
    auto file = std::ofstream(file_name, std::ios_base::trunc);
    if (!file.is_open())
    {
        throw std::runtime_error("write_xyz_file_could_not_open_input_file_error");
    }
    
    // write the file line by line
    for (const auto& frame : frames)
    {
        // system size
        file << frame.positions.size() << "\n";
        
        // comment
        file << frame.comment << "\n";
        
        // atom positions
        for (auto atom_index = std::size_t{}; atom_index < frame.positions.size(); ++atom_index)
        {
            // atom name and position
            file << frame.atom_names[atom_index] << " " << frame.positions[atom_index](0) << " "
                << frame.positions[atom_index](1) << " " << frame.positions[atom_index](2) << "\n";
        }
    }
    
    // close the file
    file.close();
    if (file.is_open())
    {
        throw std::runtime_error("write_xyz_file_could_not_close_input_file_error");
    }
}

// full instantiations
template void write_coordinates<system_view, float>(const std::string&, const std::size_t&, const system_view<float>&);
template void write_coordinates<system_view, double>(const std::string&, const std::size_t&, const system_view<double>&);
template void write_coordinates<replica_view, float>(const std::string&, const std::size_t&, const replica_view<float>&);
template void write_coordinates<replica_view, double>(const std::string&, const std::size_t&, const replica_view<double>&);

template std::vector<t_md_frame<float>> read_xyz_file(const std::string&);
template std::vector<t_md_frame<double>> read_xyz_file(const std::string&);

template void write_xyz_file(const std::string& file_name, const std::vector<t_md_frame<float>>& frames);
template void write_xyz_file(const std::string& file_name, const std::vector<t_md_frame<double>>& frames);

} // namespace vvi

// END OF IMPLEMENTATION
