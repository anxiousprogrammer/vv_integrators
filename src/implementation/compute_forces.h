#include "vector3.h"
#include "replica_view.h"
#ifdef VVI_USE_MPI
#include "pmd_bd_kernel_functions.h"
#include "scratch_cache.h"
#include "comm_cache.h"
#endif

#include "force/i_force_element.h"
#include "force/i_force_element_external.h"
#include "force/i_force_element_pairwise.h"

#include <utility> // std::size_t

#ifdef VVI_USE_MPI
#include <mpi.h>
#endif


#ifndef VVI_COMPUTE_FORCES_H
#define VVI_COMPUTE_FORCES_H

namespace vvi::implementation
{

template<class Treal_t>
void compute_forces(const vector3<Treal_t>* positions, vector3<Treal_t>* forces, const std::size_t& atom_count,
    i_force_element<Treal_t>* force_element);

template<template<class> class Tview_t, class Treal_t>
void compute_forces(Tview_t<Treal_t>& view, i_force_element<Treal_t>* force_element)
{
    return compute_forces(view.get_positions_view().begin(), view.get_forces_view().begin(), view.size(), force_element);
}


template<class Treal_t>
void compute_forces(const vector3<Treal_t>* positions, vector3<Treal_t>* forces, const std::size_t& atom_count,
    i_force_element<Treal_t>* force_element)
{
    #ifndef NDEBUG
    // DEBUG SECTION: preconditions
    if (forces == nullptr)
    {
        throw std::invalid_argument("compute_forces_forces_null_pointer_error");
    }
    if (force_element == nullptr)
    {
        throw std::invalid_argument("compute_forces_force_element_null_pointer_error");
    }
    #endif
    
    if (force_element->get_interaction_type() == e_force_element_interaction::external)
    {
        for (auto atom_index = std::size_t{}; atom_index < atom_count; ++atom_index)
        {
            forces[atom_index] = dynamic_cast<i_force_element_external<Treal_t>*>(force_element)->compute_force(
                positions[atom_index]);
        }
    }
    else if (force_element->get_interaction_type() == e_force_element_interaction::pairwise)
    {
        // clear
        std::fill(forces, forces + atom_count, vector3<Treal_t>{});
        
        // accumulate
        for (auto i1 = std::size_t{}; i1 < atom_count - 1; ++i1)
        {
            // compute
            for (auto i2 = i1 + 1; i2 < atom_count; ++i2)
            {
                const auto force = dynamic_cast<i_force_element_pairwise<Treal_t>*>(force_element)->compute_force(
                    positions[i1], positions[i2]);
                forces[i1] += force;
                forces[i2] -= force;
            }
        }
    }
}


#ifdef VVI_USE_MPI
template<class Treal_t>
void compute_forces_pmd_bd(replica_view<Treal_t>& bead, const Treal_t& dt_bd, const Treal_t& gamma_bd,
    i_force_element<Treal_t>* force_element, scratch_cache<Treal_t>& scache, comm_cache<Treal_t>& ccache, const MPI_Comm comm,
    const int comm_rank, const int comm_size)
{
    #ifndef NDEBUG
    constexpr auto zero = std::is_same<Treal_t, float>::value ? static_cast<Treal_t>(1E-6) : static_cast<Treal_t>(1E-13);
    if (dt_bd < zero)
    {
        throw std::invalid_argument("compute_forces_pmd_bd_zero_dt_bd_error");
    }
    if (gamma_bd < zero)
    {
        throw std::invalid_argument("compute_forces_pmd_bd_zero_damping_coefficient_bd_error");
    }
    #endif
    
    //+/////////////////
    // details
    //+/////////////////
    
    // renaming for convenience (better control to do it in one place)
    // renaming for convenience (comm. stuff)
    auto* positions_left = ccache.vector_vector3_0.data();
    auto* positions_right = ccache.vector_vector3_1.data();
    auto* forces_left = ccache.vector_vector3_2.data();
    
    // renaming for convenience (FDM stuff)
    auto* positions_p_eta_eps = scache.vector_vector3_0.data();
    auto* positions_m_eta_eps = scache.vector_vector3_1.data();
    auto* forces_plus = scache.vector_vector3_2.data();
    auto* forces_minus = scache.vector_vector3_3.data();
    auto* hessian_U_t_eta_eps = scache.vector_vector3_4.data();
    
    // renaming for convenience (others)
    const auto& atom_count = bead.size();
    const auto* positions = bead.get_positions_view().begin();
    auto* forces = bead.get_forces_view().begin();
    const auto* masses = bead.get_masses_view().begin();
    const auto* inv_masses = bead.get_cached_inv_masses_view().begin();
    auto* etas = bead.get_cached_etas_view().begin();
    
    // comm. constants
    const auto is_first_proc = comm_rank == 0;
    const auto is_last_proc = comm_rank + 1 == comm_size;
    const auto left_rank = comm_rank - 1;
    const auto right_rank = comm_rank + 1;
    const auto comm_data_size = atom_count * sizeof(vector3<Treal_t>) / sizeof(Treal_t);
    static constexpr auto mpi_datatype = std::is_same<Treal_t, float>::value ? MPI_FLOAT : MPI_DOUBLE;
    
    //+/////////////////
    // post receives
    //+/////////////////
    
    auto request_forces_recv = MPI_Request{};
    auto request_positions_recv_2 = MPI_Request{};
    if (!is_first_proc)
    {
        // post receives for forces and positions coming from the left neighbour
        MPI_Irecv(forces_left, comm_data_size, mpi_datatype, left_rank, 2, comm, &request_forces_recv);
        MPI_Irecv(positions_left, comm_data_size, mpi_datatype, left_rank, 1, comm, &request_positions_recv_2);
    }
    auto request_positions_recv_1 = MPI_Request{};
    if (!is_last_proc)
    {
        // post receives for positions coming from the right neighbour
        MPI_Irecv(reinterpret_cast<Treal_t*>(positions_right), comm_data_size, mpi_datatype, right_rank, 0, comm,
            &request_positions_recv_1);
    }
    
    //+/////////////////
    // force computation (parallel to comm. in all cases except first proc.)
    //+/////////////////
    
    // start sending positions to the left and right neighbours
    auto request_positions_send_1 = MPI_Request{};
    if (!is_first_proc)
    {
        MPI_Isend(reinterpret_cast<const Treal_t*>(positions), comm_data_size, mpi_datatype, left_rank, 0, comm,
            &request_positions_send_1);
    }
    auto request_positions_send_2 = MPI_Request{};
    if (!is_last_proc)
    {
        MPI_Isend(reinterpret_cast<const Treal_t*>(positions), comm_data_size, mpi_datatype, right_rank, 1, comm,
            &request_positions_send_2);
    }
    
    // compute forces
    compute_forces(positions, forces, atom_count, force_element);
    
    // wait for the sending of positions to the left to complete
    if (!is_first_proc)
    {
        MPI_Wait(&request_positions_send_1, MPI_STATUS_IGNORE);
    }
    
    //+/////////////////
    // compute etas and then Hessian(U) (not applicable to last bead)
    //+/////////////////
    
    // phase 1: computation of etas
    auto max_eta_local = Treal_t{};
    auto request_forces_send = MPI_Request{};
    if (!is_last_proc)
    {
        // start sending forces to the right
        MPI_Isend(reinterpret_cast<const Treal_t*>(forces), comm_data_size, mpi_datatype, right_rank, 2, comm,
            &request_forces_send);
        
        // wait for positions-of-right to arrive
        MPI_Wait(&request_positions_recv_1, MPI_STATUS_IGNORE);
        
        // etas
        const auto dt_bd_b_gamma_bd = dt_bd / gamma_bd;
        max_eta_local = kernels::compute_etas(positions_right, positions, forces, inv_masses, etas, atom_count,
            dt_bd_b_gamma_bd);
    }
    
    // compute delta (to normalize the 3N-sized eta vector in the FDM calculation; see https://doi.org/10.1063/1.4971438)
    auto max_eta = Treal_t{};
    MPI_Allreduce(&max_eta_local, &max_eta, 1, mpi_datatype, MPI_MAX, comm);
    constexpr auto max_eta_tol = std::is_same<Treal_t, float>::value ? static_cast<Treal_t>(1E-6) : static_cast<Treal_t>(1E-12);
    // // if max_eta is zero valued, eta * delta * eps ~ 0.0
    const auto delta = max_eta > max_eta_tol ? static_cast<Treal_t>(1.0) / max_eta : static_cast<Treal_t>(1.0);
    
    // phase 2: computations of Hessians(U) * etas
    if (!is_last_proc)
    {
        // compute force-up, force-down
        // NOTE: to choose eps, we assumed a max. box size (i.e. position value) of 500.0 and required that
        // position-plus-eta-delta-eps be representable. Then we used a factor-of-safety of 10.0.
        constexpr auto eps = std::is_same<Treal_t, float>::value ? static_cast<Treal_t>(1E-4) : static_cast<Treal_t>(1E-12); // CAREFUL!
        const auto delta_eps = delta * eps;
        kernels::plus_minus(delta_eps, etas, positions, positions_p_eta_eps, positions_m_eta_eps, atom_count);
        compute_forces(positions_p_eta_eps, forces_plus, atom_count, force_element);
        compute_forces(positions_m_eta_eps, forces_minus, atom_count, force_element);
        
        // hessian(U) times eta
        kernels::compute_hessian_U_t_eta_eps(forces_plus, forces_minus, hessian_U_t_eta_eps, atom_count, delta_eps);
        
        // wait for the sending of forces and positions to the right to complete
        MPI_Wait(&request_forces_send, MPI_STATUS_IGNORE);
        MPI_Wait(&request_positions_send_2, MPI_STATUS_IGNORE);
    }
    
    //+/////////////////
    // final computational phase
    //+/////////////////
    
    // all beads now compute effective forces
    const auto gamma_bd_b_dt_bd = gamma_bd / dt_bd;
    if (is_first_proc)
    {
        // here's the special case of the first bead
        kernels::compute_effective_forces_first_bead(hessian_U_t_eta_eps, positions_right, positions, masses, forces, atom_count,
            gamma_bd_b_dt_bd);
    }
    else
    {
        // wait for forces-of-left and positions-of-left to arrive
        MPI_Wait(&request_forces_recv, MPI_STATUS_IGNORE);
        MPI_Wait(&request_positions_recv_2, MPI_STATUS_IGNORE);
        
        // differential treatment
        if (is_last_proc)
        {
            kernels::compute_effective_forces_last_bead(forces_left, positions, positions_left, masses, forces, atom_count,
                gamma_bd_b_dt_bd);
        }
        else
        {
            kernels::compute_effective_forces(forces_left, positions, positions_right, positions_left, hessian_U_t_eta_eps,
                masses, forces, atom_count, gamma_bd_b_dt_bd);
        }
    }
}
#endif

} // namespace vvi::implementation

#endif
