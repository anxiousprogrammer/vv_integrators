#include "vector3.h"
#include "replica_view.h"
#include "pmd_bd_kernel_functions.h"

#include "force/i_force_element.h"
#include "force/i_force_element_external.h"
#include "force/i_force_element_pairwise.h"

#include <utility> // std::size_t

#ifdef VVI_USE_MPI
#include <mpi.h>
#endif


#ifndef VVI_COMPUTE_POTENTIAL_ENERGY_H
#define VVI_COMPUTE_POTENTIAL_ENERGY_H

namespace vvi::implementation
{

template<template<class> class Tview_t, class Treal_t>
Treal_t compute_potential_energy(const Tview_t<Treal_t>& view, i_force_element<Treal_t>* force_element)
{
    #ifndef NDEBUG
    if (view.size() == 0)
    {
        throw std::invalid_argument("compute_potential_energy_zero_size_error");
    }
    if (force_element == nullptr)
    {
        throw std::invalid_argument("compute_potential_energy_null_pointer_error");
    }
    #endif
    
    auto potential_energy = 0.0;
    if (force_element->get_interaction_type() == e_force_element_interaction::external)
    {
        for (auto particle_index = std::size_t{}; particle_index < view.size(); ++particle_index)
        {
            potential_energy += static_cast<double>(dynamic_cast<i_force_element_external<Treal_t>*>(
                force_element)->compute_potential_energy(view.get_position(particle_index)));
        }
    }
    else if (force_element->get_interaction_type() == e_force_element_interaction::pairwise)
    {
        for (auto i1 = std::size_t{}; i1 < view.size() - 1; ++i1)
        {
            for (auto i2 = i1 + 1; i2 < view.size(); ++i2)
            {
                potential_energy += static_cast<double>(dynamic_cast<i_force_element_pairwise<Treal_t>*>(
                    force_element)->compute_potential_energy(view.get_position(i1), view.get_position(i2)));
            }
        }
    }
    
    return static_cast<Treal_t>(potential_energy);
}


#ifdef VVI_USE_MPI
template<class Treal_t>
Treal_t compute_potential_energy_pmd_bd(const replica_view<Treal_t>& bead, const Treal_t& dt_bd,
    const Treal_t& gamma_bd, i_force_element<Treal_t>* force_element, const MPI_Comm comm, const int comm_rank,
    const int comm_size)
{
    #ifndef NDEBUG
    if (bead.size() == 0)
    {
        throw std::invalid_argument("compute_potential_energy_pmd_bd_zero_size_error");
    }
    static constexpr auto zero = std::is_same<Treal_t, float>::value ? static_cast<Treal_t>(1E-4) : static_cast<Treal_t>(1E-10);
    if (dt_bd < zero)
    {
        throw std::invalid_argument("compute_potential_energy_pmd_bd_zero_dt_error");
    }
    if (gamma_bd < zero)
    {
        throw std::invalid_argument("compute_potential_energy_pmd_bd_zero_damping_coefficient_error");
    }
    if (force_element == nullptr)
    {
        throw std::invalid_argument("compute_potential_energy_pmd_bd_null_pointer_error");
    }
    #endif
    
    // potential energy of the first bead is required as a contribution
    auto effective_potential_energy = Treal_t{};
    if (comm_rank == 0)
    {
        effective_potential_energy = compute_potential_energy(bead, force_element);
    }
    
    // compute the remaining term
    if (comm_rank + 1 != comm_size) // exclude the last bead
    {
        const auto* etas = bead.get_cached_etas_view().begin();
        const auto* masses = bead.get_masses_view().begin();
        const auto accumulated_eta_sq_t_mass = kernels::accumulate_eta_sq_t_mass(etas, masses, bead.size());
        effective_potential_energy += accumulated_eta_sq_t_mass * static_cast<Treal_t>(0.25) * gamma_bd / dt_bd;
    }
    
    // reduce to root
    if (comm_size > 2)
    {
        auto reduced_effective_potential_energy = Treal_t{};
        static constexpr auto mpi_datatype = std::is_same<Treal_t, float>::value ? MPI_FLOAT : MPI_DOUBLE;
        MPI_Reduce(&effective_potential_energy, &reduced_effective_potential_energy, 1, mpi_datatype, MPI_SUM, 0, comm);
        if (comm_rank == 0)
        {
            effective_potential_energy = reduced_effective_potential_energy;
        }
    }
    
    return effective_potential_energy;
}
#endif

} // namespace vvi::implementation

#endif
