#include "vector3.h"

#include "force/i_force_element.h"

#include <utility> // std::size_t
#include <cmath> // std::fabs
#include <algorithm> // std::max


#ifndef VVI_COMPUTE_F_PE_PMD_KERNEL_FUNCTIONS_H
#define VVI_COMPUTE_F_PE_PMD_KERNEL_FUNCTIONS_H

namespace vvi::implementation::kernels
{

template<class Treal_t>
Treal_t accumulate_eta_sq_t_mass(const vector3<Treal_t>* etas, const Treal_t* masses, const std::size_t& size)
{
    #ifndef NDEBUG
    if (etas == nullptr || masses == nullptr)
    {
        throw std::invalid_argument("accumulate_eta_sq_t_mass_null_pointer_error");
    }
    if (size == 0)
    {
        throw std::invalid_argument("accumulate_eta_sq_t_mass_size_zero_error");
    }
    #endif
    
    auto a = 0.0;
    #pragma omp parallel for simd safelen(16) reduction(+:a)
    for (auto atom_index = std::size_t{}; atom_index < size; ++atom_index)
    {
        const auto& eta = etas[atom_index];
        const auto& x = eta(0);
        const auto& y = eta(1);
        const auto& z = eta(2);
        const auto& m = masses[atom_index];
        
        a += static_cast<double>((x * x + y * y + z * z) * m);
    }
    
    return static_cast<Treal_t>(a);
}


template<class Treal_t>
Treal_t compute_etas(const vector3<Treal_t>* positions_right, const vector3<Treal_t>* positions, const vector3<Treal_t>* forces,
    const Treal_t* inv_masses, vector3<Treal_t>* etas, const std::size_t& size, const Treal_t& dt_bd_b_gamma_bd)
{
    #ifndef NDEBUG
    if (positions_right == nullptr || positions == nullptr || forces == nullptr || inv_masses == nullptr || etas == nullptr)
    {
        throw std::invalid_argument("compute_etas_null_pointer_error");
    }
    if (size == 0)
    {
        throw std::invalid_argument("compute_etas_size_zero_error");
    }
    constexpr auto zero = std::is_same<Treal_t, float>::value ? static_cast<Treal_t>(1E-6) : static_cast<Treal_t>(1E-13);
    if (dt_bd_b_gamma_bd < zero)
    {
        throw std::invalid_argument("compute_etas_zero_dt_bd_b_gamma_bd_error");
    }
    #endif
    
    auto max_eta = static_cast<Treal_t>(-1.0);
    #pragma omp parallel for simd safelen(16)
    for (auto atom_index = std::size_t{}; atom_index < size; ++atom_index)
    {
        auto& e = etas[atom_index];
        const auto& r_r = positions_right[atom_index];
        const auto& r = positions[atom_index];
        const auto& f = forces[atom_index];
        const auto& m_inv = inv_masses[atom_index];
        
        // compute eta
        e = r_r - r - f * (m_inv * dt_bd_b_gamma_bd);
        
        // compute the l^infinity norm of eta
        max_eta = std::max({std::fabs(e(0)), std::fabs(e(1)), std::fabs(e(2)), max_eta});
    }
    
    return max_eta;
}


template<class Treal_t>
void plus_minus(const Treal_t& a, const vector3<Treal_t>* x, const vector3<Treal_t>* y, vector3<Treal_t>* results1,
    vector3<Treal_t>* results2, const std::size_t& size)
{
    #ifndef NDEBUG
    if (x == nullptr || y == nullptr || results1 == nullptr || results2 == nullptr)
    {
        throw std::invalid_argument("plus_minus_null_pointer_error");
    }
    if (size == 0)
    {
        throw std::invalid_argument("plus_minus_size_zero_error");
    }
    #endif
    
    #pragma omp parallel for simd safelen(16)
    for (auto index = std::size_t{}; index < size; ++index)
    {
        auto& r1 = results1[index];
        auto& r2 = results2[index];
        
        r1 = y[index] + x[index] * a;
        r2 = y[index] - x[index] * a;
    }
}


template<class Treal_t>
void compute_hessian_U_t_eta_eps(const vector3<Treal_t>* forces_plus, const vector3<Treal_t>* forces_minus,
    vector3<Treal_t>* hessian_U_t_eta_eps, const std::size_t& size, const Treal_t& eps)
{
    #ifndef NDEBUG
    if (forces_plus == nullptr || forces_minus == nullptr || hessian_U_t_eta_eps == nullptr)
    {
        throw std::invalid_argument("compute_hessian_U_t_eta_eps_null_pointer_error");
    }
    if (size == 0)
    {
        throw std::invalid_argument("compute_hessian_U_t_eta_eps_zero_size_error");
    }
    constexpr auto zero = std::is_same<Treal_t, float>::value ? static_cast<Treal_t>(1E-6) : static_cast<Treal_t>(1E-13);
    if (eps < zero)
    {
        throw std::invalid_argument("compute_hessian_U_t_eta_eps_zero_eps_error");
    }
    #endif
    
    const auto k = static_cast<Treal_t>(0.5) / eps;
    #pragma omp parallel for simd safelen(16)
    for (auto atom_index = std::size_t{}; atom_index < size; ++atom_index)
    {
        auto& H = hessian_U_t_eta_eps[atom_index];
        const auto& f_p = forces_plus[atom_index];
        const auto& f_m = forces_minus[atom_index];
        
        H = (f_p - f_m) * k;
    }
}


template<class Treal_t>
void compute_effective_forces_first_bead(const vector3<Treal_t>* hessian_U_t_eta_eps, const vector3<Treal_t>* positions_right,
    const vector3<Treal_t>* positions, const Treal_t* masses, vector3<Treal_t>* forces, const std::size_t& size,
    const Treal_t& gamma_bd_b_dt_bd)
{
    #ifndef NDEBUG
    if (hessian_U_t_eta_eps == nullptr || positions_right == nullptr || positions == nullptr || masses == nullptr
        || forces == nullptr)
    {
        throw std::invalid_argument("compute_effective_forces_first_bead_null_pointer_error");
    }
    if (size == 0)
    {
        throw std::invalid_argument("compute_effective_forces_first_bead_zero_size_error");
    }
    constexpr auto zero = std::is_same<Treal_t, float>::value ? static_cast<Treal_t>(1E-6) : static_cast<Treal_t>(1E-13);
    if (gamma_bd_b_dt_bd < zero)
    {
        throw std::invalid_argument("compute_effective_forces_first_bead_zero_gamma_bd_b_dt_bd_error");
    }
    #endif
    
    #pragma omp parallel for simd safelen(16)
    for (auto atom_index = std::size_t{}; atom_index < size; ++atom_index)
    {
        auto& f = forces[atom_index];
        const auto& H = hessian_U_t_eta_eps[atom_index];
        const auto& r_r = positions_right[atom_index];
        const auto& r = positions[atom_index];
        const auto& m = masses[atom_index];
        
        f = ((f + H) + (r_r - r) * gamma_bd_b_dt_bd * m) * static_cast<Treal_t>(0.5);
    }
}


template<class Treal_t>
void compute_effective_forces_last_bead(const vector3<Treal_t>* forces_left, const vector3<Treal_t>* positions,
    const vector3<Treal_t>* positions_left, const Treal_t* masses, vector3<Treal_t>* forces, const std::size_t& size,
    const Treal_t& gamma_bd_b_dt_bd)
{
    // preconditions
    #ifndef NDEBUG
    if (forces_left == nullptr || positions == nullptr || positions_left == nullptr || masses == nullptr
        || forces == nullptr)
    {
        throw std::invalid_argument("compute_effective_forces_last_bead_null_pointer_error");
    }
    if (size == 0)
    {
        throw std::invalid_argument("compute_effective_forces_last_bead_zero_size_error");
    }
    constexpr auto zero = std::is_same<Treal_t, float>::value ? static_cast<Treal_t>(1E-6) : static_cast<Treal_t>(1E-13);
    if (gamma_bd_b_dt_bd < zero)
    {
        throw std::invalid_argument("compute_effective_forces_last_bead_zero_gamma_bd_b_dt_bd_error");
    }
    #endif
    
    #pragma omp parallel for simd safelen(16)
    for (auto atom_index = std::size_t{}; atom_index < size; ++atom_index)
    {
        auto& f = forces[atom_index];
        const auto& f_l = forces_left[atom_index];
        const auto& r = positions[atom_index];
        const auto& r_l = positions_left[atom_index];
        const auto& m = masses[atom_index];
        
        f = (f_l - (r - r_l) * gamma_bd_b_dt_bd * m) * static_cast<Treal_t>(0.5);
    }
}


template<class Treal_t>
void compute_effective_forces(const vector3<Treal_t>* forces_left, const vector3<Treal_t>* positions,
    const vector3<Treal_t>* positions_right, const vector3<Treal_t>* positions_left, const vector3<Treal_t>* hessian_U_t_eta_eps,
    const Treal_t* masses, vector3<Treal_t>* forces, const std::size_t& size, const Treal_t& gamma_bd_b_dt_bd)
{
    #ifndef NDEBUG
    if (forces_left == nullptr || positions == nullptr || positions_right == nullptr || positions_left == nullptr
        || hessian_U_t_eta_eps == nullptr || masses == nullptr || forces == nullptr)
    {
        throw std::invalid_argument("compute_effective_forces_null_pointer_error");
    }
    if (size == 0)
    {
        throw std::invalid_argument("compute_effective_forces_zero_size_error");
    }
    constexpr auto zero = std::is_same<Treal_t, float>::value ? static_cast<Treal_t>(1E-6) : static_cast<Treal_t>(1E-13);
    if (gamma_bd_b_dt_bd < zero)
    {
        throw std::invalid_argument("compute_effective_forces_zero_gamma_bd_b_dt_bd_error");
    }
    #endif
    
    #pragma omp parallel for simd safelen(16)
    for (auto atom_index = std::size_t{}; atom_index < size; ++atom_index)
    {
        auto& f = forces[atom_index];
        const auto& f_l = forces_left[atom_index];
        const auto& H = hessian_U_t_eta_eps[atom_index];
        const auto& r = positions[atom_index];
        const auto& r_r = positions_right[atom_index];
        const auto& r_l = positions_left[atom_index];
        const auto& m = masses[atom_index];
        
        f = ((f_l - f + H) - (r * static_cast<Treal_t>(2.0) - r_r - r_l) * gamma_bd_b_dt_bd * m) * static_cast<Treal_t>(0.5);
    }
}

} // namespace vvi::implementation::kernels

#endif
