#include "file_io.h"
#include "vector3.h"

#include <iostream> // std::cerr
#include <fstream> // std::ofstream
#include <filesystem> // ::path
#include <limits> // std::numeric_limits
#include <algorithm> // std::min


using real_t = double;

int main(int argc, char** argv)
{
    // extract arguments
    if (argc < 4)
    {
        std::cerr << "[msd]: Please pass\n"
            << "           1. path to .xyz files,\n"
            << "           2. base file name of .xyz files (without extension) and\n"
            << "           3. file count (indexes upto which will be suffixed to the base file name)\n"
            << "       as command-line arguments." << std::endl;
        return 1;
    }
    const auto path = std::string(argv[1]);
    if (path[path.size() - 1] != '/')
    {
        const_cast<std::string&>(path) += "/";
    }
    if (!std::filesystem::exists(path))
    {
        std::cerr << "[msd]: The provided path is not valid. The program will be aborted." << std::endl;
        return 1;
    }
    const auto base_file_name = std::string(argv[2]);
    if (std::filesystem::path(base_file_name).extension() != "")
    {
        std::cerr << "[msd]: The provided base file name is not valid. The program will be aborted." << std::endl;
        return 1;
    }
    const auto file_count = std::stoul(argv[3]);
    if (file_count < 1)
    {
        std::cerr << "[msd]: The provided file count is not valid. The program will be aborted." << std::endl;
        return 1;
    }
    
    // read all trajectories
    auto trajectory_matrix = std::vector<std::vector<vvi::t_md_frame<real_t>>>{};
    trajectory_matrix.reserve(file_count);
    for (auto file_index = std::size_t{}; file_index < file_count; ++file_index)
    {
        // make sure the file exists
        const auto full_file_name = path + base_file_name + std::to_string(file_index) + ".xyz";
        if (!std::filesystem::exists(full_file_name))
        {
            std::cerr << "[msd]: At least one of the input files does not exist. The program will be aborted." << std::endl;
            return 1;
        }
        
        // read the file
        try
        {
            trajectory_matrix.emplace_back(vvi::read_xyz_file<real_t>(full_file_name));
        }
        catch (const std::exception& ex)
        {
            std::cerr << "[msd]: An exception was caught: " << ex.what() << ". The program will be aborted." << std::endl;
            return 1;
        }
    }
    
    // ensure that all trajectories have the same frame count
    const auto& frame_count = trajectory_matrix[0].size();
    for (auto trajectory_index = std::size_t{}; trajectory_index < file_count; ++trajectory_index)
    {
        if (trajectory_matrix[trajectory_index].size() != frame_count)
        {
            std::cerr << "[msd]: Not all trajectories in the trajectory matrix have the same frame count. The program will be "
                << "aborted." << std::endl;
            return 1;
        }
    }
    
    // report
    std::cout << "[msd]: Read " << file_count << " .xyz files consisting of " << frame_count << " frames each.\n";
    
    // compute the mean square deviation and its standard deviation, averaging over all trajectories and all particles
    auto sum = 0.0;
    auto squared_sum = 0.0;
    auto max_value = -std::numeric_limits<real_t>::max();
    auto min_value = std::numeric_limits<real_t>::max();
    auto summed_values = std::vector<real_t>{};
    const auto& atom_count = trajectory_matrix[0][0].atom_names.size();
    for (const auto& trajectory : trajectory_matrix)
    {
        const auto& first_frame = trajectory.front();
        const auto& last_frame = trajectory.back();
        if (first_frame.atom_names.size() != atom_count || last_frame.atom_names.size() != atom_count)
        {
            std::cerr << "[msd]: Atom count mismatch detected between first and last frames of a trajectory. The program will "
                << "be aborted." << std::endl;
            return 1;
        }
        for (auto atom_index = std::size_t{}; atom_index < atom_count; ++atom_index)
        {
            // summing
            const auto deviation = last_frame.positions[atom_index] - first_frame.positions[atom_index];
            const auto summed_value = deviation(0) * deviation(0) + deviation(1) * deviation(1) + deviation(2) * deviation(2);
            sum += summed_value;
            squared_sum += summed_value * summed_value;
            
            // min, max
            min_value = std::min(summed_value, min_value);
            max_value = std::max(summed_value, max_value);
            
            // record this for writing a histogram column
            summed_values.push_back(summed_value);
        }
    }
    const auto msd = sum / (atom_count * trajectory_matrix.size());
    const auto std_dev = squared_sum / (atom_count * trajectory_matrix.size()) - msd * msd;
    
    // write a column of squared deviations to generate a histogram
    auto file = std::ofstream("squared_deviations.dat", std::ios_base::trunc);
    if (file.is_open())
    {
        // write
        for (const auto& summed_value : summed_values)
        {
            file << summed_value << "\n";
        }
        
        // close
        file.close();
        if (file.is_open())
        {
            std::cerr << "[msd]: Warning: could not close the squared deviations file after writing to it.";
        }
        
        // report
        std::cout << "[msd]: Wrote the squared deviations to .dat file.\n";
    }
    else
    {
        std::cerr << "[msd]: Warning: could not open the file to which the squared deviations were to be written.";
    }
    
    // report
    std::cout << "[msd]: Number of trajectories: " << trajectory_matrix.size() << "; MSD: " << msd << "; Std. deviation: "
        << std_dev << "; Min/max: " << min_value << "/" << max_value << ".\n";
    
    return 0;
}
