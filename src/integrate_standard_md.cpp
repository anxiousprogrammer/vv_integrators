#include "constants.h"
#include "integrate.h"
#include "system_view.h"
#include "file_io.h"

#include "force/i_force_element.h"
#include "force/i_thermostat.h"

#include "implementation/compute_forces.h"
#include "implementation/compute_potential_energy.h"

#include <iostream> // std::cout
#include <cstdlib> // std::getenv
#include <stdexcept> // std::invalid_argument
#include <iomanip> // std::setprecision
#include <filesystem> // std::filesystem::path


// implementation of standard variant in integrate.h

namespace vvi
{

//+//////////////////////////////////////////////
// helper functions
//+//////////////////////////////////////////////

template<class Treal_t>
void compute_forces_apply_thermostat(system_view<Treal_t>& sys, i_force_element<Treal_t>* force_element, i_thermostat<Treal_t>* thermostat)
{
    // compute force
    implementation::compute_forces(sys, force_element);
    
    // thermostat
    if (thermostat == nullptr)
    {
        return;
    }
    for (auto particle_index = std::size_t{}; particle_index < sys.size(); ++particle_index)
    {
        // apply the thermostat (if applicable)
        (*thermostat)(sys.get_velocity(particle_index), sys.get_force(particle_index));
    }
}

template<class Treal_t>
std::tuple<Treal_t, Treal_t, Treal_t, Treal_t> compute_pe_ke_te_t(const e_dim dim, const system_view<Treal_t>& sys,
    i_force_element<Treal_t>* force_element)
{
    // potential energy
    const auto potential_energy = implementation::compute_potential_energy(sys, force_element);
    
    // kinetic energy
    auto accumulator = 0.0;
    for (auto particle_index = std::size_t{}; particle_index < sys.size(); ++particle_index)
    {
        const auto& velocity = sys.get_velocity(particle_index);
        const auto& mass = sys.get_mass(particle_index);
        accumulator += static_cast<double>((velocity(0) * velocity(0) + velocity(1) * velocity(1)
            + velocity(2) * velocity(2)) * mass);
    }
    const auto kinetic_energy = static_cast<Treal_t>(accumulator) * static_cast<Treal_t>(0.5);
    
    // total energy
    const auto total_energy = potential_energy + kinetic_energy;
    
    // temperature
    const auto temperature = static_cast<Treal_t>(2.0) * kinetic_energy
        / (static_cast<Treal_t>(dim) * static_cast<Treal_t>(kb) * sys.size());
    
    return std::make_tuple(potential_energy, kinetic_energy, total_energy, temperature);
}



//+//////////////////////////////////////////////
// API function
//+//////////////////////////////////////////////

template<class Treal_t>
void integrate(const e_dim dim, system_view<Treal_t>& sys, const Treal_t& dt, const std::size_t& time_step_count,
    i_force_element<Treal_t>* force_element, i_thermostat<Treal_t>* thermostat, const std::string& write_file_name)
{
    // pre-conditions
    if (sys.size() == 0)
    {
        throw std::invalid_argument("integrate_standard_md_invalid_system_error");
    }
    constexpr auto zero = std::is_same<Treal_t, float>::value ? static_cast<Treal_t>(1E-6) : static_cast<Treal_t>(1E-13);
    if (dt < zero)
    {
        throw std::invalid_argument("integrate_invalid_dt_error");
    }
    if (time_step_count < 1)
    {
        throw std::invalid_argument("integrate_invalid_time_step_error");
    }
    if (force_element == nullptr)
    {
        throw std::invalid_argument("integrate_force_element_nullptr_error");
    }
    
    // logging interval
    auto log_interval = std::size_t{1};
    const auto log_interval_str = std::getenv("VVI_LOG_INTERVAL");
    if (log_interval_str != nullptr)
    {
        if (std::stol(log_interval_str) < 0)
        {
            throw std::runtime_error("integrate_invalid_log_interval_error");
        }
        log_interval = std::stoul(log_interval_str);
    }
    
    // trajectory interval
    auto trajectory_interval = std::size_t{1};
    const auto trajectory_interval_str = std::getenv("VVI_TRAJECTORY_INTERVAL");
    if (trajectory_interval_str != nullptr)
    {
        if (std::stol(trajectory_interval_str) < 0)
        {
            throw std::runtime_error("integrate_invalid_trajectory_interval_error");
        }
        trajectory_interval = std::stoul(trajectory_interval_str);
    }
    
    // equilibriation starts at...
    auto eq_cut_off = std::size_t{};
    const auto eq_cut_off_str = std::getenv("VVI_EQ_CUT_OFF");
    if (eq_cut_off_str != nullptr)
    {
        if (std::stol(eq_cut_off_str) < 0)
        {
            throw std::runtime_error("integrate_invalid_equilibriation_cut_off_error");
        }
        eq_cut_off = std::stoul(eq_cut_off_str);
    }
    
    // clear write-target
    if (std::filesystem::path(write_file_name).extension() != "")
    {
        throw std::runtime_error("integrate_filename_extension_error");
    }
    const auto write_file_name_w_ext = write_file_name + ".xyz";
    clear_target(write_file_name_w_ext);
    
    // inform user about the output
    if (log_interval != 0)
    {
        std::cout << "# timestep         Temperature                  PE                  KE                  TE" << std::endl;
    }
        
    // computation of forces for the first report
    if (trajectory_interval != 0 && eq_cut_off == 0)
    {
        compute_forces_apply_thermostat(sys, force_element, thermostat);
        write_coordinates(write_file_name_w_ext, 0, sys);
    }
    
    // recording of observables
    auto recorded_observables = std::vector<Treal_t>();
    
    // integration loop
    const auto dt_b_2 = dt * static_cast<Treal_t>(0.5);
    for (auto time_step = std::size_t{}; time_step < time_step_count; ++time_step)
    {
        // report
        if (log_interval != 0 && time_step % log_interval == 0)
        {
            const auto [potential_energy, kinetic_energy, total_energy, temperature] = compute_pe_ke_te_t(dim, sys,
                force_element);
            std::printf("%10lu%20.10f%20.10f%20.10f%20.10f\n", time_step, temperature, potential_energy, kinetic_energy,
                    total_energy);
            
            // record the observables
            if (thermostat == nullptr)
            {
                recorded_observables.push_back(total_energy);
            }
            else
            {
                recorded_observables.push_back(temperature);
            }
        }
        
        // update phase 1 (half-step velocity and full-step position)
        for (auto particle_index = std::size_t{}; particle_index < sys.size(); ++particle_index)
        {
            sys.get_velocity(particle_index) += sys.get_force(particle_index) * dt_b_2;
            sys.get_position(particle_index) += sys.get_velocity(particle_index) * dt;
        }
        
        // compute the force
        compute_forces_apply_thermostat(sys, force_element, thermostat);
        
        // update phase 2 (half-step velocity)
        for (auto particle_index = std::size_t{}; particle_index < sys.size(); ++particle_index)
        {
            sys.get_velocity(particle_index) += sys.get_force(particle_index) * dt_b_2;
        }
        
        // write the system's coordinates (as per the given frequency)
        if (trajectory_interval != 0 && time_step % trajectory_interval == 0 && time_step > eq_cut_off)
        {
            write_coordinates(write_file_name_w_ext, time_step + 1, sys);
        }
    }
    
    // observables report
    if (thermostat == nullptr)
    {
        // expect total energies to have been recorded
        if (recorded_observables.empty())
        {
            throw std::runtime_error("integrate_total_energies_not_recorded_error");
        }
        
        // compute the mean
        auto mean = 0.0;
        for (const auto& recorded_observable : recorded_observables)
        {
            mean += static_cast<double>(recorded_observable);
        }
        mean /= recorded_observables.size();
        
        // compute the std deviation
        auto std_dev = 0.0;
        for (const auto& recorded_observable : recorded_observables)
        {
            const auto dev = static_cast<double>(recorded_observable - mean);
            std_dev += dev * dev;
        }
        std_dev = std::sqrt(std_dev / recorded_observables.size());
        
        // report
        std::cout << "The std. deviation of " << recorded_observables.size() << " recorded total energies is: " << std_dev
            << " kJ/mol. This corresponds to " << std_dev / mean * 100.0 << "% drift.\n";
    }
    else
    {
        // make sure that there are records beyond the equilibriation cut off point
        if (recorded_observables.size() <= eq_cut_off)
        {
            throw std::runtime_error("integrate_temperatures_not_recorded_error");
        }
        
        // compute the mean beyond the transient (if applicable)
        auto mean = 0.0;
        auto record_count = std::size_t{};
        for (auto record_index = eq_cut_off; record_index < recorded_observables.size(); ++record_index)
        {
            mean += static_cast<double>(recorded_observables[record_index]);
            ++record_count;
        }
        mean /= record_count;
        
        // report
        if (eq_cut_off > 0)
        {
            std::cout << "The transient was cut off at " << eq_cut_off << ". ";
        }
        std::cout << "The mean of " << record_count << " recorded temperatures is: " << mean << " K.\n";
    }
}

// full instantiation
template void integrate<float>(const e_dim, system_view<float>&, const float&, const std::size_t&, i_force_element<float>*,
    i_thermostat<float>*, const std::string&);
template void integrate<double>(const e_dim, system_view<double>&, const double&, const std::size_t&, i_force_element<double>*,
    i_thermostat<double>*, const std::string&);

} // namespace vvi

// END OF IMPLEMENTATION
