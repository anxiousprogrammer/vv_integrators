#include "vector3.h"
#include "replica_view.h"
#include "scratch_cache.h"
#include "comm_cache.h"
#include "integrate.h"
#include "zero_force_element.h"

#include "force/langevin_thermostat.h"

#include <iostream> // std::cerr
#include <string> // std::stoul
#include <cmath> // std::sqrt
#include <random> // std::mt19937
#include <cstdlib> // std::getenv
#include <type_traits> // std::is_same
#include <thread> // std::this_thread::sleep_for

#include <mpi.h>


using real_t = float;

int main(int argc, char** argv)
{
    // MPI init
    auto provided = -1;
    MPI_Init_thread(&argc, &argv, MPI_THREAD_FUNNELED, &provided);
    if (provided != MPI_THREAD_FUNNELED)
    {
        std::cerr << "Fatal error: MPI does not provide required multithreading support!" << std::endl;
        MPI_Abort(MPI_COMM_WORLD, MPI_ERR_UNKNOWN);
    }
    
    // comm details
    auto comm_rank = -1;
    auto comm_size = -1;
    MPI_Comm_rank(MPI_COMM_WORLD, &comm_rank);
    MPI_Comm_size(MPI_COMM_WORLD, &comm_size);
    if (comm_rank < 0 || comm_size < 1)
    {
        std::cerr << "Fatal error: Could not obtain size and rank w.r.t the MPI world communicator!" << std::endl;
        MPI_Abort(MPI_COMM_WORLD, MPI_ERR_COMM);
    }
    
    // extract arguments
    if (argc < 8)
    {
        if (comm_rank == 0)
        {
            std::cerr << "Please pass:\n"
                << "    1. time-step (for Brownian dynamics), \n"
                << "    2. time-step count (for Brownian dynamics),\n"
                << "    3. gamma (for the Brownian dynamics),\n"
                << "    4. particle count, \n"
                << "    5. time-step (for the integration),\n"
                << "    6. time-step count (for the integration),\n"
                << "    7. gamma (for Langevin thermostat) and\n"
                << "    8. temperature (for the Langevin thermostat),\n"
                << "as command-line arguments." << std::endl;
        }
        MPI_Abort(MPI_COMM_WORLD, MPI_ERR_UNKNOWN);
    }
    const auto dt_bd = static_cast<real_t>(std::stod(argv[1]));
    const auto time_step_count_bd = std::stoul(argv[2]);
    const auto gamma_bd = static_cast<real_t>(std::stod(argv[3]));
    const auto particle_count = static_cast<real_t>(std::stoul(argv[4]));
    const auto dt = static_cast<real_t>(std::stod(argv[5]));
    const auto time_step_count = std::stoul(argv[6]);
    const auto gamma = static_cast<real_t>(std::stod(argv[7]));
    const auto temperature = static_cast<real_t>(std::stod(argv[8]));
    if (comm_rank == 0)
    {
        constexpr auto zero = std::is_same<real_t, float>::value ? static_cast<real_t>(1E-6) : static_cast<real_t>(1E-13);
        if (dt_bd < zero)
        {
            std::cerr << "Fatal error: Invalid time-step (for Brownian dynamics) provided." << std::endl;
            MPI_Abort(MPI_COMM_WORLD, MPI_ERR_UNKNOWN);
        }
        if (time_step_count_bd != static_cast<std::size_t>(comm_size))
        {
            std::cerr << "Fatal error: The time-step count for Brownian dynamics must be equal to the number of MPI processes!" << std::endl;
            MPI_Abort(MPI_COMM_WORLD, MPI_ERR_UNKNOWN);
        }
        if (gamma_bd < zero)
        {
            std::cerr << "Fatal error: Invalid gamma (for Brownian dynamics) provided." << std::endl;
            MPI_Abort(MPI_COMM_WORLD, MPI_ERR_UNKNOWN);
        }
        if (particle_count == 0)
        {
            std::cerr << "Fatal error: Invalid particle count provided." << std::endl;
            MPI_Abort(MPI_COMM_WORLD, MPI_ERR_UNKNOWN);
        }
        const auto particle_count_cbrt = std::cbrt(static_cast<real_t>(particle_count));
        if (std::fabs(std::floor(particle_count_cbrt) - particle_count_cbrt) > zero)
        {
            std::cerr << "Fatal error: Particle count must be cubic." << std::endl;
            return 1;
        }
        if (dt < zero)
        {
            std::cerr << "Fatal error: Invalid time-step (for integration) provided." << std::endl;
            MPI_Abort(MPI_COMM_WORLD, MPI_ERR_UNKNOWN);
        }
        if (time_step_count == 0)
        {
            std::cerr << "Fatal error: Invalid time-step count (for integration) provided." << std::endl;
            MPI_Abort(MPI_COMM_WORLD, MPI_ERR_UNKNOWN);
        }
        if (gamma < zero)
        {
            std::cerr << "Fatal error: Invalid gamma (for Langevin thermostat) provided." << std::endl;
            MPI_Abort(MPI_COMM_WORLD, MPI_ERR_UNKNOWN);
        }
        if (temperature < zero)
        {
            std::cerr << "Fatal error: Invalid temperature (for Langevin thermostat) provided." << std::endl;
            MPI_Abort(MPI_COMM_WORLD, MPI_ERR_UNKNOWN);
        }
        
        // report
        std::cout << "Provided parameters\n"
            << "    1. time-step (BD):                            " << dt_bd << "\n"
            << "    2. time-step count (BD):                      " << time_step_count_bd << "\n"
            << "    3. gamma (BD):                                " << gamma_bd << "\n"
            << "    4. particle count:                            " << particle_count << "\n"
            << "    5. time-step (integration):                   " << dt << "\n"
            << "    6. time-step count (integration):             " << time_step_count << "\n"
            << "    7. gamma (Langevin thermostat):               " << gamma << "\n"
            << "    8. temperature (Langevin thermostat):         " << temperature << ".\n" << std::endl;
    }
    
    // create system
    auto world = vvi::create_simulation_state<real_t>(particle_count);
    vvi::init_simulation_state(world.get());
    auto bead = vvi::replica_view<real_t>(world.get());
    
    // initialize the coordinates
    const auto box_size = static_cast<real_t>(5.0);
    const auto box_size_b_2 = box_size * static_cast<real_t>(0.5);
    const auto start_pos = vvi::vector3(-box_size_b_2, -box_size_b_2, -box_size_b_2);
    const auto n = static_cast<std::size_t>(std::cbrt(static_cast<real_t>(particle_count)));
    const auto step = box_size / static_cast<real_t>(n);
    auto random_engine = std::mt19937(std::random_device()());
    auto real_dist = std::uniform_real_distribution<real_t>(static_cast<real_t>(-0.05), static_cast<real_t>(0.05));
    for (auto particle_index = std::size_t{}; particle_index < bead.size(); ++particle_index)
    {
        const auto q = particle_index % (n * n);
        const auto z = q % n;
        const auto y = (q - q % n) / n;
        const auto x = (particle_index - q) / (n * n);
        auto& position = bead.get_position(particle_index);
        position = start_pos + vvi::vector3(static_cast<real_t>(x) * step + real_dist(random_engine),
            static_cast<real_t>(y) * step + real_dist(random_engine),
            static_cast<real_t>(z) * step + real_dist(random_engine));
    }
    
    // see if we're doing an NVE simulation
    auto thermostat = vvi::langevin_thermostat<real_t>(vvi::e_dim::d3, dt, static_cast<real_t>(1.0), gamma, temperature);
    vvi::i_thermostat<real_t>* thermostat_ptr = &thermostat;
    const auto thermostat_var = std::getenv("VVI_THERMOSTAT");
    if (thermostat_var != nullptr)
    {
        if (std::stoi(thermostat_var) == 0)
        {
            thermostat_ptr = nullptr;
        }
    }
    
    // report
    if (comm_rank == 0)
    {
        std::cout << "Started the simulation." << (thermostat_ptr == nullptr ? " Note: thermostat has been deactivated." : "")
            << std::endl;
    }
    
    // simulate
    auto fe = zero_force_element<real_t>();
    auto scache = vvi::create_scratch_cache<real_t>(bead.size());
    auto ccache = vvi::create_comm_cache<real_t>(bead.size());
    vvi::integrate(vvi::e_dim::d3, bead, dt_bd, gamma_bd, dt, time_step_count, &fe, thermostat_ptr, scache, ccache,
        MPI_COMM_WORLD, "many_body_msd_pmd");
    
    // report
    if (comm_rank == 0)
    {
        std::cout << "Completed the simulation successfully." << std::endl;
    }
    
    // Finalize
    MPI_Finalize();
    
    return 0;
}
