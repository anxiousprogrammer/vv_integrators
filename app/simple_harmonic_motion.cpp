#include "vector3.h"
#include "simulation_state.h"
#include "system_view.h"
#include "integrate.h"

#include "force/i_force_element_external.h"
#include "force/i_thermostat.h"

#include <iostream> // std::cerr
#include <string> // std::stoul
#include <cmath> // std::sqrt
#include <random> // std::mt19937_64
#include <type_traits> // std::is_same


using real_t = double;

//+//////////////////////////////////////////////
// force interface
//+//////////////////////////////////////////////

class simple_harmonic_motion final : public vvi::i_force_element_external<real_t>
{
public:
    
    //+/////////////////
    // lifecycle
    //+/////////////////
    
    simple_harmonic_motion(const real_t& stiffness_coefficient)
        : stiffness_coefficient_(stiffness_coefficient) {}
    
    //+/////////////////
    // main functionality
    //+/////////////////
    
    virtual real_t compute_potential_energy(const vvi::vector3<real_t>& position) final
    {
        // constants
        const auto& x = position(0);
        return static_cast<real_t>(0.5) * stiffness_coefficient_ * x * x;
    }
    
    virtual vvi::vector3<real_t> compute_force(const vvi::vector3<real_t>& position) final
    {
        // constants
        const auto& x = position(0);
        
        auto force = vvi::vector3<real_t>{};
        force(0) = -stiffness_coefficient_ * x;
        force(1) = real_t{};
        force(2) = real_t{};
        
        return force;
    }

private:
    
    double stiffness_coefficient_;
};


//+//////////////////////////////////////////////
// main function
//+//////////////////////////////////////////////

int main(int argc, char** argv)
{
    // extract arguments
    if (argc < 4)
    {
        std::cerr << "Please pass:\n"
            << "    1. time-step,\n"
            << "    2. time-step count and\n"
            << "    3. stiffness coefficient\n"
            << "as command-line arguments." << std::endl;
        return 1;
    }
    const auto dt = static_cast<real_t>(std::stod(argv[1]));
    const auto time_step_count = std::stoul(argv[2]);
    const auto stiffness_coefficient = static_cast<real_t>(std::stod(argv[3]));
    constexpr auto zero = std::is_same<real_t, float>::value ? static_cast<real_t>(1E-6) : static_cast<real_t>(1E-13);
    if (dt < zero)
    {
        std::cerr << "Fatal error: Invalid dt provided." << std::endl;
        return 1;
    }
    if (stiffness_coefficient < zero)
    {
        std::cerr << "Fatal error: Invalid stiffness coefficient provided." << std::endl;
        return 1;
    }
    
    // report
    std::cout << "Provided parameters\n"
        << "    1. time-step:             " << dt << "\n"
        << "    2. time-step count:       " << time_step_count << "\n"
        << "    3. stiffness coefficient: " << stiffness_coefficient << ".\n" << std::endl;
    
    // create system
    auto world = vvi::create_simulation_state<real_t>(1);
    vvi::init_simulation_state(world.get());
    auto world_view = vvi::system_view(world.get());
    
    // initialize the coordinates
    // particle 1
    world_view.get_position(0) = vvi::vector3<real_t>{static_cast<real_t>(-1.0), real_t{}, real_t{}};
    world_view.get_velocity(0) = vvi::vector3<real_t>{};
    world_view.get_force(0) = vvi::vector3<real_t>{};
    
    // report
    std::cout << "Started the simulation." << std::endl;
    
    // simulate
    auto fe = simple_harmonic_motion(stiffness_coefficient);
    vvi::integrate(vvi::e_dim::d1, world_view, dt, time_step_count, &fe, static_cast<vvi::i_thermostat<real_t>*>(nullptr),
        "simple_harmonic_motion");
    
    // report
    std::cout << "Completed the simulation successfully." << std::endl;
    
    return 0;
}
