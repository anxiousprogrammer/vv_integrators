#include "vector3.h"

#include "force/i_force_element_external.h"

#include <vector>
#include <functional>
#include <stdexcept>


#ifndef DOUBLE_WELL_2D_FORCE_ELEMENT_H
#define DOUBLE_WELL_2D_FORCE_ELEMENT_H

//+//////////////////////////////////////////////
// force interface
//+//////////////////////////////////////////////

template<class Treal_t>
class double_well_2d final : public vvi::i_force_element_external<Treal_t>
{
    //+/////////////////
    // helper function (readability)
    //+/////////////////
    
    template<class Tsome_real_t>
    inline constexpr Treal_t real_cast(const Tsome_real_t& val)
    {
        return static_cast<Treal_t>(val);
    }
    
public:
    
    //+/////////////////
    // main functionality
    //+/////////////////
    
    virtual Treal_t compute_potential_energy(const vvi::vector3<Treal_t>& position) final
    {
        // constants
        const auto& x = position(0);
        const auto& y = position(1);
        const auto x_2 = x * x;
        const auto x_4 = x_2 * x_2;
        const auto y_2 = y * y;
        const auto y_4 = y_2 * y_2;
        
        const auto potential_energy = real_cast(2.0) + real_cast(4.0) / real_cast(3.0) * x_4 - real_cast(2.0) * y_2 + y_4
            + real_cast(10.0) / real_cast(3.0) * x_2 * (y_2 - real_cast(1.0));
        
        return potential_energy;
    }
    
    virtual vvi::vector3<Treal_t> compute_force(const vvi::vector3<Treal_t>& position) final
    {
        // constants
        const auto& x = position(0);
        const auto& y = position(1);
        const auto x_2 = x * x;
        const auto y_2 = y * y;
        
        auto force = vvi::vector3<Treal_t>{};
        force(0) = real_cast(-4.0) / real_cast(3.0) * x * (real_cast(4.0) * x_2 + real_cast(5.0) * (y_2 - real_cast(1.0)));
        force(1) = real_cast(-4.0) * y * (y_2 + real_cast(5.0) / real_cast(3.0) * x_2 - real_cast(1.0));
        force(2) = real_cast(0.0);
        
        return force;
    }
};

#endif
