#include "vector3.h"

#include "force/i_force_element_external.h"


#ifndef ZERO_FORCE_ELEMENT_H
#define ZERO_FORCE_ELEMENT_H

//+//////////////////////////////////////////////
// force interface
//+//////////////////////////////////////////////

template<class Treal_t>
class zero_force_element final : public vvi::i_force_element_external<Treal_t>
{

public:
    
    //+/////////////////
    // main functionality
    //+/////////////////
    
    virtual Treal_t compute_potential_energy(const vvi::vector3<Treal_t>&) final
    {
        return static_cast<Treal_t>(0.0);
    }
    
    virtual vvi::vector3<Treal_t> compute_force(const vvi::vector3<Treal_t>&) final
    {
        return {};
    }
};

#endif
