#include "vector3.h"

#include "force/i_force_element_pairwise.h"


#ifndef LJ_FORCE_ELEMENT_H
#define LJ_FORCE_ELEMENT_H

//+//////////////////////////////////////////////
// force interface
//+//////////////////////////////////////////////

template<class Treal_t>
class lj_force_element final : public vvi::i_force_element_pairwise<Treal_t>
{
public:
    
    //+/////////////////
    // main functionality
    //+/////////////////
    
    virtual Treal_t compute_potential_energy(const vvi::vector3<Treal_t>& p1, const vvi::vector3<Treal_t>& p2) final
    {
        const auto r = p1 - p2;
        const auto r_l2_norm_sq = r(0) * r(0) + r(1) * r(1) + r(2) * r(2);
        const auto r_minus6 = std::pow(r_l2_norm_sq, -3);
        
        return static_cast<Treal_t>(4.0) * r_minus6 * (r_minus6 - static_cast<Treal_t>(1.0)); // TODO: sigma, epsilon = 1.0
    }
    
    virtual vvi::vector3<Treal_t> compute_force(const vvi::vector3<Treal_t>& p1, const vvi::vector3<Treal_t>& p2) final
    {
        // force on particle 1
        const auto r = p1 - p2;
        const auto r_l2_norm_sq = r(0) * r(0) + r(1) * r(1) + r(2) * r(2);
        const auto r_minus8 = std::pow(r_l2_norm_sq, -4);
        const auto r_minus6 = std::pow(r_l2_norm_sq, -3);
        const auto k1 = static_cast<Treal_t>(24.0) * r_minus8 * (static_cast<Treal_t>(2.0) * r_minus6
            - static_cast<Treal_t>(1.0));
        
        return r * k1;
    }
};

#endif
