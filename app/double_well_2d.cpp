#include "dim.h"
#include "vector3.h"
#include "simulation_state.h"
#include "system_view.h"
#include "double_well_2d_force_element.h"

#include "force/langevin_thermostat.h"

#include "integrate.h"

#include <iostream> // std::cerr
#include <string> // std::stoul
#include <cmath> // std::sqrt
#include <random> // std::mt19937
#include <cstdlib> // std::getenv
#include <type_traits> // std::is_same


using real_t = float;

//+//////////////////////////////////////////////
// main function
//+//////////////////////////////////////////////

int main(int argc, char** argv)
{
    // extract arguments
    if (argc < 6)
    {
        std::cerr << "Please pass:\n"
            << "    1. time-step, \n"
            << "    2. time-step count,\n"
            << "    3. particle count,\n"
            << "    4. gamma (for the Langevin thermostat) and\n"
            << "    5. temperature (for the Langevin thermostat)\n"
            << "as command-line arguments." << std::endl;
        return 1;
    }
    const auto dt = static_cast<real_t>(std::stod(argv[1]));
    const auto time_step_count = std::stoul(argv[2]);
    const auto particle_count = std::stoul(argv[3]);
    const auto gamma = static_cast<real_t>(std::stod(argv[4]));
    const auto temperature = static_cast<real_t>(std::stod(argv[5]));
    constexpr auto zero = std::is_same<real_t, float>::value ? static_cast<real_t>(1E-6) : static_cast<real_t>(1E-13);
    if (dt < zero)
    {
        std::cerr << "Fatal error: Invalid time-step provided." << std::endl;
        return 1;
    }
    if (time_step_count == 0)
    {
        std::cerr << "Fatal error: Invalid time-step count provided." << std::endl;
        return 1;
    }
    if (particle_count == 0)
    {
        std::cerr << "Fatal error: Invalid particle count provided." << std::endl;
        return 1;
    }
    if (gamma < zero)
    {
        std::cerr << "Fatal error: Invalid damping coefficient (for Langevin dynamics) provided." << std::endl;
        return 1;
    }
    if (temperature < zero)
    {
        std::cerr << "Fatal error: Invalid temperature (for Langevin dynamics) provided." << std::endl;
        return 1;
    }
    
    // report
    std::cout << "Provided parameters\n"
        << "    1. time-step:                            " << dt << "\n"
        << "    2. time-step count:                      " << time_step_count << "\n"
        << "    3. particle count:                       " << particle_count << "\n"
        << "    4. gamma (Langevin thermostat):          " << gamma << "\n"
        << "    5. temperature: (Langevin thermostat)    " << temperature << ".\n" << std::endl;
    
    // create system
    auto world = vvi::create_simulation_state<real_t>(particle_count);
    vvi::init_simulation_state(world.get());
    auto world_view = vvi::system_view<real_t>(world.get());
    
    // initialize the coordinates
    auto random_engine = std::mt19937(std::random_device()());
    auto real_dist = std::uniform_real_distribution<real_t>(static_cast<real_t>(-1.1), static_cast<real_t>(1.1));
    for (auto particle_index = std::size_t{}; particle_index < world_view.size(); ++particle_index)
    {
        auto& position = world_view.get_position(particle_index);
        position(0) = real_dist(random_engine);
        position(1) = real_dist(random_engine);
        position(2) = real_t{}; // 2D simulation
        
        // position(0) = -std::sqrt(static_cast<real_t>(5.0)) * static_cast<real_t>(0.5);
        // position(1) = real_t{};
        // position(2) = real_t{};
    }
    
    // see if we're doing an NVE simulation
    auto thermostat = vvi::langevin_thermostat<real_t>(vvi::e_dim::d2, dt, static_cast<real_t>(1.0), gamma, temperature);
    vvi::i_thermostat<real_t>* thermostat_ptr = &thermostat;
    const auto thermostat_var = std::getenv("VVI_THERMOSTAT");
    if (thermostat_var != nullptr)
    {
        if (std::stoi(thermostat_var) == 0)
        {
            thermostat_ptr = nullptr;
        }
    }
    
    // report
    std::cout << "Started the simulation." << (thermostat_ptr == nullptr ? " Note: thermostat has been deactivated." : "")
        << std::endl;
    
    // simulate
    auto fe = double_well_2d<real_t>();
    vvi::integrate(vvi::e_dim::d2, world_view, dt, time_step_count, &fe, thermostat_ptr, "double_well_2d");
    
    // report
    std::cout << "Completed the simulation successfully." << std::endl;
    
    return 0;
}
