#!/bin/bash
printf "\n---------------------------------------\nTEST: vector3\n"
./test_vector3

printf "\n---------------------------------------\nTEST: vector3_array\n"
./test_vector3_array

printf "\n---------------------------------------\nTEST: array_view\n"
./test_array_view

printf "\n---------------------------------------\nTEST: system_view\n"
./test_system_view

printf "\n---------------------------------------\nTEST: replica_view\n"
./test_replica_view
