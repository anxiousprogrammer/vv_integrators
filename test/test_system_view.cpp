/// \file This file contains test(s) for 'system_view.h'.

#include "simulation_state.h"
#include "system_view.h"

#include "gtest/gtest.h"

#include <limits> // std::numeric_limits


using namespace vvi;

template<class Treal_t>
void test_lifecycle1()
{
    // constructibility
    auto state = create_simulation_state<Treal_t>(5);
    const auto sys_view1 = system_view<Treal_t>(state.get());
    
    #ifndef NDEBUG
    ASSERT_THROW(const auto err_sys_view = system_view<Treal_t>(nullptr), std::invalid_argument);
    #endif
}

TEST(testsuite_system_view, lifecycle1_sp)
{
    test_lifecycle1<float>();
}

TEST(testsuite_system_view, lifecycle1_dp)
{
    test_lifecycle1<double>();
}


template<class Treal_t>
void test_access()
{
    // create a test instance
    auto state = create_simulation_state<Treal_t>(5);
    auto sys_view1 = system_view<Treal_t>(state.get());
    
    // 0. size
    ASSERT_EQ(5, sys_view1.size());
    
    // initialize state (tests simultaneously the non-const qualified access)
    for (auto particle_index = std::size_t{}; particle_index < sys_view1.size(); ++particle_index)
    {
        const auto position_value = static_cast<Treal_t>(1.0 * particle_index);
        *(sys_view1.get_positions_view().begin() + particle_index) = vector3<Treal_t>(position_value, {}, {});
        
        const auto force_value = static_cast<Treal_t>(2.0 * particle_index);
        *(sys_view1.get_forces_view().begin() + particle_index) = vector3<Treal_t>(force_value, {}, {});
        
        *(sys_view1.get_masses_view().begin() + particle_index) = static_cast<Treal_t>(3.0 * particle_index);
        
        *(sys_view1.get_cached_inv_masses_view().begin() + particle_index) = static_cast<Treal_t>(4.0 * particle_index);
    }
    
    // const-qualified access
    const auto sys_view2 = system_view<Treal_t>(state.get());
    
    // 1. positions
    constexpr auto tolerance = std::numeric_limits<Treal_t>::epsilon();
    ASSERT_NEAR(static_cast<Treal_t>(0.0), (*sys_view2.get_positions_view().begin())(0), tolerance);
    ASSERT_NEAR(static_cast<Treal_t>(1.0), (*(sys_view2.get_positions_view().begin() + 1))(0), tolerance);
    ASSERT_NEAR(static_cast<Treal_t>(2.0), (*(sys_view2.get_positions_view().begin() + 2))(0), tolerance);
    ASSERT_NEAR(static_cast<Treal_t>(3.0), (*(sys_view2.get_positions_view().begin() + 3))(0), tolerance);
    ASSERT_NEAR(static_cast<Treal_t>(4.0), (*(sys_view2.get_positions_view().begin() + 4))(0), tolerance);
    
    // 2. forces
    ASSERT_NEAR(static_cast<Treal_t>(0.0), (*sys_view2.get_forces_view().begin())(0), tolerance);
    ASSERT_NEAR(static_cast<Treal_t>(2.0), (*(sys_view2.get_forces_view().begin() + 1))(0), tolerance);
    ASSERT_NEAR(static_cast<Treal_t>(4.0), (*(sys_view2.get_forces_view().begin() + 2))(0), tolerance);
    ASSERT_NEAR(static_cast<Treal_t>(6.0), (*(sys_view2.get_forces_view().begin() + 3))(0), tolerance);
    ASSERT_NEAR(static_cast<Treal_t>(8.0), (*(sys_view2.get_forces_view().begin() + 4))(0), tolerance);
    
    // 3. masses
    ASSERT_NEAR(static_cast<Treal_t>(0.0), (*sys_view2.get_masses_view().begin()), tolerance);
    ASSERT_NEAR(static_cast<Treal_t>(3.0), (*(sys_view2.get_masses_view().begin() + 1)), tolerance);
    ASSERT_NEAR(static_cast<Treal_t>(6.0), (*(sys_view2.get_masses_view().begin() + 2)), tolerance);
    ASSERT_NEAR(static_cast<Treal_t>(9.0), (*(sys_view2.get_masses_view().begin() + 3)), tolerance);
    ASSERT_NEAR(static_cast<Treal_t>(12.0), (*(sys_view2.get_masses_view().begin() + 4)), tolerance);
    
    // 4. inverse masses
    ASSERT_NEAR(static_cast<Treal_t>(0.0), (*sys_view2.get_cached_inv_masses_view().begin()), tolerance);
    ASSERT_NEAR(static_cast<Treal_t>(4.0), (*(sys_view2.get_cached_inv_masses_view().begin() + 1)), tolerance);
    ASSERT_NEAR(static_cast<Treal_t>(8.0), (*(sys_view2.get_cached_inv_masses_view().begin() + 2)), tolerance);
    ASSERT_NEAR(static_cast<Treal_t>(12.0), (*(sys_view2.get_cached_inv_masses_view().begin() + 3)), tolerance);
    ASSERT_NEAR(static_cast<Treal_t>(16.0), (*(sys_view2.get_cached_inv_masses_view().begin() + 4)), tolerance);
}

TEST(testsuite_system_view, access_sp)
{
    test_access<float>();
}

TEST(testsuite_system_view, access_dp)
{
    test_access<double>();
}


template<class Treal_t>
void test_lifecycle2()
{
    // create a test instance
    const auto size = std::size_t{5};
    auto state = create_simulation_state<Treal_t>(size);
    auto sys_view1 = system_view<Treal_t>(state.get());
    
    // initialize state (tests simultaneously the non-const qualified access)
    for (auto particle_index = std::size_t{}; particle_index < sys_view1.size(); ++particle_index)
    {
        const auto position_value = static_cast<Treal_t>(1.0 * particle_index);
        *(sys_view1.get_positions_view().begin() + particle_index) = vector3<Treal_t>(position_value, {}, {});
        
        const auto force_value = static_cast<Treal_t>(2.0 * particle_index);
        *(sys_view1.get_forces_view().begin() + particle_index) = vector3<Treal_t>(force_value, {}, {});
        
        *(sys_view1.get_masses_view().begin() + particle_index) = static_cast<Treal_t>(3.0 * particle_index);
        
        *(sys_view1.get_cached_inv_masses_view().begin() + particle_index) = static_cast<Treal_t>(4.0 * particle_index);
    }
    
    // lambda for testing purposes
    const auto test_contents = [size](const system_view<Treal_t>& sv)
    {
        // 0. size
        ASSERT_EQ(size, sv.size());
        
        // 1. positions
        constexpr auto tolerance = std::numeric_limits<Treal_t>::epsilon();
        ASSERT_NEAR(static_cast<Treal_t>(0.0), (*sv.get_positions_view().begin())(0), tolerance);
        ASSERT_NEAR(static_cast<Treal_t>(1.0), (*(sv.get_positions_view().begin() + 1))(0), tolerance);
        ASSERT_NEAR(static_cast<Treal_t>(2.0), (*(sv.get_positions_view().begin() + 2))(0), tolerance);
        ASSERT_NEAR(static_cast<Treal_t>(3.0), (*(sv.get_positions_view().begin() + 3))(0), tolerance);
        ASSERT_NEAR(static_cast<Treal_t>(4.0), (*(sv.get_positions_view().begin() + 4))(0), tolerance);
        
        // 2. forces
        ASSERT_NEAR(static_cast<Treal_t>(0.0), (*sv.get_forces_view().begin())(0), tolerance);
        ASSERT_NEAR(static_cast<Treal_t>(2.0), (*(sv.get_forces_view().begin() + 1))(0), tolerance);
        ASSERT_NEAR(static_cast<Treal_t>(4.0), (*(sv.get_forces_view().begin() + 2))(0), tolerance);
        ASSERT_NEAR(static_cast<Treal_t>(6.0), (*(sv.get_forces_view().begin() + 3))(0), tolerance);
        ASSERT_NEAR(static_cast<Treal_t>(8.0), (*(sv.get_forces_view().begin() + 4))(0), tolerance);
        
        // 3. masses
        ASSERT_NEAR(static_cast<Treal_t>(0.0), (*sv.get_masses_view().begin()), tolerance);
        ASSERT_NEAR(static_cast<Treal_t>(3.0), (*(sv.get_masses_view().begin() + 1)), tolerance);
        ASSERT_NEAR(static_cast<Treal_t>(6.0), (*(sv.get_masses_view().begin() + 2)), tolerance);
        ASSERT_NEAR(static_cast<Treal_t>(9.0), (*(sv.get_masses_view().begin() + 3)), tolerance);
        ASSERT_NEAR(static_cast<Treal_t>(12.0), (*(sv.get_masses_view().begin() + 4)), tolerance);
        
        // 4. inverse masses
        ASSERT_NEAR(static_cast<Treal_t>(0.0), (*sv.get_cached_inv_masses_view().begin()), tolerance);
        ASSERT_NEAR(static_cast<Treal_t>(4.0), (*(sv.get_cached_inv_masses_view().begin() + 1)), tolerance);
        ASSERT_NEAR(static_cast<Treal_t>(8.0), (*(sv.get_cached_inv_masses_view().begin() + 2)), tolerance);
        ASSERT_NEAR(static_cast<Treal_t>(12.0), (*(sv.get_cached_inv_masses_view().begin() + 3)), tolerance);
        ASSERT_NEAR(static_cast<Treal_t>(16.0), (*(sv.get_cached_inv_masses_view().begin() + 4)), tolerance);
    };
    
    // copy constructor
    auto sys_view2 = sys_view1;
    test_contents(sys_view2);
    
    // move constructor
    const auto sys_view3 = system_view(std::move(sys_view2)); // forced move constructor
    test_contents(sys_view2);
    test_contents(sys_view3);
    
    // copy operator
    sys_view2 = sys_view3; // forced copy operator
    test_contents(sys_view2);
    test_contents(sys_view3);
    
    // move operator
    const auto sys_view4 = std::move(sys_view2); // forced move operator
    test_contents(sys_view2);
    test_contents(sys_view4);
}

TEST(testsuite_system_view, lifecycle2_sp)
{
    test_lifecycle2<float>();
}

TEST(testsuite_system_view, lifecycle2_dp)
{
    test_lifecycle2<double>();
}

// END OF TESTFILE
