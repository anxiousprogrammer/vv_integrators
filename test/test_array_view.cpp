/// \file This file contains test(s) for 'replica_view.h'.

#include "vector3.h"
#include "array_view.h"

#include "gtest/gtest.h"

#include <limits> // std::numeric_limits
#include <vector> // std::vector


using namespace vvi;

template<class Treal_t>
void test_lifecycle1()
{
    // default constructibility
    auto view1 = array_view<Treal_t>();
    
    // init constructibility
    auto vec1 = std::vector<Treal_t>{static_cast<Treal_t>(1.0), static_cast<Treal_t>(2.0), static_cast<Treal_t>(3.0),
        static_cast<Treal_t>(4.0)};
    const auto view2 = array_view<Treal_t>(vec1.data(), vec1.data() + vec1.size(), 4);
    
    #ifndef NDEBUG
    ASSERT_THROW(const auto view_err = array_view<Treal_t>(vec1.data(), vec1.data(), 4), std::invalid_argument);
    #endif
}

TEST(testsuite_array_view, lifecycle1_sp)
{
    test_lifecycle1<float>();
}

TEST(testsuite_array_view, lifecycle1_dp)
{
    test_lifecycle1<double>();
}


template<class Treal_t>
void test_access()
{
    // create a test instance
    const auto size = std::size_t{500};
    auto vec = std::vector<Treal_t>(size);
    auto view = array_view(vec.data(), vec.data() + vec.size(), size);
    
    // 0. size
    ASSERT_EQ(size, view.size());
    
    // initialize state
    for (auto index = std::size_t{}; index < vec.size(); ++index)
    {
        vec[index] = static_cast<Treal_t>(index + 1);
    }
    
    // 1. const-qualified access
    constexpr auto tolerance = std::numeric_limits<Treal_t>::epsilon();
    auto index = std::size_t{};
    for (auto iter = view.begin(); iter != view.end(); ++iter, ++index)
    {
        ASSERT_NEAR(static_cast<Treal_t>(index + 1), *iter, tolerance);
    }
    
    // 2. non-const-qualified access
    for (auto iter = view.begin(); iter != view.end(); ++iter)
    {
        *iter = static_cast<Treal_t>(-1.0);
    }
    
    // test
    for (auto iter = view.begin(); iter != view.end(); ++iter)
    {
        ASSERT_NEAR(static_cast<Treal_t>(-1.0), *iter, tolerance);
    }
}

TEST(testsuite_array_view, access_sp)
{
    test_access<float>();
}

TEST(testsuite_array_view, access_dp)
{
    test_access<double>();
}


template<class Treal_t>
void test_lifecycle2()
{
    // create a test instance
    const auto size = std::size_t{500};
    auto vec = std::vector<Treal_t>(size);
    auto view = array_view(vec.data(), vec.data() + vec.size(), size);
    
    // initialize state
    for (auto index = std::size_t{}; index < vec.size(); ++index)
    {
        vec[index] = static_cast<Treal_t>(index + 1);
    }
    
    // tester lambda
    const auto test_contents = [&size](const array_view<Treal_t>& v)
    {
        constexpr auto tolerance = std::numeric_limits<Treal_t>::epsilon();
        auto index = std::size_t{};
        for (auto iter = v.begin(); iter != v.end(); ++iter, ++index)
        {
            ASSERT_NEAR(static_cast<Treal_t>(index + 1), *iter, tolerance);
        }
        ASSERT_EQ(size, index);
    };
    
    // 1. copy constructor
    auto view2 = view;
    test_contents(view2);
    
    // 2. move constructor
    const auto view3 = array_view(std::move(view2)); // forced move constructor
    test_contents(view2);
    test_contents(view3);
    
    // 3. copy operator
    view2 = view3; // forced copy operator
    test_contents(view2);
    test_contents(view3);
    
    // 4. move operator
    const auto view4 = std::move(view2); // forced move operator
    test_contents(view2);
    test_contents(view4);
}

TEST(testsuite_array_view, lifecycle2_sp)
{
    test_lifecycle2<float>();
}

TEST(testsuite_array_view, lifecycle2_dp)
{
    test_lifecycle2<double>();
}

// END OF TESTFILE
