/// \file This file contains test(s) for 'vector3_array'.

#include "vector3_array.h"

#include "gtest/gtest.h"

#include <limits> // std::numeric_limits


using namespace vvi;

template<class Treal_t>
void test_lifecycle1()
{
    // default constructibility
    auto test_va = vector3_array<Treal_t>();
}

TEST(testsuite_vector3_array, lifecycle1_sp)
{
    test_lifecycle1<float>();
}

TEST(testsuite_vector3_array, lifecycle1_dp)
{
    test_lifecycle1<double>();
}


template<class Treal_t>
void test_access()
{
    // create a test instance
    auto test_va = vector3_array<Treal_t>{};
    const auto size = std::size_t{100};
    test_va.resize(size);
    
    // size
    ASSERT_EQ(size, test_va.size());
    
    // error case
    #ifndef NDEBUG
    ASSERT_THROW(test_va[size], std::invalid_argument);
    ASSERT_THROW(test_va[size + 1], std::invalid_argument);
    #endif
    
    // non-const-qualified operator[]
    constexpr auto tolerance = std::numeric_limits<Treal_t>::epsilon();
    for (auto index = std::size_t{}; index < test_va.size(); ++index)
    {
        test_va[index] =
            vector3<Treal_t>(static_cast<Treal_t>(index), static_cast<Treal_t>(index + 1), static_cast<Treal_t>(index + 2));
    }
    
    // const-qualified operator[]
    const auto& first_v = test_va[0];
    ASSERT_NEAR(static_cast<Treal_t>(0.0), first_v(0), tolerance);
    ASSERT_NEAR(static_cast<Treal_t>(1.0), first_v(1), tolerance);
    ASSERT_NEAR(static_cast<Treal_t>(2.0), first_v(2), tolerance);
    for (auto index = std::size_t{}; index < test_va.size(); ++index)
    {
        const auto& v = test_va[index];
        ASSERT_NEAR(static_cast<Treal_t>(index), v(0), tolerance);
        ASSERT_NEAR(static_cast<Treal_t>(index + 1), v(1), tolerance);
        ASSERT_NEAR(static_cast<Treal_t>(index + 2), v(2), tolerance);
    }
    
    // non-const-qualified raw access
    auto index = std::size_t{};
    const auto offset = std::size_t{100};
    for (auto ptr = test_va.data(); ptr != test_va.data() + size; ++ptr, ++index)
    {
        *ptr = vector3<Treal_t>(static_cast<Treal_t>(index + offset), static_cast<Treal_t>(index + offset + 1),
            static_cast<Treal_t>(index + offset + 2));
    }
    
    // const-qualified raw access
    const auto& first_v_raw = *test_va.data();
    ASSERT_NEAR(static_cast<Treal_t>(100.0), first_v_raw(0), tolerance);
    ASSERT_NEAR(static_cast<Treal_t>(101.0), first_v_raw(1), tolerance);
    ASSERT_NEAR(static_cast<Treal_t>(102.0), first_v_raw(2), tolerance);
    index = {};
    for (auto ptr = test_va.data(); ptr != test_va.data() + size; ++ptr, ++index)
    {
        const auto& v = *ptr;
        ASSERT_NEAR(static_cast<Treal_t>(index + offset), v(0), tolerance);
        ASSERT_NEAR(static_cast<Treal_t>(index + offset + 1), v(1), tolerance);
        ASSERT_NEAR(static_cast<Treal_t>(index + offset + 2), v(2), tolerance);
    }
}

TEST(testsuite_vector3_array, access_sp)
{
    test_access<float>();
}

TEST(testsuite_vector3_array, access_dp)
{
    test_access<double>();
}


template<class Treal_t>
void test_lifecycle2()
{
    // create a test instance
    const auto size = std::size_t{100};
    const auto create_initialized_vector3_array = [size]()
    {
        auto test_va = vector3_array<Treal_t>{};
        test_va.resize(size);
        for (auto index = std::size_t{}; index < test_va.size(); ++index)
        {
            test_va[index] =
                vector3<Treal_t>(static_cast<Treal_t>(index), static_cast<Treal_t>(index + 1), static_cast<Treal_t>(index + 2));
        }
        return test_va;
    };
    const auto test_va = create_initialized_vector3_array(); // needs to be const to ensure copy control!
    
    // lambdas for testing purposes
    const auto test_contents = [size](const vector3_array<Treal_t>& va)
    {
        ASSERT_EQ(size, va.size());
        constexpr auto tolerance = std::numeric_limits<Treal_t>::epsilon();
        for (auto index = std::size_t{}; index < va.size(); ++index)
        {
            const auto& v = va[index];
            ASSERT_NEAR(static_cast<Treal_t>(index), v(0), tolerance);
            ASSERT_NEAR(static_cast<Treal_t>(index + 1), v(1), tolerance);
            ASSERT_NEAR(static_cast<Treal_t>(index + 2), v(2), tolerance);
        }
    };
    const auto test_emptiness = [](const vector3_array<Treal_t>& va)
    {
        ASSERT_EQ(0, va.size());
        ASSERT_EQ(nullptr, va.data());
    };
    
    // copy constructor
    auto test_va2 = test_va;
    test_contents(test_va2);
    
    // move constructor
    const auto test_va3 = vector3_array(std::move(test_va2)); // forced move constructor
    test_emptiness(test_va2);
    test_contents(test_va3);
    
    // copy operator
    test_va2 = test_va3; // forced copy operator
    test_contents(test_va2);
    test_contents(test_va3);
    
    // move operator
    const auto test_va4 = std::move(test_va2); // forced move operator
    test_emptiness(test_va2);
    test_contents(test_va4);
}

TEST(testsuite_vector3_array, lifecycle2_sp)
{
    test_lifecycle2<float>();
}

TEST(testsuite_vector3_array, lifecycle2_dp)
{
    test_lifecycle2<double>();
}

// END OF TESTFILE
