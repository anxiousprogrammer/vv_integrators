/// \file This file contains test(s) for 'replica_view.h'.

#include "simulation_state.h"
#include "replica_view.h"

#include "gtest/gtest.h"

#include <limits> // std::numeric_limits


using namespace vvi;

template<class Treal_t>
void test_lifecycle1()
{
    // constructibility
    auto state = create_simulation_state<Treal_t>(5);
    const auto rep_view1 = replica_view<Treal_t>(state.get());
    
    #ifndef NDEBUG
    ASSERT_THROW(const auto err_rep_view = replica_view<Treal_t>(nullptr), std::invalid_argument);
    #endif
}

TEST(testsuite_replica_view, lifecycle1_sp)
{
    test_lifecycle1<float>();
}

TEST(testsuite_replica_view, lifecycle1_dp)
{
    test_lifecycle1<double>();
}


template<class Treal_t>
void test_access()
{
    // create a test instance
    auto state = create_simulation_state<Treal_t>(5);
    auto rep_view1 = replica_view<Treal_t>(state.get());
    
    // 0. size
    ASSERT_EQ(5, rep_view1.size());
    
    // initialize state (tests simultaneously the non-const qualified access)
    for (auto particle_index = std::size_t{}; particle_index < rep_view1.size(); ++particle_index)
    {
        const auto position_value = static_cast<Treal_t>(1.0 * particle_index);
        *(rep_view1.get_positions_view().begin() + particle_index) = vector3<Treal_t>(position_value, {}, {});
        
        const auto force_value = static_cast<Treal_t>(2.0 * particle_index);
        *(rep_view1.get_forces_view().begin() + particle_index) = vector3<Treal_t>(force_value, {}, {});
        
        *(rep_view1.get_masses_view().begin() + particle_index) = static_cast<Treal_t>(3.0 * particle_index);
        
        *(rep_view1.get_cached_inv_masses_view().begin() + particle_index) = static_cast<Treal_t>(4.0 * particle_index);
        
        const auto eta_value = static_cast<Treal_t>(5.0 * particle_index);
        *(rep_view1.get_cached_etas_view().begin() + particle_index) = vector3<Treal_t>(eta_value, {}, {});
    }
    
    // const-qualified access
    const auto rep_view2 = replica_view<Treal_t>(state.get());
    
    // 1. position
    constexpr auto tolerance = std::numeric_limits<Treal_t>::epsilon();
    ASSERT_NEAR(static_cast<Treal_t>(0.0), (*rep_view2.get_positions_view().begin())(0), tolerance);
    ASSERT_NEAR(static_cast<Treal_t>(1.0), (*(rep_view2.get_positions_view().begin() + 1))(0), tolerance);
    ASSERT_NEAR(static_cast<Treal_t>(2.0), (*(rep_view2.get_positions_view().begin() + 2))(0), tolerance);
    ASSERT_NEAR(static_cast<Treal_t>(3.0), (*(rep_view2.get_positions_view().begin() + 3))(0), tolerance);
    ASSERT_NEAR(static_cast<Treal_t>(4.0), (*(rep_view2.get_positions_view().begin() + 4))(0), tolerance);
    
    // 2. force
    ASSERT_NEAR(static_cast<Treal_t>(0.0), (*rep_view2.get_forces_view().begin())(0), tolerance);
    ASSERT_NEAR(static_cast<Treal_t>(2.0), (*(rep_view2.get_forces_view().begin() + 1))(0), tolerance);
    ASSERT_NEAR(static_cast<Treal_t>(4.0), (*(rep_view2.get_forces_view().begin() + 2))(0), tolerance);
    ASSERT_NEAR(static_cast<Treal_t>(6.0), (*(rep_view2.get_forces_view().begin() + 3))(0), tolerance);
    ASSERT_NEAR(static_cast<Treal_t>(8.0), (*(rep_view2.get_forces_view().begin() + 4))(0), tolerance);
    
    // 3. masses
    ASSERT_NEAR(static_cast<Treal_t>(0.0), (*rep_view2.get_masses_view().begin()), tolerance);
    ASSERT_NEAR(static_cast<Treal_t>(3.0), (*(rep_view2.get_masses_view().begin() + 1)), tolerance);
    ASSERT_NEAR(static_cast<Treal_t>(6.0), (*(rep_view2.get_masses_view().begin() + 2)), tolerance);
    ASSERT_NEAR(static_cast<Treal_t>(9.0), (*(rep_view2.get_masses_view().begin() + 3)), tolerance);
    ASSERT_NEAR(static_cast<Treal_t>(12.0), (*(rep_view2.get_masses_view().begin() + 4)), tolerance);
    
    // 4. inv. masses
    ASSERT_NEAR(static_cast<Treal_t>(0.0), (*rep_view2.get_cached_inv_masses_view().begin()), tolerance);
    ASSERT_NEAR(static_cast<Treal_t>(4.0), (*(rep_view2.get_cached_inv_masses_view().begin() + 1)), tolerance);
    ASSERT_NEAR(static_cast<Treal_t>(8.0), (*(rep_view2.get_cached_inv_masses_view().begin() + 2)), tolerance);
    ASSERT_NEAR(static_cast<Treal_t>(12.0), (*(rep_view2.get_cached_inv_masses_view().begin() + 3)), tolerance);
    ASSERT_NEAR(static_cast<Treal_t>(16.0), (*(rep_view2.get_cached_inv_masses_view().begin() + 4)), tolerance);
    
    // 5. etas
    ASSERT_NEAR(static_cast<Treal_t>(0.0), (*rep_view2.get_cached_etas_view().begin())(0), tolerance);
    ASSERT_NEAR(static_cast<Treal_t>(5.0), (*(rep_view2.get_cached_etas_view().begin() + 1))(0), tolerance);
    ASSERT_NEAR(static_cast<Treal_t>(10.0), (*(rep_view2.get_cached_etas_view().begin() + 2))(0), tolerance);
    ASSERT_NEAR(static_cast<Treal_t>(15.0), (*(rep_view2.get_cached_etas_view().begin() + 3))(0), tolerance);
    ASSERT_NEAR(static_cast<Treal_t>(20.0), (*(rep_view2.get_cached_etas_view().begin() + 4))(0), tolerance);
}

TEST(testsuite_replica_view, access_sp)
{
    test_access<float>();
}

TEST(testsuite_replica_view, access_dp)
{
    test_access<double>();
}


template<class Treal_t>
void test_lifecycle2()
{
    // create a test instance
    const auto size = std::size_t{5};
    auto state = create_simulation_state<Treal_t>(size);
    auto rep_view1 = replica_view<Treal_t>(state.get());
    
    // initialize state (tests simultaneously the non-const qualified access)
    for (auto particle_index = std::size_t{}; particle_index < rep_view1.size(); ++particle_index)
    {
        const auto position_value = static_cast<Treal_t>(1.0 * particle_index);
        *(rep_view1.get_positions_view().begin() + particle_index) = vector3<Treal_t>(position_value, {}, {});
        
        const auto force_value = static_cast<Treal_t>(2.0 * particle_index);
        *(rep_view1.get_forces_view().begin() + particle_index) = vector3<Treal_t>(force_value, {}, {});
        
        *(rep_view1.get_masses_view().begin() + particle_index) = static_cast<Treal_t>(3.0 * particle_index);
        
        *(rep_view1.get_cached_inv_masses_view().begin() + particle_index) = static_cast<Treal_t>(4.0 * particle_index);
        
        const auto eta_value = static_cast<Treal_t>(5.0 * particle_index);
        *(rep_view1.get_cached_etas_view().begin() + particle_index) = vector3<Treal_t>(eta_value, {}, {});
    }
    
    // lambda for testing purposes
    const auto test_contents = [size](const replica_view<Treal_t>& rv)
    {
        // 0. size
        ASSERT_EQ(size, rv.size());
        
        // 1. position
        constexpr auto tolerance = std::numeric_limits<Treal_t>::epsilon();
        ASSERT_NEAR(static_cast<Treal_t>(0.0), (*rv.get_positions_view().begin())(0), tolerance);
        ASSERT_NEAR(static_cast<Treal_t>(1.0), (*(rv.get_positions_view().begin() + 1))(0), tolerance);
        ASSERT_NEAR(static_cast<Treal_t>(2.0), (*(rv.get_positions_view().begin() + 2))(0), tolerance);
        ASSERT_NEAR(static_cast<Treal_t>(3.0), (*(rv.get_positions_view().begin() + 3))(0), tolerance);
        ASSERT_NEAR(static_cast<Treal_t>(4.0), (*(rv.get_positions_view().begin() + 4))(0), tolerance);
        
        // 2. force
        ASSERT_NEAR(static_cast<Treal_t>(0.0), (*rv.get_forces_view().begin())(0), tolerance);
        ASSERT_NEAR(static_cast<Treal_t>(2.0), (*(rv.get_forces_view().begin() + 1))(0), tolerance);
        ASSERT_NEAR(static_cast<Treal_t>(4.0), (*(rv.get_forces_view().begin() + 2))(0), tolerance);
        ASSERT_NEAR(static_cast<Treal_t>(6.0), (*(rv.get_forces_view().begin() + 3))(0), tolerance);
        ASSERT_NEAR(static_cast<Treal_t>(8.0), (*(rv.get_forces_view().begin() + 4))(0), tolerance);
        
        // 3. masses
        ASSERT_NEAR(static_cast<Treal_t>(0.0), (*rv.get_masses_view().begin()), tolerance);
        ASSERT_NEAR(static_cast<Treal_t>(3.0), (*(rv.get_masses_view().begin() + 1)), tolerance);
        ASSERT_NEAR(static_cast<Treal_t>(6.0), (*(rv.get_masses_view().begin() + 2)), tolerance);
        ASSERT_NEAR(static_cast<Treal_t>(9.0), (*(rv.get_masses_view().begin() + 3)), tolerance);
        ASSERT_NEAR(static_cast<Treal_t>(12.0), (*(rv.get_masses_view().begin() + 4)), tolerance);
        
        // 4. inv. masses
        ASSERT_NEAR(static_cast<Treal_t>(0.0), (*rv.get_cached_inv_masses_view().begin()), tolerance);
        ASSERT_NEAR(static_cast<Treal_t>(4.0), (*(rv.get_cached_inv_masses_view().begin() + 1)), tolerance);
        ASSERT_NEAR(static_cast<Treal_t>(8.0), (*(rv.get_cached_inv_masses_view().begin() + 2)), tolerance);
        ASSERT_NEAR(static_cast<Treal_t>(12.0), (*(rv.get_cached_inv_masses_view().begin() + 3)), tolerance);
        ASSERT_NEAR(static_cast<Treal_t>(16.0), (*(rv.get_cached_inv_masses_view().begin() + 4)), tolerance);
        
        // 5. etas
        ASSERT_NEAR(static_cast<Treal_t>(0.0), (*rv.get_cached_etas_view().begin())(0), tolerance);
        ASSERT_NEAR(static_cast<Treal_t>(5.0), (*(rv.get_cached_etas_view().begin() + 1))(0), tolerance);
        ASSERT_NEAR(static_cast<Treal_t>(10.0), (*(rv.get_cached_etas_view().begin() + 2))(0), tolerance);
        ASSERT_NEAR(static_cast<Treal_t>(15.0), (*(rv.get_cached_etas_view().begin() + 3))(0), tolerance);
        ASSERT_NEAR(static_cast<Treal_t>(20.0), (*(rv.get_cached_etas_view().begin() + 4))(0), tolerance);
    };
    
    // copy constructor
    auto rep_view2 = rep_view1;
    test_contents(rep_view2);
    
    // move constructor
    const auto rep_view3 = replica_view(std::move(rep_view2)); // forced move constructor
    test_contents(rep_view2);
    test_contents(rep_view3);
    
    // copy operator
    rep_view2 = rep_view3; // forced copy operator
    test_contents(rep_view2);
    test_contents(rep_view3);
    
    // move operator
    const auto rep_view4 = std::move(rep_view2); // forced move operator
    test_contents(rep_view2);
    test_contents(rep_view4);
}

TEST(testsuite_replica_view, lifecycle2_sp)
{
    test_lifecycle2<float>();
}

TEST(testsuite_replica_view, lifecycle2_dp)
{
    test_lifecycle2<double>();
}

// END OF TESTFILE
