/// \file This file contains test(s) for 'vector3'.

#include "vector3.h"

#include "gtest/gtest.h"

#include <limits> // std::numeric_limits


using namespace vvi;

template<class Treal_t>
void test_lifecycle1()
{
    // default constructibility
    auto test_vector = vector3<Treal_t>();
    
    // init constructibility
    auto test_vector2 = vector3<Treal_t>(static_cast<Treal_t>(1.0), static_cast<Treal_t>(2.0), static_cast<Treal_t>(3.0));
}

TEST(testsuite_vector3, lifecycle1_sp)
{
    test_lifecycle1<float>();
}

TEST(testsuite_vector3, lifecycle1_dp)
{
    test_lifecycle1<double>();
}


template<class Treal_t>
void test_access()
{
    // create a test instance
    auto test_vector = vector3<Treal_t>(static_cast<Treal_t>(1.0), static_cast<Treal_t>(2.0), static_cast<Treal_t>(3.0));
    
    // error case
    #ifndef NDEBUG
    ASSERT_THROW(test_vector(3), std::invalid_argument);
    ASSERT_THROW(test_vector(4), std::invalid_argument);
    #endif
    
    // const-qualified
    constexpr auto tolerance = std::numeric_limits<Treal_t>::epsilon();
    ASSERT_NEAR(static_cast<Treal_t>(1.0), test_vector(0), tolerance);
    ASSERT_NEAR(static_cast<Treal_t>(2.0), test_vector(1), tolerance);
    ASSERT_NEAR(static_cast<Treal_t>(3.0), test_vector(2), tolerance);
    
    // non-const qualified
    test_vector(0) = static_cast<Treal_t>(4.0);
    test_vector(1) = static_cast<Treal_t>(5.0);
    test_vector(2) = static_cast<Treal_t>(6.0);
    ASSERT_NEAR(static_cast<Treal_t>(4.0), test_vector(0), tolerance);
    ASSERT_NEAR(static_cast<Treal_t>(5.0), test_vector(1), tolerance);
    ASSERT_NEAR(static_cast<Treal_t>(6.0), test_vector(2), tolerance);
}

TEST(testsuite_vector3, access_sp)
{
    test_access<float>();
}

TEST(testsuite_vector3, access_dp)
{
    test_access<double>();
}


template<class Treal_t>
void test_lifecycle2()
{
    // create a test instance
    const auto test_vector = vector3<Treal_t>(static_cast<Treal_t>(1.0), static_cast<Treal_t>(2.0), static_cast<Treal_t>(3.0));
    
    // lambda for testing purposes
    const auto test_contents = [](const vector3<Treal_t>& v)
    {
        constexpr auto tolerance = std::numeric_limits<Treal_t>::epsilon();
        ASSERT_NEAR(static_cast<Treal_t>(1.0), v(0), tolerance);
        ASSERT_NEAR(static_cast<Treal_t>(2.0), v(1), tolerance);
        ASSERT_NEAR(static_cast<Treal_t>(3.0), v(2), tolerance);
    };
    
    // copy constructor
    auto test_vector2 = test_vector;
    test_contents(test_vector2);
    
    // move constructor
    const auto test_vector3 = vector3(std::move(test_vector2)); // forced move constructor
    test_contents(test_vector2);
    test_contents(test_vector3);
    
    // copy operator
    test_vector2 = test_vector3; // forced copy operator
    test_contents(test_vector2);
    test_contents(test_vector3);
    
    // move operator
    const auto test_vector4 = std::move(test_vector2); // forced move operator
    test_contents(test_vector2);
    test_contents(test_vector4);
}

TEST(testsuite_vector3, lifecycle2_sp)
{
    test_lifecycle2<float>();
}

TEST(testsuite_vector3, lifecycle2_dp)
{
    test_lifecycle2<double>();
}


template<class Treal_t>
void test_arithmetic()
{
    // negation
    const auto test_vector = vector3<Treal_t>(static_cast<Treal_t>(1.0), static_cast<Treal_t>(2.0), static_cast<Treal_t>(3.0));
    const auto neg_test_vector = -test_vector;
    constexpr auto tolerance = std::numeric_limits<Treal_t>::epsilon();
    ASSERT_NEAR(static_cast<Treal_t>(-1.0), neg_test_vector(0), tolerance);
    ASSERT_NEAR(static_cast<Treal_t>(-2.0), neg_test_vector(1), tolerance);
    ASSERT_NEAR(static_cast<Treal_t>(-3.0), neg_test_vector(2), tolerance);
    
    // reflexive addition
    auto ra_test_vector = test_vector;
    ra_test_vector += vector3<Treal_t>(static_cast<Treal_t>(5.0), static_cast<Treal_t>(6.0), static_cast<Treal_t>(7.0));
    ASSERT_NEAR(static_cast<Treal_t>(6.0), ra_test_vector(0), tolerance);
    ASSERT_NEAR(static_cast<Treal_t>(8.0), ra_test_vector(1), tolerance);
    ASSERT_NEAR(static_cast<Treal_t>(10.0), ra_test_vector(2), tolerance);
    
    // reflexive subtraction
    auto rs_test_vector = test_vector;
    rs_test_vector -= vector3<Treal_t>(static_cast<Treal_t>(5.0), static_cast<Treal_t>(6.0), static_cast<Treal_t>(7.0));
    ASSERT_NEAR(static_cast<Treal_t>(-4.0), rs_test_vector(0), tolerance);
    ASSERT_NEAR(static_cast<Treal_t>(-4.0), rs_test_vector(1), tolerance);
    ASSERT_NEAR(static_cast<Treal_t>(-4.0), rs_test_vector(2), tolerance);
    
    // addition
    const auto a_test_vector = test_vector
        + vector3{static_cast<Treal_t>(-1.0), static_cast<Treal_t>(-2.0), static_cast<Treal_t>(-3.0)};
    ASSERT_NEAR(static_cast<Treal_t>(0.0), a_test_vector(0), tolerance);
    ASSERT_NEAR(static_cast<Treal_t>(0.0), a_test_vector(1), tolerance);
    ASSERT_NEAR(static_cast<Treal_t>(0.0), a_test_vector(2), tolerance);
    
    // subtraction
    const auto s_test_vector = test_vector
        - vector3{static_cast<Treal_t>(1.0), static_cast<Treal_t>(2.0), static_cast<Treal_t>(3.0)};
    ASSERT_NEAR(static_cast<Treal_t>(0.0), s_test_vector(0), tolerance);
    ASSERT_NEAR(static_cast<Treal_t>(0.0), s_test_vector(1), tolerance);
    ASSERT_NEAR(static_cast<Treal_t>(0.0), s_test_vector(2), tolerance);
    
    // scaling
    const auto sc_test_vector = test_vector * static_cast<Treal_t>(2.0);
    ASSERT_NEAR(static_cast<Treal_t>(2.0), sc_test_vector(0), tolerance);
    ASSERT_NEAR(static_cast<Treal_t>(4.0), sc_test_vector(1), tolerance);
    ASSERT_NEAR(static_cast<Treal_t>(6.0), sc_test_vector(2), tolerance);
}

TEST(testsuite_vector3, arithmetic_sp)
{
    test_arithmetic<float>();
}

TEST(testsuite_vector3, arithmetic_dp)
{
    test_arithmetic<double>();
}

// END OF TESTFILE
