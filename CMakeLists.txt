cmake_minimum_required(VERSION 3.16)

# specify the C++ standard
set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED True)

# set the project name
project(vv_integrators VERSION 0.1.0)

# options
option(BUILD_DEBUG_SECTIONS "Builds parts of the code which can be only turned on for debugging purposes." OFF)
option(BUILD_TESTS "Builds test programs which are used to verify the functionality of the library." ON)
option(BUILD_BENCHMARKS "Builds benchmark programs which are used to analyze the performance of the library." ON)
set(LLD_CACHE_SIZE_MB "32" CACHE STRING "Size of the last level data cache in MiB.")
set(AVX_INSTRUCTION_SET "WIDEST" CACHE STRING "Choose which AVX instruction set is to be used for the vectorization of kernels.")
set_property(CACHE AVX_INSTRUCTION_SET PROPERTY STRINGS WIDEST AVX2x AVX512x)

# propagate the options
if (NOT BUILD_DEBUG_SECTIONS)
    add_compile_definitions("NDEBUG")
endif ()

# obtain information about the compiler
if (CMAKE_CXX_COMPILER_ID MATCHES "^GNU")
    set(GNU_COMPILER ON)
    set(INTEL_COMPILER OFF)
elseif (CMAKE_CXX_COMPILER_ID MATCHES "^Intel")
    set(GNU_COMPILER OFF)
    set(INTEL_COMPILER ON)
else()
    message(WARNING "Only Intel and GCC compiler are currently configured; compilation may not succeed!")
endif()

# find required OpenMP lib
find_package(OpenMP REQUIRED)
if (GNU_COMPILER)
    message("-- OpenMP Library located at: " ${OpenMP_gomp_LIBRARY})
elseif (INTEL_COMPILER)
    message("-- OpenMP Library located at: " ${OpenMP_iomp5_LIBRARY})
endif()

# find MPI
if (DEFINED ENV{EBROOTPSMPI})
    set(MPI_INCLUDE_DIR $ENV{EBROOTPSMPI}/include)
    set(MPI_CXX_LIBRARIES $ENV{EBROOTPSMPI}/lib/libmpi.so)
    message("-- ParaStationMPI Library located at: " ${MPI_CXX_LIBRARIES})
elseif(DEFINED ENV{I_MPI_ROOT})
    set(MPI_INCLUDE_DIR $ENV{I_MPI_ROOT}/include)
    set(MPI_CXX_LIBRARIES $ENV{I_MPI_ROOT}/lib/release/libmpi.so)
    message("-- IntelMPI Library located at: " ${MPI_CXX_LIBRARIES})
else()
    find_package(MPI REQUIRED)
    set(MPI_INCLUDE_DIR ${MPI_CXX_INCLUDE_DIRS})
    message("-- MPI Library located at: " ${MPI_CXX_LIBRARIES})
endif()

# common variables (top level)
set(OPTIMIZATION_FLAGS "-O3;-ffast-math;-march=native;")
if (INTEL_COMPILER)
    string(CONCAT ${OPTIMIZATION_FLAGS} "-qopt-zmm-usage=high")
endif()
if (INTEL_COMPILER)
    set(WARNING_FLAGS "-Wall;-Wextra;-Werror")
elseif (GNU_COMPILER)
    set(WARNING_FLAGS "-fstack-protector-all;-O3;-Wall;-Wpedantic;\
-Wclobbered;-Wdeprecated-copy;-Wenum-conversion;-Wignored-qualifiers;-Wmissing-field-initializers;\
-Wsign-compare;-Wstring-compare;-Wredundant-move;-Wtype-limits;-Wuninitialized;-Wshift-negative-value;-Wunused-parameter;\
-Werror")
endif()
set(COMMON_COMPILE_FLAGS ${OPTIMIZATION_FLAGS} ${WARNING_FLAGS})

# project include directory
set(PROJ_INCLUDE_DIR ${CMAKE_SOURCE_DIR}/include/)

# add subdirectories
add_subdirectory(src)
add_subdirectory(app)
if (BUILD_BENCHMARKS)
    add_subdirectory(benchmark)
endif()
if (BUILD_TESTS)
    add_subdirectory(test)
endif()
